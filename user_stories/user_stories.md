# User-stories

## Client

- Une personne non identifiée arrive sur le site et veut consulter la liste des
  spectacles disponibles.
- Une personne non identifiée veut pouvoir afficher un planing des spectacles,
  affiché par semaines, avec les jours en abscisse et les heures en ordonnées.
- Une personne non identifiée veut créer son compte sur le site.
- Une personne non identifiée veut s'identifier sur le site.
- Une personne identifiée veut éditer son profile.
- Une personne identifiée veut ajouter au panier une (ou plusieurs) place d'un
  spectacle donnée.
- Une personne identifiée veut supprimer des items de son panier.
- Une personne identifiée veut valider son panier.
- Une personne identifiée veut lister les places en fonction de leur prix.
- Une personne identifiée veut lister les places en fonction de leur emplacement
  dans la salle.

## Administrateur

- Un administrateur veut ajouter un spectacle.
- Un administrateur veut ajouter une salle de spectacle.
- Un administrateur veut modifier la programmation d'un spectacle (jour:heure).
- Un administrateur veut modifier le prix des zones de place.
- Un administrateur veut supprimer un spectacle.
- Un administrateur veut voir le nombre de places vendues pour un spectacle.
- Un administrateur veut voir le nombre de places vendues pour une séance d'un
  spectacle.

## Divers

- Un client doit pouvoir réserver sa place pendant 20 minutes maximum.
- Un client ne doit pas faire plus de 2 clics pour accéder à la fonctionnalitée
  voulue.
