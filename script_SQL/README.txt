Il n'est pas possible de deplacer individuellement une unique base de donnée
vers le repertoire git (source JDF ;) )
Ainsi, il va falloir que chacun mette en place sur son ordinateur la base de
donnée mysql.
Il faudrait que chacun ait le même nom de base de donnée (projet_web_bd).
Il faudrait que l'un des nom d'utilisateur ayant accès à cette BD soit le
même pour tout le monde (java) et ait le même mot de passe (pass si vous
tenez à quelque chose de compliquer, allez-y, je suis ouvert c'est bien
connu ^^)
voici donc les étapes à réaliser:

	Installation de mysql:
sudo apt-get install mysql-server mysql-client
A la fin de l'installation, on vous demande un mot de passe root.
Choisissez-en un. Si par la suite vous voulez le modifier, tappez:
sudo mysqladmin -p -u root -h localhost password mon_super_mot_de_passe

	Création de la base de donnée:
On se connecte en root: sudo mysql -p
On crée la BD: CREATE DATABASE projet_web_bd

	Création de l'utilisateur "java":
CREATE USER 'java'@'localhost' IDENTIFIED BY 'pass';
GRANT ALL ON projet_web_bd.* TO 'java'@'localhost' IDENTIFIED BY 'pass';



	Si vous voulez vous connecter à la base de donnée:
mysql -h localhost -u java -p
USE projet_web_bd;

	Pour executer l'un des scripts .sql:
mysql -p -u java projet_web_bd < mon_script.sql

	Si vous n'aimez pas le password pass on pourra choisir un mot de
passe du genre Abrµti_du_38 et il faudra alors taper:
mysql -p -u root
SET PASSWORD FOR 'java'@'localhost' = PASSWORD('Abrµti_du_38');
