CREATE TABLE User (
	userMail varchar(100) NOT NULL,
	userFirstName varchar(100),
	userLastName varchar(100),
	userPassword varchar(200) NOT NULL,
	PRIMARY KEY (userMail)
);

CREATE TABLE Administrator (
	userMail varchar(100) NOT NULL,
	FOREIGN KEY (userMail) REFERENCES User(userMail),
	PRIMARY KEY (userMail)
);

CREATE TABLE Customer (
	userMail varchar(100) NOT NULL,
	FOREIGN KEY (userMail) REFERENCES User(userMail),
	PRIMARY KEY (userMail)
);

CREATE TABLE Spectacle (
    spectacleNumber integer NOT NULL,
    spectacleName varchar(100) NOT NULL,
    spectaclePicture varchar(200),
    userMail varchar(100),
	FOREIGN KEY (userMail) REFERENCES Administrator(userMail),
	PRIMARY KEY (spectacleNumber)
);

CREATE TABLE DateHour (
    dateHour datetime NOT NULL,
	PRIMARY KEY (dateHour)
);

CREATE TABLE Room (
    roomName varchar(100) NOT NULL,
	PRIMARY KEY (roomName)
);

CREATE TABLE Case_ (
    caseNumber integer NOT NULL,
	ticketNumber integer NOT NULL UNIQUE,
	userMail varchar(100) NOT NULL,
	FOREIGN KEY (userMail) REFERENCES Customer(userMail),
	PRIMARY KEY (caseNumber)
);

CREATE TABLE Category (
	categoryName varchar(100) NOT NULL,
	tariff DECIMAL(9,2) NOT NULL,
	PRIMARY KEY (categoryName),
	CHECK(tariff>0)
);

CREATE TABLE Row_ (
    roomName varchar(100),
	rowNumber integer NOT NULL,
	categoryName varchar(100) NOT NULL,
	FOREIGN KEY (roomName) REFERENCES Room(roomName),
	FOREIGN KEY (categoryName) REFERENCES Category(categoryName),
	PRIMARY KEY (roomName,rowNumber),
	CHECK (Row_>0)
);

CREATE TABLE Seat (
    roomName varchar(100),
	rowNumber integer,
	seatNumber integer NOT NULL,
	FOREIGN KEY (roomName, rowNumber) REFERENCES Row_(roomName, rowNumber),
	PRIMARY KEY (roomName, rowNumber, seatNumber),
	CHECK (seatNumber>0)
);

CREATE TABLE Representation (
	spectacleNumber integer,
    dateHour datetime,
    roomName varchar(100),
	isWithdrawn boolean,
	FOREIGN KEY (spectacleNumber) REFERENCES Spectacle(spectacleNumber),
	FOREIGN KEY (dateHour) REFERENCES DateHour(dateHour),
	FOREIGN KEY (roomName) REFERENCES Room(roomName),
	PRIMARY KEY (spectacleNumber, dateHour, roomName)
);

CREATE TABLE Sale (
    caseNumber integer,
    dateHour datetime,
    roomName varchar(100),
	rowNumber integer,
	seatNumber integer,
	FOREIGN KEY (caseNumber) REFERENCES Case_(caseNumber),
	FOREIGN KEY (dateHour) REFERENCES DateHour(dateHour),
	FOREIGN KEY (roomName, rowNumber, seatNumber) REFERENCES Seat(roomName, rowNumber, seatNumber),
	PRIMARY KEY (caseNumber, dateHour, roomName, rowNumber, seatNumber)
);

CREATE TABLE Bookings (
	userMail varchar(100),
    dateHour datetime,
    roomName varchar(100),
	rowNumber integer,
	seatNumber integer,
	FOREIGN KEY (userMail) REFERENCES Customer(userMail),
	FOREIGN KEY (dateHour) REFERENCES DateHour(dateHour),
	FOREIGN KEY (roomName, rowNumber, seatNumber) REFERENCES Seat(roomName, rowNumber, seatNumber),
	PRIMARY KEY (userMail, dateHour, roomName, rowNumber, seatNumber)
);

INSERT INTO User VALUES 
('admin@imag.fr', 'Seigneur', 'Dumal', 'b646b1b1914c0a41bee504e96f5a92af$62c8cf386727c276e53a16c68e8028f61f7a381a1ddc0513450d254f9378f9e11fc970b2b9371929baf95df23d9445ef5c0d0d8587e59380c1616900ed8216b4');

INSERT INTO Administrator VALUES
('admin@imag.fr');
