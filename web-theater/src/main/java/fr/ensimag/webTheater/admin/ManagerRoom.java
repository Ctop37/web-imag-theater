/*
 * The MIT License
 *
 * Copyright 2015 louis.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.admin;

import fr.ensimag.webTheater.beans.*;
import fr.ensimag.webTheater.dao.*;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author louis
 */
public class ManagerRoom {
    
    public static final String SELECT_SALLE = "select_salle";
    
    private Room salle;
    private ArrayList<Row> rows;
    
    private DAOFactory daoFactory = null;
    private DAORoomImpl daoRoom = null;
    
    public ManagerRoom(){
        daoFactory = DAOFactory.getInstance();
        daoRoom = new DAORoomImpl(daoFactory);
    }
    
    public void setSalle(Room salle){
        this.salle = salle;
    }
    
    public Room getSalle(){
        return this.salle;
    }

    public int manageRoom(HttpServletRequest request) {
        salle = new Room();
        salle.setName(request.getParameter(SELECT_SALLE));
        
        
        return 1;
    }
    
    public String showRang(HttpServletRequest request){
        String strRang = "";
        
        for (int i = 0; i<10; i++){
            strRang += "<option value=\"" + "Rang" + i +  "\">" + "Rang" + i + "</option>\n";
        }
        
        return strRang;
    }

    public int newRoom(HttpServletRequest request) {
        salle = new Room();
        salle.setName("Entrer le nom de la salle");
        return 1;
    }
    
}
