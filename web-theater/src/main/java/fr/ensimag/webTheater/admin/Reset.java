/*
 * The MIT License
 *
 * Copyright 2015 louis.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.admin;

import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Spectacle;
import fr.ensimag.webTheater.dao.DAOArchiveImpl;
import fr.ensimag.webTheater.dao.DAOCategoryImpl;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.dao.DAORepresentationImpl;
import fr.ensimag.webTheater.dao.DAORoomImpl;
import fr.ensimag.webTheater.dao.DAOSpectacleImpl;
import fr.ensimag.webTheater.dao.DAOUserImpl;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author louis
 */
public class Reset {
    
    private DAOFactory daoFactory = null;
    private DAOArchiveImpl daoArchive = null;
    
    public Reset(){
        daoFactory = DAOFactory.getInstance();
        daoArchive = new DAOArchiveImpl(daoFactory);
    }

    public int resetBD(HttpServletRequest request) {
        String output;
        Date currDate = new Date();
        Date dateDebut;
        Date dateFin;
        int anneeD = 0, anneeF = 0;
        
        anneeD = currDate.getYear()-1;
        anneeF = currDate.getYear();
        
        if (currDate.getMonth() <= 7 ){
            anneeD = anneeD -1;
            anneeF = anneeF -1;
        }
        
        dateDebut = new Date(anneeD, 9-1, 1, 0, 0);
        dateFin = new Date(anneeF,6-1,30,23,0);
        output = daoArchive.archive(dateDebut, dateFin);
        
        return 1;
    }
    
    
    
}

