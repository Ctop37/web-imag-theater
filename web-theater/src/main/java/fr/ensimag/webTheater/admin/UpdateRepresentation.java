/*
 * The MIT License
 *
 * Copyright 2015 louis.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.admin;

import fr.ensimag.webTheater.beans.*;
import fr.ensimag.webTheater.dao.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;



/**
 *
 * @author louis
 */
public class UpdateRepresentation {
    
    public static final String DATE_REPRESENTATION = "date_representation";
    public static final String SALLE_REPRESENTATION = "select_representation_salle";
    public static final String NUM_SPECTACLE = "num_spectacle_representation";
    
    private DAOFactory daoFactory = null;
    private DAOSpectacleImpl daoSpect = null;
    private DAORepresentationImpl daoRepre = null;
    private DAODateImpl daoDate = null;
    
    private SimpleDateFormat dateFormat;
    
    public UpdateRepresentation(){
        daoFactory = DAOFactory.getInstance();
        daoSpect = new DAOSpectacleImpl(daoFactory);
        daoRepre = new DAORepresentationImpl(daoFactory);
        daoDate = new DAODateImpl(daoFactory);
        
        dateFormat = new SimpleDateFormat("dd/MM/yyyy:HH");
    }

    public int saveRepresentation(HttpServletRequest request) throws ParseException {
        Representation currRepresentation = new Representation();
        Date currDate = new Date();
        String strDate = request.getParameter(DATE_REPRESENTATION);
        
        currDate = dateFormat.parse(strDate);
        
        daoDate.create(currDate);
        currRepresentation.setDate(currDate);
        
        currRepresentation.setSpectacleNumber(Integer.parseInt(request.getParameter(NUM_SPECTACLE)));
        currRepresentation.setRoomName(request.getParameter(SALLE_REPRESENTATION));
        currRepresentation.setIsWithdrawn(true);
        
        daoRepre.create(currRepresentation);
        return 1;
    }
    
    public int withdrawRepresentation(HttpServletRequest request) throws ParseException{
        Representation currRepresentation = new Representation();
        currRepresentation.setDate(dateFormat.parse(request.getParameter(DATE_REPRESENTATION)));
        currRepresentation.setRoomName(request.getParameter(SALLE_REPRESENTATION));
        currRepresentation.setSpectacleNumber(Integer.parseInt(request.getParameter(NUM_SPECTACLE)));
        currRepresentation.setIsWithdrawn(false);
        
        daoRepre.cancel(currRepresentation);
        
        return 1;
    }

}
