/*
 * The MIT License
 *
 * Copyright 2015 louis.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.admin;

import fr.ensimag.webTheater.beans.*;
import fr.ensimag.webTheater.dao.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author louis
 */
public class Manager {

    public static final String SELECT_SPECTACLE = "select_spectacle";
    public static final String NEW_SPECTACLE = "new_spectacle";
    public static final String SELECT_REPRESENTATION = "select_representation";
    public static final String NEW_REPRESENTATION = "new_representation";

    public static final String SELECT_REPRESENTATION_SALLE = "select_representation_salle";
    public static final String SELECT_REPRESENTATION_SPECTACLE = "representation_spectacle";
    public static final String DISCARD_REPRESENTATION = "discard_representation";
    public static final String DATE_REPRESENTATION = "date_representation";

    private Spectacle spectacle;
    private Representation representation;
    
    private DAOFactory daoFactory = null;
    private DAOSpectacleImpl daoSpect = null;
    private DAORepresentationImpl daoRepre = null;
    private DAORoomImpl daoRoom = null;
    private DAOCategoryImpl daoCat = null;
    
    private DAOUserImpl daoUser = null;
    
    private Random rand;
    private int maxRand;
    private int minRand;
    
    private SimpleDateFormat dateFormat;
    
    public Manager(){
        daoFactory = DAOFactory.getInstance();
        daoSpect = new DAOSpectacleImpl(daoFactory);
        daoRepre = new DAORepresentationImpl(daoFactory);
        daoRoom = new DAORoomImpl(daoFactory);
        daoCat = new DAOCategoryImpl(daoFactory);
        
        daoUser = new DAOUserImpl(daoFactory);
        
        rand = new Random();
        maxRand = 199;
        minRand = 1;
        
        dateFormat = new SimpleDateFormat("dd/MM/yyyy:HH");
        
    }

    public void setSpectacle(Spectacle spectacle) {
        this.spectacle = spectacle;
    }

    public void setRepresentation(Representation representation) {
        this.representation = representation;
    }

    public Spectacle getSpectacle() {
        return spectacle;
    }

    public Representation getRepresentation() {
        return representation;
    }

    public String showSpectacle(HttpServletRequest request) {
        Spectacle tempSpect = new Spectacle();

        String strSpectacle = "";

        Set listSpectacle = new HashSet();

        for (int i = 0; i< 200; i++){
            tempSpect = daoSpect.find(i);
            if (tempSpect != null){
                listSpectacle.add(tempSpect);
            }
        }

        //-----------------
        for (Iterator it = listSpectacle.iterator(); it.hasNext();) {
            Spectacle listSpectacle1 = (Spectacle) it.next();
            strSpectacle += "<option value=\"" + listSpectacle1.getNumber() + "\">" + listSpectacle1.getName() + "</option>\n";
        }

        return strSpectacle;
    }

    public String showRepresentation(HttpServletRequest request){
        Set listRepresentation = new HashSet();
        String cancel = "";
        
        String strRepresentation = "";
        
        if(this.spectacle != null){
            daoSpect.findRepresentations(this.spectacle);
            listRepresentation = this.spectacle.getRepresentations();
        }

        for (Iterator it = listRepresentation.iterator(); it.hasNext();) {
            Representation listRepresentation1 = (Representation) it.next();
            if (listRepresentation1.isIsWithdrawn()){
                cancel = "";
            }
            else {
                cancel = ".annulé";
            }
            strRepresentation += "<option value=\"" + listRepresentation1.getSpectacleNumber() 
                    + "." + listRepresentation1.getRoomName() + "." 
                    + dateFormat.format(listRepresentation1.getDate()) + "\">" 
                    + listRepresentation1.getSpectacleNumber() + "." 
                    + listRepresentation1.getRoomName() + "." 
                    + dateFormat.format(listRepresentation1.getDate())
                    + cancel + "</option>\n";
        }

        return strRepresentation;
    }
    
    public String showRoom(HttpServletRequest request){
        String strRoom = "";
        Set<String> listRoom = new HashSet();
        
        listRoom = daoRoom.findAll();
        
        for (Iterator it = listRoom.iterator(); it.hasNext();) {
            String listRoom1 = (String) it.next();
            strRoom += "<option value=\"" + listRoom1 +  "\">" + listRoom1 + "</option>\n";
        }
        
        return strRoom;
    }
    
    public String showCat(HttpServletRequest request) {
        String strCat = "";
        
        
        return strCat;
    }

    public Spectacle newSpectacle() {
        //this.resetSpectacle();
        this.resetRepresentation();
        Spectacle spectacle1 = new Spectacle();
        int numSp = 0;
        spectacle1.setImagePath("/public/media/spectacles/cat1.jpg");
        spectacle1.setMailAdmin("admin@imag.fr");
        spectacle1.setName("Entrer un nom de spectacle");
        numSp = rand.nextInt((this.maxRand - this.minRand) + 1) + this.minRand;
        while (daoSpect.find(numSp) != null){
            numSp = rand.nextInt((this.maxRand - this.minRand) + 1) + this.minRand;
        }
        spectacle1.setNumber(numSp); //Need to get next Number
        this.setSpectacle(spectacle1);
        return spectacle1;
    }

    public Representation newRepresentation() {
        //this.resetSpectacle();
        //this.resetRepresentation();
        Representation repre1 = new Representation();
        Date newDate = new Date();
        newDate.setYear(0000);
        newDate.setMonth(00);
        newDate.setMonth(00);
        newDate.setHours(00);
        repre1.setDate(newDate);
        repre1.setIsWithdrawn(false);
        repre1.setRoomName("Entrer une salle");
        if (this.spectacle != null){
            repre1.setSpectacleNumber(this.spectacle.getNumber());
        }
        else{
            repre1.setSpectacleNumber(-1);
        }
        this.setRepresentation(repre1);
        return repre1;
    }

    public void resetSpectacle() {
        this.spectacle = new Spectacle();
    }

    public void resetRepresentation() {
        this.representation = new Representation();
    }

    public void manageSpectacle(HttpServletRequest request) {
        String numSpectacle = request.getParameter(SELECT_SPECTACLE);
        Spectacle currSpectacle = new Spectacle();
       
        
        //this.resetSpectacle();
        this.resetRepresentation();
        if (numSpectacle != null){
            currSpectacle = this.daoSpect.find(Integer.parseInt(numSpectacle));
            this.setSpectacle(currSpectacle);
        }
        else{
            this.setSpectacle(null);
        }
        
    }

    public void manageRepresentation(HttpServletRequest request) throws ParseException {
        String numRepresentation = request.getParameter(SELECT_REPRESENTATION);
        Representation currRepresentation = new Representation();
        Set<Representation> listRepre = new HashSet();
        
        //this.resetSpectacle();
        //this.resetRepresentation();
        if (numRepresentation != null){
            listRepre = this.spectacle.getRepresentations();
        }
           
        for (Iterator it = listRepre.iterator(); it.hasNext();) {
            Representation repre = (Representation) it.next();
            if (numRepresentation.equals(repre.getSpectacleNumber() + "." + repre.getRoomName() + "." + dateFormat.format(repre.getDate()) )){
                currRepresentation = repre;
                break;
            }
            
        }
        this.setRepresentation(currRepresentation);
    }

    

}
