/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.listeners;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.dao.DAOBooking;
import fr.ensimag.webTheater.dao.DAOFactory;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author jean-david
 */
public class SessionCycle implements HttpSessionListener {

  @Override
  public void sessionCreated(HttpSessionEvent se) {
    // noting to do here
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent se) {
    Object bookings = se.getSession().getAttribute("bookings");
    if(bookings != null) {
      DAOFactory myFactory = DAOFactory.getInstance();
      DAOBooking myDAOBooking = myFactory.getDAOBooking();
      Iterator<Booking> iterator = ((LinkedList<Booking>) bookings).iterator();
      while(iterator.hasNext()) {
        myDAOBooking.delete(iterator.next());
      }
    }
  }  
}
