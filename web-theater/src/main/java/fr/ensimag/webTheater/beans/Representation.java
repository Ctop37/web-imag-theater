/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jean-david
 */
public class Representation {
  private boolean isWithdrawn = true;
  private Date date;
  private int spectacleNumber;
  private String roomName;
  private HashMap<Category, LinkedList<Seat>> seats;

  /**
   * @return the isWithdrawn
   */
  public boolean isIsWithdrawn() {
    return isWithdrawn;
  }

  /**
   * @param isWithdrawn the isWithdrawn to set
   */
  public void setIsWithdrawn(boolean isWithdrawn) {
    this.isWithdrawn = isWithdrawn;
  }

  /**
   * @return the date
   */
  public Date getDate() {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate(Date date) {
    this.date = date;
  }

  /**
   * @return the room
   */
  public String getRoomName() {
    return roomName;
  }

  /**
   * @param roomName the room to set
   */
  public void setRoomName(String roomName) {
    this.roomName = roomName;
  }

  /**
   *
   * @param spectacleNumber the spectacle to set.
   */
  public void setSpectacleNumber(int spectacleNumber) {
      this.spectacleNumber = spectacleNumber;
  }

  /**
   *
   * @return the number of the corresponding spectacle.
   */
  public int getSpectacleNumber() {
      return spectacleNumber;
  }
  
  public boolean equals (Date date, int id, String roomName) {
    return 
      (roomName == null ? false : roomName.equals(this.getRoomName())) 
        && (date == null ? false : date.equals(this.getDate())) 
        && id == this.getSpectacleNumber();
  }

  /**
   * @return the seats
   */
  public HashMap<Category, LinkedList<Seat>> getSeats() {
    return seats;
  }

  /**
   * @param seats the seats to set
   */
  public void setSeats(HashMap<Category, LinkedList<Seat>> seats) {
    this.seats = seats;
  }
   
  public List <Seat> pickSeat(String categoryName, int number) {
    Iterator<Category> categoryIterator = this.seats.keySet().iterator();
    Category category = null;
    Category currentCategory = null;
    while(categoryIterator.hasNext()) {
      if((currentCategory = categoryIterator.next())
              .getName().equals(categoryName)) {
        category = currentCategory;
        break;
      }
    }
    
    if(category == null) {
      return null;
    }
    
    try {
      List <Seat> seatsToReturn = new LinkedList <Seat>() {} ;
      if( number > 1) {
        seatsToReturn = this.seats.get(category).subList(0, number);
      } else {
        seatsToReturn.add(this.seats.get(category).get(0));
      }
        
      Iterator<Seat> seatIterator = seatsToReturn.iterator();
      while(seatIterator.hasNext()) {
        seatIterator.next().setPrice(category.getPrice());
      }
      return seatsToReturn;
    } catch (Exception e) {
      return null;
    }   
  }
}
