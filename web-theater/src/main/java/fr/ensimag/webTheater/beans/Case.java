/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.beans;

import java.util.Date;

/**
 *
 * @author jean-david
 */
public class Case {
  private long number;
  private long ticketNumber;
  private String roomName;
  private String spectacleName;
  private String userMail;
  private Date date;

  /**
   * @return the number
   */
  public long getNumber() {
    return number;
  }

  /**
   * @param number the number to set
   */
  public void setNumber(long number) {
    this.number = number;
  }

  /**
   * @return the ticketNumber
   */
  public long getTicketNumber() {
    return ticketNumber;
  }

  /**
   * @param ticketNumber the ticketNumber to set
   */
  public void setTicketNumber(long ticketNumber) {
    this.ticketNumber = ticketNumber;
  }
  
    /**
     *
     * @param userMail the userMail of the customer
     */
    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    /**
     *
     * @return the userMail of the customer
     */
    public String getUserMail() {
        return userMail;
    }   

  /**
   * @return the roomName
   */
  public String getRoomName() {
    return roomName;
  }

  /**
   * @param roomName the roomName to set
   */
  public void setRoomName(String roomName) {
    this.roomName = roomName;
  }

  /**
   * @return the spectacleName
   */
  public String getSpectacleName() {
    return spectacleName;
  }

  /**
   * @param spectacleName the spectacleName to set
   */
  public void setSpectacleName(String spectacleName) {
    this.spectacleName = spectacleName;
  }

  /**
   * @return the date
   */
  public Date getDate() {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate(Date date) {
    this.date = date;
  }
}