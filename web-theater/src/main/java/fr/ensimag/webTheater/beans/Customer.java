/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.beans;

import java.util.Set;

/**
 *
 * @author jean-david
 */
public class Customer extends User {
  private Set<Case> cases;
  private Set<Booking> bookings;
  /**
   * @return the cases
   */
  public Set<Case> getCases() {
    return cases;
  }

  /**
   * @param cases the cases to set
   */
  public void setCases(Set<Case> cases) {
    this.cases = cases;
  }

  /**
   * @return the bookings
   */
  public Set<Booking> getBookings() {
    return bookings;
  }

  /**
   * @param bookings the bookings to set
   */
  public void setBookings(Set<Booking> bookings) {
    this.bookings = bookings;
  }
  
}
