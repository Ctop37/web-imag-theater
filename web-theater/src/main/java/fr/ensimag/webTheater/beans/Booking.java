/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.beans;

import java.util.Date;
import java.util.Set;

/**
 *
 * @author jean-david
 */
public class Booking {
  private Date date;
  private Date creationDate;
  private Set<Seat> seats;
  private Spectacle spectacle;
  private String roomName;
  private String userMail;
  private double totalPrice = 0;
  
  /**
   * @return the date
   */
  public Date getDate() {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate(Date date) {
    this.date = date;
  }

  /**
   * @return the seats
   */
  public Set<Seat> getSeats() {
    return seats;
  }

  /**
   * @param seats the seats to set
   */
  public void setSeats(Set<Seat> seats) {
    this.seats = seats;
  }

    /**
     *
     * @param userMail the userMail of the Customer
     */
    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    /**
     *
     * @return the userMail of the Customer
     */
    public String getUserMail() {
        return userMail;
    }

  /**
   * @return the creationDate
   */
  public Date getCreationDate() {
    return creationDate;
  }

  /**
   * @param creationDate the creationDate to set
   */
  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  /**
   * @return the spectacle
   */
  public Spectacle getSpectacle() {
    return spectacle;
  }

  /**
   * @param spectacle the spectacle to set
   */
  public void setSpectacle(Spectacle spectacle) {
    this.spectacle = spectacle;
  }

  /**
   * @return the totalPrice
   */
  public double getTotalPrice() {
    return totalPrice;
  }

  /**
   * @param totalPrice the totalPrice to set
   */
  public void setTotalPrice(double totalPrice) {
    this.totalPrice = totalPrice;
  }

  /**
   * @return the roomName
   */
  public String getRoomName() {
    return roomName;
  }

  /**
   * @param roomName the roomName to set
   */
  public void setRoomName(String roomName) {
    this.roomName = roomName;
  }
}
