/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author jean-david
 */
public class Spectacle {
  private int number = 0;
  private String name;
  private String imagePath;
  private String mailAdmin;
  private Set<Representation> representations;

  /**
   * @return the number
   */
  public int getNumber() {
    return number;
  }

  /**
   * @param id the number to set
   */
  public void setNumber(int number) {
    this.number = number;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the imagePath
   */
  public String getImagePath() {
    return imagePath;
  }

  /**
   * @param imagePath the imagePath to set
   */
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  /**
   * @return the representations
   */
  public Set<Representation> getRepresentations() {
    return representations;
  }

  /**
   * @param representations the representations to set
   */
  public void setRepresentations(Set<Representation> representations) {
    this.representations = representations;
  }

    /**
     *
     * @param mailAdmin the mail of the administrator having creating this
     * spectacle.
     */
    public void setMailAdmin(String mailAdmin) {
        this.mailAdmin = mailAdmin;
    }

    /**
     *
     * @return the mail of the administrator having creating this spectacle.
     */
    public String getMailAdmin() {
        return mailAdmin;
    }

    @Override
    public String toString() {
        return number+" "+name+" "+imagePath+" "+mailAdmin;
    }
    
    public Representation findRepresentation(Date date, String room) {
      Iterator<Representation> iterator = this.representations.iterator();
      Representation current;
      SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHH");
      while(iterator.hasNext()) {
        current = iterator.next();
        System.out.println();
        if(fmt.format(date).equals(fmt.format(current.getDate()))
                && current.getRoomName().equals(room))
          return current;
      }
      return null;
    }
}
