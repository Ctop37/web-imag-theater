/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.servlets;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Spectacle;
import fr.ensimag.webTheater.dao.DAOBooking;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.dao.DAORepresentation;
import fr.ensimag.webTheater.dao.DAOSpectacle;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ensimag.webTheater.beans.Seat;
import fr.ensimag.webTheater.beans.User;
import fr.ensimag.webTheater.dao.DAOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jean-david
 */
@WebServlet(name = "Bookings", urlPatterns = {"/bookings"})
public class Bookings extends HttpServlet {

  private DAOBooking daoBooking;
  private DAOSpectacle daoSpectacle;
  private DAORepresentation daoRepresentation;
  

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    int spectacleNumber = 0;
    Representation representation;
    
    try {
      spectacleNumber = Integer.decode(((String)request.getParameter("id")));      
    } catch(Exception e) {
      response.sendError(HttpServletResponse.SC_NOT_FOUND);
      return;
    }
    
    Spectacle spectacle = this.daoSpectacle.find(spectacleNumber);
    this.daoSpectacle.findRepresentations(spectacle);
    HashSet <Representation> representationsQuery = 
                                        (HashSet)spectacle.getRepresentations();
    
    Iterator<Representation> iterator = representationsQuery.iterator();
    LinkedList <Representation> representations = new LinkedList <>();
    while(iterator.hasNext()) {
      representation = iterator.next();
      if(representation.isIsWithdrawn()) 
        representations.add(representation);
      representation.setSeats(this.daoRepresentation.findSeats(representation));
    }
    Collections.sort(representations, new Comparator<Representation>() {

      @Override
      public int compare(Representation o1, Representation o2) {
        return o1.getDate().compareTo(o2.getDate());
      }
      
    });
    
    request.setAttribute("representations", representations);
    request.setAttribute("spectacle", spectacle);
    this.getServletContext()
            .getRequestDispatcher("/WEB-INF/jsp/spectacle_bookings.jsp")
            .forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    try {
      LinkedList <Booking> bookings = this.jsonToBookings(request, response);
      Object actual;
      this.validateBookings(bookings);
      if((actual = request.getSession().getAttribute("bookings"))==null) {
        actual = new LinkedList<Booking> ();
      }
      bookings.addAll((LinkedList <Booking>)actual);
      Collections.sort(bookings, new Comparator<Booking>() {

        @Override
        public int compare(Booking o1, Booking o2) {
          return o1.getDate().compareTo(o2.getDate());
        }

      });      
      request.getSession().setAttribute("bookings", bookings);
    } catch (IOException | ParseException | DAOException ex) {
      Logger.getLogger(Bookings.class.getName()).log(Level.SEVERE, null, ex);
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }
  }

  /**
   * Returns a short description of the servlet
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "bookings servlet";
  }

  @Override
  public void init() throws ServletException {
      this.daoBooking = ((DAOFactory) getServletContext()
              .getAttribute("DAOFactory")).getDAOBooking();
      this.daoRepresentation = ((DAOFactory) getServletContext()
              .getAttribute("DAOFactory")).getDAORepresentation();
      this.daoSpectacle = ((DAOFactory) getServletContext()
              .getAttribute("DAOFactory")).getDAOSpectacle();
  }
  
  private synchronized LinkedList <Booking> jsonToBookings(HttpServletRequest request, HttpServletResponse response) 
          throws IOException, ParseException {
    LinkedList <Booking> bookings = new LinkedList <Booking>();
    BufferedReader br = new BufferedReader(
            new InputStreamReader(request.getInputStream()));
    String json = "";
    if(br != null){
        json = br.readLine();
    }
    ObjectMapper mapper = new ObjectMapper();
    JsonNode root = mapper.readTree(json);
    
    Spectacle spectacle = this.daoSpectacle.find(root.path("id").asInt());
    this.daoSpectacle.findRepresentations(spectacle);
    
    JsonNode bookingsArray = root.path("bookings");
    int i = 0;
    JsonNode bookingNode = null;
    while((bookingNode = bookingsArray.get(i))!=null) {
      
      Booking booking = new Booking();
      booking.setCreationDate(new Date());
      booking.setUserMail(
           ((User)request.getSession().getAttribute("sessionUser")).getEmail());
      booking.setSpectacle(spectacle);
      String room = bookingNode.get("room").asText();
      booking.setRoomName(room);
      SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yy à HH");
      Calendar cal = Calendar.getInstance();
      cal.setTime(ft.parse(bookingNode.get("date").asText()));
      Date date = cal.getTime();
      booking.setDate(date);
      
      Representation currentRepresentation = 
              spectacle.findRepresentation(date, room);
      
      if(currentRepresentation == null)
        Logger.getLogger(Bookings.class.getName()).log(Level.SEVERE, null, new Exception());
      currentRepresentation.setSeats(this.daoRepresentation.findSeats(currentRepresentation));
      int j=0;
      JsonNode categorySeatsArrayNode = bookingNode.get("seats");
      JsonNode categorySeatsNode;
      while((categorySeatsNode=categorySeatsArrayNode.get(j))!=null) {
        if(booking.getSeats()==null)
          booking.setSeats(new TreeSet<Seat>());
        List <Seat> seats = currentRepresentation.pickSeat(
                categorySeatsNode.get("category").asText(), 
                categorySeatsNode.get("seatsNumber").asInt());
        booking.setTotalPrice(booking.getTotalPrice()
                              + seats.get(0).getPrice()*seats.size());
        booking.getSeats().addAll(seats);
        j++;
      }
      bookings.add(booking);
      i++;
    }
    
    return bookings;
  }
  
  private synchronized void validateBookings(LinkedList<Booking> bookings) throws DAOException {
    Iterator<Booking> iterator = bookings.iterator();
    while(iterator.hasNext()) {
      this.daoBooking.create(iterator.next());
    }
  }
}
