/*
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.servlets;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.dao.DAOBooking;
import fr.ensimag.webTheater.dao.DAOCase;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.dao.DAOSale;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jean-david
 */
@WebServlet(name = "Checkout", urlPatterns = {"/bookings/checkout"})
public class Checkout extends HttpServlet {

  private DAOSale daoSale;
  private DAOBooking daoBooking;
  private DAOCase daoCase;
  
  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {      
    Object o = request.getSession().getAttribute("bookings");
    if(o!=null && !((LinkedList<Booking>)o).isEmpty()) {
      LinkedList <Booking> bookings = (LinkedList <Booking>) o;
      request.setAttribute("bookings", bookings);
      Iterator<Booking> iterator = bookings.iterator();
      double totalPrice = 0;
      while(iterator.hasNext()) {
        totalPrice += iterator.next().getTotalPrice();
      }
      request.setAttribute("totalPrice", totalPrice);
    } else {
      request.setAttribute("errorMessage", "Vous n'avez aucune réservation dans votre panier");
    }
    request.setAttribute("cases", this.getCases(request));
    this.getServletContext()
            .getRequestDispatcher("/WEB-INF/jsp/checkout.jsp")
            .forward(request, response);
  }

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    String toDelete = request.getParameter("id");
    if(toDelete != null) {
      int idToDelete = Integer.parseInt(toDelete);
      LinkedList<Booking> bookings =(LinkedList<Booking>) request.getSession()
              .getAttribute("bookings");
      try {
        this.daoBooking.delete(bookings.remove(idToDelete));
        request.getSession().setAttribute("bookings", bookings);        
      } catch (Exception e) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
      }
      return;
    }    
    this.checkBookings(request);
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    LinkedList<Booking> bookings = 
            (LinkedList<Booking>) request.getSession().getAttribute("bookings");
    if(this.checkBookings(request)) {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    } else {
      Iterator <Booking> iterator = bookings.iterator();
      LinkedList<Sale> sales = new LinkedList<Sale>();
      LinkedList<Case> cases = new LinkedList<Case>();
      while(iterator.hasNext()) {
        Booking currentBooking = iterator.next();
        Case currentCase = new Case();
        currentCase.setNumber(this.hashTheBooking(currentBooking));
        currentCase.setTicketNumber(currentCase.getNumber());
        currentCase.setUserMail(currentBooking.getUserMail());
        currentCase.setRoomName(currentBooking.getRoomName());
        currentCase.setDate(currentBooking.getDate());
        currentCase.setSpectacleName(currentBooking.getSpectacle().getName());
        cases.add(currentCase);
        try {
          this.daoCase.create(currentCase);
          Sale currentSale = this.daoSale
                  .createFromBooking(currentBooking, currentCase);
          sales.add(currentSale);
          bookings.remove(currentBooking);
        } catch (Exception e) {
          this.daoBooking.create(currentBooking);
          response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
      }
      LinkedList<Sale> salesSession = this.getSales(request);
      LinkedList<Case> casesSession = this.getCases(request);
      if(salesSession!=null)
        sales.addAll(salesSession);
      if(casesSession!=null)
        cases.addAll(casesSession);
      request.getSession().setAttribute("sales", sales);
      request.getSession().setAttribute("cases", cases);
      request.getSession().setAttribute("bookings", bookings);
    }      
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }
  
  @Override
  public void init() {
    DAOFactory myFactory = 
            (DAOFactory) this.getServletContext().getAttribute("DAOFactory");
    this.daoBooking = myFactory.getDAOBooking();
    this.daoCase = myFactory.getDAOCase();
    this.daoSale = myFactory.getDAOSale();
  }
  
  private boolean checkBookings(HttpServletRequest request) {
    Object o = request.getSession().getAttribute("bookings");
    if(o == null || ((LinkedList <Booking>)o).isEmpty()) {
      return true;
    }
    LinkedList <Booking> bookings = (LinkedList <Booking>)o;
    Date now = new Date();
    boolean wasDeleted = false;
    Iterator<Booking> iterator = bookings.iterator();
    while(iterator.hasNext()) {
      Booking booking = iterator.next();
      long diff = now.getTime() - booking.getCreationDate().getTime();
      if((diff / (60 * 1000) % 60) > 1 ) {
        wasDeleted = true;
        bookings.remove(booking);
        this.daoBooking.delete(booking);
      }
    }
    request.getSession().setAttribute("bookings", bookings);
    return wasDeleted;
  }

  private long hashTheBooking(Booking booking) {
    String toHash = booking.getRoomName() + booking.getUserMail();
    SimpleDateFormat ft = new SimpleDateFormat("HHddMMyyyy");
    toHash += ft.format(booking.getDate());
    toHash += ft.format(booking.getCreationDate());
    toHash += booking.getSpectacle().getName();
    toHash += booking.getTotalPrice();
    return toHash.hashCode();
  }
 
  private LinkedList<Sale> getSales(HttpServletRequest request) {
    Object o = request.getSession().getAttribute("sales");
    if(o!=null) {
      return (LinkedList<Sale>)o;
    }
    return null;
  }
  
  private LinkedList<Case> getCases(HttpServletRequest request) {
    Object o = request.getSession().getAttribute("cases");
    if(o!=null) {
      return (LinkedList<Case>)o;
    }
    return null;
  }
  
}
