/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.servlets;

import fr.ensimag.webTheater.beans.User;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.form.ConnectionForm;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author xavier
 */
public class Connection extends HttpServlet {
  
  public static final String VIEW = "/WEB-INF/jsp/index.jsp";
  public static final String ATT_USER = "user";
  public static final String ATT_FORM = "form";
  public static final String ATT_SESSION_USER = "sessionUser";
  
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    this.getServletContext().getRequestDispatcher(VIEW).forward( request, response );
  }
  
  
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    
    ConnectionForm form = new ConnectionForm(
            (DAOFactory) getServletContext().getAttribute("DAOFactory"));
    // Return Administrator or Customer depending on the account
    User user = form.connectUser(request);
    
    /* Create or recover session */
    HttpSession session = request.getSession();
    
    /* If the user succed connection, add the bean to the session */
    if (form.getErrors().isEmpty()){
      session.setAttribute(ATT_SESSION_USER, user);
    } else {
      session.setAttribute(ATT_SESSION_USER, null);
    }
    
    request.setAttribute(ATT_FORM, form);
    request.setAttribute(ATT_USER, user);
    
    this.getServletContext().getRequestDispatcher(VIEW).forward( request, response );
  }
  
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet Inscription.java";
  }
  
  
}
