/*
 * The MIT License
 *
 * Copyright 2015 louis.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.servlets;

import fr.ensimag.webTheater.admin.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.ensimag.webTheater.beans.*;
import fr.ensimag.webTheater.dao.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author louis
 */
public class AdminPage extends HttpServlet {

    public static final String VIEW = "/WEB-INF/jsp/adminPage.jsp";
    public static final String FOLLOW = "/adminPage";

    public static final String NEW_SPECTACLE = "new_spectacle";
    public static final String NEW_REPRESENTATION = "new_representation";

    private static Manager manager;
    
    public AdminPage(){
        manager = new Manager();
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //manager = new Manager();
        String strSpectacle = manager.showSpectacle(request);
        String strRoom = manager.showRoom(request);
        String strCat = manager.showCat(request);
        

        request.setAttribute("strSpectacle", strSpectacle);
        request.setAttribute("strRoom", strRoom);
        request.setAttribute("strCat", strCat);
        request.setAttribute("manager", manager);

        this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String new_spectacle = "2";
        String new_representation = "2";
        new_spectacle = request.getParameter(NEW_SPECTACLE);
        new_representation = request.getParameter(NEW_REPRESENTATION);
        Spectacle currSpectacle = new Spectacle();
        Representation currRepresentation = new Representation();
        String strRepresentation = "";
        String strRoom = "";

        //Manager manager = new Manager();
        if ("1".equals(new_spectacle)) {
            currSpectacle = manager.newSpectacle();
        } else if ("1".equals(new_representation)) {
            currRepresentation = manager.newRepresentation();
            strRoom = manager.showRoom(request);
            strRepresentation = manager.showRepresentation(request);
        } else if ("0".equals(new_spectacle)) {
            manager.manageSpectacle(request);
            strRepresentation = manager.showRepresentation(request);
        } else if ("0".equals(new_representation)) {
          try {
            manager.manageRepresentation(request);
          } catch (ParseException ex) {
            Logger.getLogger(AdminPage.class.getName()).log(Level.SEVERE, null, ex);
          }
            strRoom = manager.showRoom(request);
            strRepresentation = manager.showRepresentation(request);
        } else {
            strRepresentation = manager.showRepresentation(request);
        }

        request.setAttribute("strRepresentation", strRepresentation);
        request.setAttribute("strRoom", strRoom);
        //request.setAttribute("manager", manager);
        //request.setAttribute("spectacle", currSpectacle);
        //request.setAttribute("representation", currRepresentation);

        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet for Admin Page : adminPage.java";
    }

}
