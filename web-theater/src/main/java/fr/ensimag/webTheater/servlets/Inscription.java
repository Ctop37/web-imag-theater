/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.servlets;

import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.form.InscriptionForm;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author xavier
 */
public class Inscription extends HttpServlet {
  
  public static final String INSCRIPTION_PAGE = "/WEB-INF/jsp/inscription.jsp";
  public static final String INDEX_PAGE = "/index.jsp";
  public static final String ATT_USER = "user";
  public static final String ATT_FORM = "form";
  public static final String ATT_SESSION_USER = "sessionUser";
  
  
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    this.getServletContext().getRequestDispatcher(INSCRIPTION_PAGE).forward( request, response );
  }
  
  
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    InscriptionForm form = new InscriptionForm(
            (DAOFactory) getServletContext().getAttribute("DAOFactory"));
    Customer customer = null;
    try {
      customer = form.registerUser(request);
    } catch (Exception e) {
      throw new ServletException(e.getMessage());
    }
    request.setAttribute(ATT_FORM, form);
    request.setAttribute(ATT_USER, customer);
    
    /* If the user succed the inscription, we authenticate the new user */
    HttpSession session = request.getSession();
    if (form.getErrors().isEmpty()){
      session.setAttribute(ATT_SESSION_USER, customer);
      this.getServletContext().getRequestDispatcher(INDEX_PAGE).forward(request, response);
    } else {
      session.setAttribute(ATT_SESSION_USER, null);
    }
    
    this.getServletContext().getRequestDispatcher(INSCRIPTION_PAGE).forward(request, response);
  }
  
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet Inscription.java";
  }
  
  
}
