/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.servlets;

import com.lowagie.text.DocumentException;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.util.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author xavier
 * @author jean-david
 */
public class Invoice extends HttpServlet {
  
  
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/pdf");
    OutputStream out = response.getOutputStream() ;
    HttpSession session = request.getSession(true) ;
    
    Object o = session.getAttribute("cases");
    LinkedList<Case> cases = new LinkedList<Case>();
    
    if(o!=null) {
      cases = (LinkedList<Case>) o;      
    } else {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }
    
    Object o2 = session.getAttribute("sales");
    LinkedList<Sale> sales = new LinkedList<Sale>();
    
    if(o2!=null) {
      sales = (LinkedList<Sale>) o2;      
    } else {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }
    
    String idToParse = request.getParameter("id");
    int id = 0;
    if(idToParse!=null) {
      id = Integer.parseInt(idToParse);
    } else {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }
    
    Case currentCase = cases.get(id);
    Sale theSale = null;
    Iterator <Sale> saleIt = sales.iterator();
    
    SimpleDateFormat ft = new SimpleDateFormat("HHddMMyy");
    SimpleDateFormat ft2 = new SimpleDateFormat("dd/MM/yy à HH");
    
    while(saleIt.hasNext()) {
      Sale currentSale = saleIt.next();
      if(ft.format(currentSale.getDate()).equals(ft.format(currentCase.getDate()))) {
        theSale = currentSale;
        break;
      }
    }
    
    try{
      PDFInvoice.generate(
              out, 
              theSale, 
              currentCase, 
              currentCase.getSpectacleName(), 
              currentCase.getRoomName(), 
              ft2.format(currentCase.getDate())+"h");
    }catch(DocumentException e){
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }

  }
  
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
  }
 
  
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Invoice servlet";
  }
  
  
}



