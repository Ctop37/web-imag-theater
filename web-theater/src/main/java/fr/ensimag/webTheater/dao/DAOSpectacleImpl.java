/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Spectacle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;


public class DAOSpectacleImpl implements DAOSpectacle {

    private static final String SQL_INSERT =
            "INSERT INTO Spectacle VALUES (?,?,?,?)";
    private static final String SQL_FIND =
            "SELECT * FROM Spectacle WHERE spectacleNumber=?";
    private static final String SQL_DELETE =
            "DELETE FROM Spectacle WHERE spectacleNumber=?";
    private static final String SQL_FIND_REPRESENTATIONS =
            "SELECT * FROM Representation WHERE spectacleNumber=?";
    
    private static final String SQL_FIND_SPECTACLES = 
            "SELECT * FROM Spectacle";
    private final DAOFactory daoFactory;
    
    public DAOSpectacleImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    @Override
    public void create(Spectacle spectacle) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT, false,
                    spectacle.getNumber(),
                    spectacle.getName(),
                    spectacle.getImagePath(),
                    spectacle.getMailAdmin());
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at inserting a Spectacle.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Spectacle find(int spectacleNumber) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Spectacle spectacle = null;
        
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_FIND, false, spectacleNumber);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                spectacle = map(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return spectacle;

    }

    @Override
    public void findRepresentations(Spectacle spectacle) throws DAOException {
        if (spectacle.getNumber() == 0) {
            throw new DAOException("spectacle number undefined");
        }
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        HashSet<Representation> representations = null;
        
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_FIND_REPRESENTATIONS, false, spectacle.getNumber());
            resultSet = preparedStatement.executeQuery();
            representations = new HashSet<>();
            while (resultSet.next()) {
                Representation rep = new Representation();
                rep.setSpectacleNumber(resultSet.getInt("spectacleNumber"));
                rep.setDate(dAODateImpl.dateOfString(
                        resultSet.getString("dateHour")));
                rep.setRoomName(resultSet.getString("roomName"));
                rep.setIsWithdrawn(resultSet.getBoolean("isWithdrawn"));
                representations.add(rep);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        spectacle.setRepresentations(representations);
    }

    private Spectacle map(ResultSet resultSet) throws SQLException {
        Spectacle spectacle = new Spectacle();
        spectacle.setNumber(resultSet.getInt("spectacleNumber"));
        spectacle.setName(resultSet.getString("spectacleName"));
        spectacle.setImagePath(resultSet.getString("spectaclePicture"));
        spectacle.setMailAdmin(resultSet.getString("userMail"));
        return spectacle;
    }

    @Override
    public void delete(int spectacleNumber) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false, spectacleNumber);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at deleting a Spectacle.");
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Set<Spectacle> findAll() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Set<Spectacle> result = null;
        
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_FIND_SPECTACLES, false);
            resultSet = preparedStatement.executeQuery();
            result = new HashSet<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return result;
    }
}
