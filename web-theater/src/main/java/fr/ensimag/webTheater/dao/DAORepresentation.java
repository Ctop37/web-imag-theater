/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Seat;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author thibaut
 */
public interface DAORepresentation {
    
    /**
     * Being given a Representation bean, this method add it to the database.
     * @param representation
     * @throws DAOException 
     */
    void create(Representation representation) throws DAOException;
    
    /**
     * Being given a Representation bean, this method put the boolean
     * isWithdrawn at false in the database.
     * @param representation
     * @throws DAOException 
     */
    void cancel(Representation representation) throws DAOException;
    
    /**
     * Being given a Representation bean, this method searches it in the
     * database and actualizes the boolean isWithdrawn of the bean when it has
     * been changed in the database.
     * It is equivalent to a kind of method "find": given the key, we return
     * the data.
     * @param representation
     * @throws DAOException 
     */
    void actualize(Representation representation) throws DAOException;
    
    /**
     * Being given a Representation bean, this method delete it in the database.
     * It will be used at the end of the year when we erase the database.
     * @param representation
     * @throws DAOException 
     */
    void delete(Representation representation) throws DAOException;
    
  /**
   * Retrieves the Seats (ordered by categories which they belong to) available
   * for a Representation
   * @param representation
   * @return
   * @throws DAOException
   */
  HashMap<Category, LinkedList<Seat>> findSeats(Representation representation)
      throws DAOException;
}
