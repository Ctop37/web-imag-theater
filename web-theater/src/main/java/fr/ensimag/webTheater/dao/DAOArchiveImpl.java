/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DAOArchiveImpl implements DAOArchive {

    private static final String SQL_SELECT_BOOKING=
            "SELECT * FROM Bookings WHERE dateHour >= ? AND dateHour <= ?";
    
    private static final String SQL_DELETE_BOOKING=
            "DELETE FROM Bookings WHERE dateHour >= ? AND dateHour <= ?";
    
    private static final String SQL_SELECT_SALE_CASE=
            "SELECT c.caseNumber, c.ticketNumber, c.userMail, s.dateHour, "
            + "s.roomName, s.rowNumber, s.seatNumber "
            + "FROM Sale s, Case_ c "
            + "WHERE s.caseNumber = c.caseNumber "
            + "AND s.dateHour >= ? AND s.dateHour <= ?";

    private static final String SQL_DELETE_SALE=
            "DELETE FROM Sale WHERE dateHour >= ? AND dateHour <= ?";
        
    private static final String SQL_DELETE_CASE=
            "DELETE FROM Case WHERE caseNumber = ?";
    
    private static final String SQL_SELECT_REPRESENTATION=
            "SELECT * FROM Representation WHERE dateHour >= ? AND dateHour <= ?";
    
    private static final String SQL_DELETE_REPRESENTATION=
            "DELETE FROM Representation WHERE dateHour >= ? AND dateHour <= ?";
    
    private static final String SQL_DELETE_DATE=
            "DELETE FROM DateHour WHERE dateHour >= ? AND dateHour <= ?";
    
    private final DAOFactory daoFactory;

    public DAOArchiveImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    @Override
    public String archive(Date dateDebut, Date dateFin) throws DAOException {
        String sortie= "";
        DAODateImpl daoDateImpl = new DAODateImpl(daoFactory);
        String sDateDebut = daoDateImpl.stringOfDate(dateDebut);
        String sDateFin = daoDateImpl.stringOfDate(dateFin);
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            
            // Récuperation des Bookings:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_BOOKING, false,
                    sDateDebut, sDateFin);
            resultSet = preparedStatement.executeQuery();
            sortie = sortie + mapBooking(resultSet);
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
            // Suppression des Bookings:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_BOOKING, false,
                    sDateDebut, sDateFin);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);
            
            // Récupération des Sales et des Cases:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_SALE_CASE, false,
                    sDateDebut, sDateFin);
            resultSet = preparedStatement.executeQuery();
            HashSet<Long> caseNumbers = new HashSet<>();
            sortie = sortie + mapSaleCase(resultSet, caseNumbers);
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
            
            // Suppression des Sales:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_SALE, false,
                    sDateDebut, sDateFin);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);
            
            // Suppression des Cases:
            Iterator<Long> iterator = caseNumbers.iterator();
            while (iterator.hasNext()) {
                preparedStatement = DAOUtility.initializePreparedRequest(
                        connection, SQL_DELETE_CASE, false,
                        iterator.next());
                preparedStatement.executeUpdate();
                DAOUtility.silentClosure(preparedStatement);
                }
            
            // Récuperation des representations:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_REPRESENTATION, false,
                    sDateDebut, sDateFin);
            resultSet = preparedStatement.executeQuery();
            sortie = sortie + mapRepresentation(resultSet);
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);

            // Suppression des representations:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_REPRESENTATION, false,
                    sDateDebut, sDateFin);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);

            // Suppressions des dates:
                        preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_DATE, false,
                    sDateDebut, sDateFin);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(DAOArchiveImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return sortie;
    }

    /**
     * Being given a ResultSet containg Tuples with fields: 
     *   userMail, dateHour, roomName, rowNumber, seatNumber
     * This method writes a string including this data.
     * @param resultSet
     * @return 
     */
    private String mapBooking(ResultSet resultSet) throws SQLException {
        String sortie = "Set of Booking:\n";
        sortie = sortie + "userMail of the Customer,  date of Booking, "
                + " name of the Room,  number of row, number of seat:\n";
        while (resultSet.next()) {
            String userMail = resultSet.getString("userMail");
            String date = resultSet.getString("dateHour");
            String roomName = resultSet.getString("roomName");
            int rowNumber = resultSet.getInt("rowNumber");
            int seatNumber = resultSet.getInt("seatNumber");
            sortie = sortie + userMail+",  "+date+",  "+roomName+",  "+
                    rowNumber+",  "+seatNumber+"\n";
        }
        return sortie + "\n\n";
    }

    /**
     * Being given a ResultSet containing Tuples with fields:
     * userMail, dateHour, roomName, rowNumber, seatNumber, ticketNumber, caseNumber
     * This method writes a string including this data.
     * This method also fill the HashSet with the caseNumbers.
     * @param resultSet
     * @param caseNumbers
     * @return 
     */
    private String mapSaleCase(ResultSet resultSet, HashSet<Long> caseNumbers) throws SQLException {
        String sortie = "Set of Sales and there Cases:\n";
        sortie = sortie + "userMail of the Customer, date of Booking, "
                + " name of the Room, number of row, number of seat, "
                + "number of ticket, number of case:\n";
        while (resultSet.next()) {
            String userMail = resultSet.getString("userMail");
            String date = resultSet.getString("dateHour");
            String roomName = resultSet.getString("roomName");
            int rowNumber = resultSet.getInt("rowNumber");
            int seatNumber = resultSet.getInt("seatNumber");
            long ticketNumber = resultSet.getInt("ticketNumber");
            long caseNumber = resultSet.getInt("caseNumber");
            sortie = sortie + userMail+",  "+date+",  "+roomName+",  "+
                    rowNumber+",  "+seatNumber+",  "+ ticketNumber+",  "
                    +caseNumber+"\n";
            caseNumbers.add(caseNumber);
        }
        return sortie + "\n\n";    }

    /**
     * Being given a ResultSet containing Tuples with fields:
     *   spectacleNumber, dateHour, roomName, isWithdrawn
     * This method writes a string including this data.
     * @param resultSet
     * @return
     * @throws SQLException 
     */
    private String mapRepresentation(ResultSet resultSet) throws SQLException {
        String sortie = "Set of Representations:\n";
        sortie = sortie + "Number of spectacle,  date of Representation, "
                + " name of the Room,  is canceled:\n";
        while (resultSet.next()) {
            int spectacleNumber = resultSet.getInt("spectacleNumber");
            String date = resultSet.getString("dateHour");
            String roomName = resultSet.getString("roomName");
            boolean isCanceled = ! resultSet.getBoolean("isWithdrawn");
            sortie = sortie + spectacleNumber+",  "+date+",  "+roomName+",  "
                    +isCanceled+"\n";
        }
        return sortie + "\n\n";}
    
}
