/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Administrator;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.User;

/**
 * This interface concerns creation, searching and the deleting of users.
 * A user can be a customer, an administrator or both of it.
 * As an user of this interface, you should use only the method in this
 * interface and not using the methods create, find and delete which concerns
 * Users instead of Customer or Administrator. These three methods are public
 * only for debugging.
 * @author thibaut
 */
public interface DAOUser {
    
    /**
     * Return null or a User presents in the database, having this e-mail.
     * This method never return a Customer or an Administrator, even if there
     * is in the database a Customer or an Adminitrator having this e-mail.
     * @param mail
     * @return
     * @throws DAOException 
     */
    User find(String mail) throws DAOException;

    /**
     * Create a User with his properties:
     *   userMail
     *   First Name
     *   Last Name
     *   Password
     * Create then a Costumer ie a Tuple containing his userMail.
     * @param customer
     * @throws DAOException 
     */
    void createCustomer(Customer customer) throws DAOException;
    
    /**
     * This method finds a Customer (and the User representing it) in the
     * database and complete his User attributs:
     *   userMail
     *   First Name
     *   Last Name
     *   Password
     * @param mail
     * @return
     * @throws DAOException 
     */
    Customer findCustomer(String mail) throws DAOException;
    
    /**
     * Be careful: You can't delete a User having a Booking or a Sale not
     * deleted.
     * @param mail
     * @throws DAOException 
     */
    void deleteCustomer(String mail) throws DAOException;

    /**
     * Create an administrator with his properties:
     *   userMail
     *   First Name
     *   Last Name
     *   Password
     * Create then an Administrator ie a Tuple containing his userMail.
     * @param mail
     * @return
     * @throws DAOException 
     */
    void createAdministrator(Administrator administrator) throws DAOException;
    
    /**
     * This method finds an Administrator (and the User representing it) in the
     * database and complete his User attributs:
     *   userMail
     *   First Name
     *   Last Name
     *   Password
     * @param mail
     * @return
     * @throws DAOException 
     */
    Administrator findAdministrator(String mail) throws DAOException;
    
    /**
     * Be careful: You can't delete an Administrator having created a Spectacle.
     * @param mail
     * @throws DAOException 
     */
    void deleteAdministrator(String mail) throws DAOException;

}