/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * inspired of OpenClassrooms
 * @author thibaut
 */
public final class DAOUtility {

    // Builder:
    private DAOUtility() {return;}
    
    /* Silent closing down of the resultset */
    public static void silentClosure( ResultSet resultSet ) {
        if ( resultSet != null ) {
            try {
                resultSet.close();
            } catch ( SQLException e ) {
                System.out.println( "Failure in closing the ResultSet : "
                        + e.getMessage() );
            }
        }
    }

    /* Silent closing down of the statement */
    public static void silentClosure( Statement statement ) {
        if ( statement != null ) {
            try {
                statement.close();
            } catch ( SQLException e ) {
                System.out.println("Failure in closing the Statement : "
                        + e.getMessage() );
            }
        }
    }

    /* Silent closing down of the connection */
    public static void silentClosure( Connection connexion ) {
        if ( connexion != null ) {
            try {
                connexion.close();
            } catch ( SQLException e ) {
                System.out.println( "Failure in closing the connection : "
                        + e.getMessage() );
            }
        }
    }

    /* Silent closing down of statement and connection */
    public static void silentClosure(Statement statement, Connection connexion){
        silentClosure( statement );
        silentClosure( connexion );
    }

    /* Silent closing down of resultset, statement and connection */
    public static void silentClosure( ResultSet resultSet, Statement statement,
            Connection connexion ) {
        DAOUtility.silentClosure( resultSet );
        silentClosure( statement );
        silentClosure( connexion );
    }

    /*
     * Initialize the prepared request based on the connection given in argument
     * with the SQL request and objects given.
     */
    public static PreparedStatement initializePreparedRequest(
            Connection connection, String sql, boolean returnGeneratedKeys,
            Object... objets ) throws SQLException {
        
        PreparedStatement preparedStatement = 
            connection.prepareStatement( sql,
            returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS 
            : Statement.NO_GENERATED_KEYS );
        
        for ( int i = 0; i < objets.length; i++ ) {
            preparedStatement.setObject( i + 1, objets[i] );
        }
        return preparedStatement;
    }
}