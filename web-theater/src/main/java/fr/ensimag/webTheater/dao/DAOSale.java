/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.beans.Seat;
import java.util.Date;

/**
 *
 * @author thibaut
 */
public interface DAOSale {

    /**
     * Being given a Sale bean, this method adds it to the database.
     * Deprecated.
     * @param sale
     * @throws DAOException
     */
    void create(Sale sale) throws DAOException;
    
    /**
     * Beeing given a booking and a Case, this method delete the booking
     * in the database and create a Sale in the same transaction.
     * That corresponds to the following situation:
     * A customer has booked a set of seats for a representation and he would
     * like to buy these.
     * This method requires that:
     *    the booking were in the database
     *    the Case were in the database
     *    the Case and the booking corresponds to the same customerMail
     * This method verifies that:
     *    the Case and the booking corresponds to the same customerMail
     *    the corresponding representation exists in the database, and is not
     * withdrawn.
     * This method doesn't verifies that it remains 70 available seats at the
     * end of the operation since this transaction doesn't change the number of
     * remaining seats. Indeed, we delete a booking while creating a Sale and
     * the verification have ever been done for the creation of the Booking.
     * @param booking
     * @param c
     * @return
     * @throws DAOException 
     */
    Sale createFromBooking(Booking booking, Case c) throws DAOException;
    
    /**
     * I don't see when we could have to delete a Sale. However, we will have to
     * delete a triplet CaseNumber, date, seat.
     * @param caseNumber
     * @param date
     * @param seat
     * @throws DAOException 
     */
    void delete(long caseNumber, Date date, Seat seat) throws DAOException;
}
