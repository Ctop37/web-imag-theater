/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Spectacle;
import java.util.Set;

/**
 *
 * @author thibaut
 */
public interface DAOSpectacle {
    
    /**
     * Create one Tuple containing:
     *   spectacleNumber
     *   spectacleName
     *   spectaclePicture        (can be null)
     *   userMail of the Administrator creating this spectacle.
     * @param spectacle
     * @throws DAOException 
     */
    void create(Spectacle spectacle) throws DAOException;
    
    
    /**
     * make a Spectacle bean containing all the informations:
     *   spectacleNumber
     *   spectacleName
     *   spectaclePicture        (can be null)
     *   userMail of the Administrator having created this spectacle. (can be null)
     * NB: the Representations stay at null in this bean.
     * @param spectacleNumber
     * @return
     * @throws DAOException 
     */
    Spectacle find(int spectacleNumber) throws DAOException;
    
    /**
     * delete the Spectacle representing by spectacleNumber in the database.
     * @param spectacleNumber
     * @throws DAOException 
     */
    void delete(int spectacleNumber) throws DAOException;
    
    /**
     * This method adds to an existing Spectacle bean the Representations.
     * @param spectacleNumber
     * @throws DAOException 
     */
    void findRepresentations(Spectacle spectacle) throws DAOException;

    /**
     * @return all the name of the spectacle present in the database.
     * The Representation Set in Spectacle Bean is not filled. 
     * @throws DAOException 
     */
    Set<Spectacle> findAll() throws DAOException;

    
}