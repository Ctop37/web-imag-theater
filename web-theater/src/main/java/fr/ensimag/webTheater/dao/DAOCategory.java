/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Category;

/**
 *
 * @author thibaut
 */
public interface DAOCategory {
    
    /**
     * Being given a Category bean, this method add it to the database.
     * @param category
     * @throws DAOException
     */
    void create(Category category) throws DAOException;

    /**
     * Being given a categoryName, this method return the Category bean from
     * the database.
     * @param categoryName
     * @return
     * @throws DAOException
     */
    Category find(String categoryName) throws DAOException;

    /**
     * Being given a categoryName, this method delete the Category in the
     * database.
     * @param categoryName
     * @throws DAOException
     */
    void delete(String categoryName) throws DAOException;
}
