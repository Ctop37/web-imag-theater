/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Room;
import java.util.Set;

/**
 *
 * @author thibaut
 */
public interface DAORoom {
    
    /**
     * Being given a Room bean, this method create in the database a complete
     * room, with its 10 rows from the number 1 to the number 10 and their
     * categories, and each with their 20 seats from number 1 to number 20.
     * @param room
     * @throws DAOException
     */
    void create(Room room) throws DAOException;
    
    /**
     * Delete all the rooms (including their rows and theirs seats).
     * @throws DAOException
     */
    void deleteAll() throws DAOException;
    
    /**
     * 
     * @return all the name of the rooms present in the database.
     * @throws DAOException 
     */
    Set<String> findAll() throws DAOException;
}
