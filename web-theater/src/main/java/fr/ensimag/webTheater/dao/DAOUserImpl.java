/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Administrator;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thibaut
 */
public class DAOUserImpl implements DAOUser {

    private static final String SQL_SELECT_MAIL =
            "SELECT userMail, userFirstName, userLastName, userPassword "
          + "FROM User WHERE userMail = ? ";
    private static final String SQL_INSERT_USER =
            "INSERT INTO User VALUES (?,?,?,?)";
    private static final String SQL_DELETE_USER =
            "DELETE FROM User WHERE userMail=?";
    private static final String SQL_SELECT_MAIL_CUSTOMER =
            "SELECT userMail, userFirstName, userLastName, userPassword "
          + "FROM User WHERE userMail IN (SELECT userMail FROM Customer "
            + "WHERE userMail=?)";
    private static final String SQL_SELECT_MAIL_ADMINISTRATOR = 
            "SELECT userMail, userFirstName, userLastName, userPassword "
          + "FROM User WHERE userMail IN (SELECT userMail FROM Administrator "
            + "WHERE userMail=?)";
    private static final String SQL_INSERT_CUSTOMER =
            "INSERT INTO Customer VALUES (?)";
    private static final String SQL_INSERT_ADMINISTRATOR =
            "INSERT INTO Administrator VALUES (?)";
    private static final String SQL_DELETE_CUSTOMER =
            "DELETE FROM Customer WHERE userMail=?";
    private static final String SQL_DELETE_ADMINISTRATOR =
            "DELETE FROM Administrator WHERE userMail=?";
    private final DAOFactory daoFactory;
    
    public DAOUserImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    public void create(User user) throws DAOException {
        Connection connection = null;
        try {
            connection = daoFactory.getConnection();
            createUser(connection, user);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(connection);
        }
    }

    private void createUser(Connection connection, User user) throws SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                connection, SQL_INSERT_USER, false,
                user.getEmail(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword());
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at inserting a User.");
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }
    }
    
    @Override
    public User find(String mail) throws DAOException {
        Connection connection = null;
        User user = null;
        try {
            connection = daoFactory.getConnection();
            user = findUser(connection, mail);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(connection);
        }
        return user;
    }

    private User findUser(Connection connection, String mail) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_MAIL, false, mail);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = map(resultSet, new User());
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
        }
        return user;
    }
    
    
    private User map(ResultSet resultSet, User user) throws SQLException {
        String userMail = resultSet.getString("userMail");
        String userFirstName = resultSet.getString("userFirstName");
        String userLastName = resultSet.getString("userLastName");
        String userPassword = resultSet.getString("userPassword");
        
        user.setEmail(userMail);
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setPassword(userPassword);
        
        return user;
    }

    public void delete(String mail) throws DAOException {
        Connection connection = null;
        try {
            connection = daoFactory.getConnection();
            deleteUser(connection, mail);
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(connection);
        }
    }

    private void deleteUser(Connection connection, String mail) throws SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_USER, false, mail);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at deleting an User."
                        + "Perhaps this user doesn't exist.");
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }
    }
    
    @Override
    public void createCustomer(Customer customer) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        if (customer.getEmail() == null) {
            throw new DAOException("email null");
        }

        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            // If the customer isn't in the table of User, we add him.
            User user = findUser(connection, customer.getEmail());
            if (user == null) {
                createUser(connection, customer);
            } else {
                // If the customer was in the table of User, we ensure it is the good profil.
                if (!user.getFirstName().equals(customer.getFirstName()) ||
                    !user.getLastName().equals(customer.getLastName()) ||
                    !user.getPassword().equals(customer.getPassword())) {
                    throw new SQLException("You try to create a Customer having an "
                        + "e-mail ever presented in the database, but a profil"
                        + " differents.");
                }
            }

            // We add the customer to the table of Customers:
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT_CUSTOMER, false, customer.getEmail());
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at inserting a Customer.");
            }
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(DAOUserImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Customer findCustomer(String mail) throws DAOException {
        Connection connection = null;
        Customer customer = null;        
        try {
            connection = daoFactory.getConnection();
            customer = findCustomerConnection(connection, mail);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(connection);
        }
        return customer;
    }
    
    private Customer findCustomerConnection(Connection connection, String mail)
            throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Customer customer = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_MAIL_CUSTOMER, false, mail);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                customer = (Customer) map(resultSet, new Customer());
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
        }
        return customer;  
    }

    @Override
    public void deleteCustomer(String mail) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_CUSTOMER, false, mail);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at deleting a Customer."
                        + "Perhaps this customer doesn't exist.");
            }
            // If the Customer isn't also a Administrator, we delete the user:
            if (findAdministratorConnection(connection, mail) == null) {
                deleteUser(connection, mail);
            }
            connection.commit();
        } catch (SQLException ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(DAOUserImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public void createAdministrator(Administrator administrator) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        if (administrator.getEmail() == null) {
            throw new DAOException("email null");
        }
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            // If the administrator isn't in the table of User, we add him.
            User user = findUser(connection, administrator.getEmail());
            if (user == null) {
                createUser(connection, administrator);
            } else {
            // If the administrator was in the table of User, we ensure it is the good profil.
                if (!user.getFirstName().equals(administrator.getFirstName()) ||
                    !user.getLastName().equals(administrator.getLastName()) ||
                    !user.getPassword().equals(administrator.getPassword())
                    ) {
                    throw new SQLException("You try to create an Administrator having "
                        + "an e-mail ever presents in the database, but a profil"
                        + " differents.");
                }
            }

            // We add the administrator to the table of Customers:
            preparedStatement = DAOUtility.initializePreparedRequest(connection,
                    SQL_INSERT_ADMINISTRATOR,false,administrator.getEmail());
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at inserting an Administrator");
            }
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(DAOUserImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Administrator findAdministrator(String mail) throws DAOException {
        Connection connection = null;
        Administrator administrator = null;        
        try {
            connection = daoFactory.getConnection();
            administrator = findAdministratorConnection(connection, mail);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(connection);
        }
        return administrator;
    }

    private Administrator findAdministratorConnection(Connection connection,
            String mail) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Administrator administrator = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_MAIL_ADMINISTRATOR, false, mail);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                administrator = (Administrator) map(resultSet, new Administrator());
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
        }
        return administrator;
    }
    
    @Override
    public void deleteAdministrator(String mail) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_ADMINISTRATOR, false, mail);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at deleting a Administrator."
                        + "Perhaps this administrator doesn't exist.");
            }
                    // If the Administrator isn't also a Customer, we delete the user:
            if (findCustomerConnection(connection, mail) == null) {
                deleteUser(connection, mail);
            }
            connection.commit();
        } catch (SQLException ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(DAOUserImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
        
    }
    
}
