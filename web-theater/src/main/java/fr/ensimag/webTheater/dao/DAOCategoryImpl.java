/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DAOCategoryImpl implements DAOCategory {

    private static final String SQL_INSERT=
            "INSERT INTO Category VALUES (?,?)";
    private static final String SQL_SELECT=
            "SELECT * FROM Category WHERE categoryName=?";
    private static final String SQL_DELETE=
            "DELETE FROM Category WHERE categoryName=?";
    
    private final DAOFactory daoFactory;
    
    public DAOCategoryImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    @Override
    public void create(Category category) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT, false,
                    category.getName(),
                    category.getPrice());
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at inserting a Category.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }

    }

    @Override
    public Category find(String categoryName) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Category category = null;
        
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT, false, categoryName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                category = new Category();
                category.setName(resultSet.getString("categoryName"));
                category.setPrice(resultSet.getDouble("tariff"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return category;

    }

    @Override
    public void delete(String categoryName) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false, categoryName);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at deleting a Category.");
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }
    
}
