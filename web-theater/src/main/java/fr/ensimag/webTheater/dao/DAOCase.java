/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Case;

/**
 *
 * @author thibaut
 */
public interface DAOCase {

    /**
     * Being given a Case bean, this method add it to the database.
     * @param c
     * @throws DAOException
     */
    void create(Case c) throws DAOException;

    /**
     * Being given an id, this method find in the database the corresponding
     * Case bean.
     * @param id
     * @return
     * @throws DAOException
     */
    Case find(long caseNumber) throws DAOException;

    /**
     * Being given an id, this method delete in the database the corresponding
     * Case.
     * @param id
     * @throws DAOException
     */
    void delete(long caseNumber) throws DAOException;
}
