/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.beans.Seat;
import static fr.ensimag.webTheater.dao.DAOBookingImpl.deleteBooking;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOSaleImpl implements DAOSale {

    private static final String SQL_INSERT
            = "INSERT INTO Sale VALUES (?,?,?,?,?)";

    private static final String SQL_DELETE
            = "DELETE FROM Sale WHERE caseNumber=? AND dateHour=? AND "
            + "roomName=? AND rowNumber=? AND seatNumber=?";
    private final DAOFactory daoFactory;

    public DAOSaleImpl(DAOFactory daofactory) {
        this.daoFactory = daofactory;
    }

    @Override
    public void create(Sale sale) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(sale.getDate());
        Set<Seat> seats = sale.getSeats();
        Iterator<Seat> iterator = seats.iterator();
        Connection connection = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            while (iterator.hasNext()) {
                Seat seat = iterator.next();
                createSale(connection,
                        sale.getIdCase(),
                        sDate,
                        seat.getRow().getRoom().getName(),
                        seat.getRow().getRowNumber(),
                        seat.getId());
                int statut;
            }
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(DAOSaleImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(connection);
        }
    }

    public static void createSale(Connection connection,
            long caseNumber, String sDate, String roomName, int rowNumber,
            int seatNumber) throws SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT, false,
                    caseNumber, sDate, roomName, rowNumber, seatNumber);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at inserting a Sale.");
            }
        } catch (SQLException ex) {
            throw new SQLException("failure in createSale", ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }

    }

    @Override
    public void delete(long caseNumber, Date date, Seat seat) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(date);
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false,
                    caseNumber,
                    sDate,
                    seat.getRow().getRoom().getName(),
                    seat.getRow().getRowNumber(),
                    seat.getId());
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at deleting a Sale.");
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Sale createFromBooking(Booking booking, Case c) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(booking.getDate());
        Iterator<Seat> iterator = booking.getSeats().iterator();
        String roomName = iterator.next().getRow().getRoom().getName();
        int spectacleNumber;
        Connection connection = null;
        Sale sale = null;

        if (! booking.getUserMail().equals(c.getUserMail())) {
            throw new DAOException("the booking and the Case don't correspond "
                    + "to the same customerMail");
        }
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            spectacleNumber = DAORepresentationImpl.spectacleNumber(
                    connection, sDate, roomName);
            Boolean isWithdrawn = DAORepresentationImpl.isAlwaysWithdrawn(
                    connection, sDate, roomName, spectacleNumber);
            if (!isWithdrawn) {
                throw new SQLException("The representation isn't withdrawn");
            }
            iterator = booking.getSeats().iterator();
            while (iterator.hasNext()) {
                Seat seat = iterator.next();
                deleteBooking(connection,
                        booking.getUserMail(),
                        sDate,
                        roomName,
                        seat.getRow().getRowNumber(),
                        seat.getId());
                createSale(connection,
                        c.getNumber(),
                        sDate,
                        roomName,
                        seat.getRow().getRowNumber(),
                        seat.getId());
            }
            connection.commit();

            sale = new Sale();
            sale.setDate(booking.getDate());
            sale.setIdCase(c.getNumber());
            sale.setSeats(booking.getSeats());
        } catch (SQLException ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(DAOSaleImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(connection);
        }
        return sale;
    }
}
