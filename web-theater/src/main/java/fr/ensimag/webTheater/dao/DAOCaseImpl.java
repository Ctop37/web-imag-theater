/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Case;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DAOCaseImpl implements DAOCase {

    private static final String SQL_INSERT=
            "INSERT INTO Case_ VALUES (?,?,?)";
    private static final String SQL_SELECT=
            "SELECT * FROM Case_ WHERE caseNumber=?";
    private static final String SQL_DELETE=
            "DELETE FROM Case_ WHERE caseNumber=?";
    
    private final DAOFactory daoFactory;
    
    public DAOCaseImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    

    @Override
    public void create(Case c) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT, false,
                    c.getNumber(),
                    c.getTicketNumber(),
                    c.getUserMail());
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at inserting a Case.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Case find(long caseNumber) throws DAOException {
                Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Case c = null;
        
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT, false, caseNumber);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                c = new Case();
                c.setNumber(resultSet.getLong("caseNumber"));
                c.setTicketNumber(resultSet.getLong("ticketNumber"));
                c.setUserMail(resultSet.getString("userMail"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return c;
    }

    @Override
    public void delete(long caseNumber) throws DAOException {
                Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false, caseNumber);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at deleting a Case.");
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }    
}
