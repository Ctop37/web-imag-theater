/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import fr.ensimag.webTheater.beans.Seat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DAORepresentationImpl implements DAORepresentation {

    private static final String SQL_INSERT=
            "INSERT INTO Representation VALUES (?,?,?,?)";
    private static final String SQL_CANCEL=
            "UPDATE Representation SET isWithdrawn=false WHERE "
            + "SpectacleNumber=? AND dateHour=? AND roomName=?";
    private static final String SQL_SELECT=
            "SELECT isWithdrawn FROM Representation WHERE "
            + "SpectacleNumber=? AND dateHour=? AND roomName=?";
    private static final String SQL_DELETE=
            "DELETE FROM Representation WHERE "
            + "SpectacleNumber=? AND dateHour=? AND roomName=?";
    
    private static final String SQL_FIND_SPECTACLE_NUMBER=
            "SELECT Representation.spectacleNumber "
            + "FROM Representation "
            + "WHERE Representation.dateHour=? AND Representation.roomName=?";
    
    private static final String SQL_SEATS=
            "SELECT Seat.seatNumber, Seat.rowNumber, Category.categoryName, Category.tariff "
            + "FROM Row_, Seat, Category, Representation "
            + "WHERE Row_.roomName = Seat.roomName "
            + "  AND Seat.rowNumber = Row_.rowNumber "
            + "  AND Row_.categoryName = Category.categoryName "
            + "  AND Representation.roomName = Row_.roomName "
            + "  AND Representation.roomName = ? "
            + "  AND Representation.dateHour = ? "
            + "GROUP BY Category.categoryName, Seat.seatNumber, Seat.rowNumber "
            + "HAVING (Seat.seatNumber, Seat.rowNumber) NOT IN "
            + "( "
            + "    SELECT Seat.seatNumber, Seat.rowNumber "
            + "    FROM Seat, Row_, Representation,Bookings "
            + "    WHERE Row_.roomName = Seat.roomName "
            + "      AND Seat.rowNumber = Row_.rowNumber "
            + "      AND Representation.roomName = Row_.roomName "
            + "      AND Representation.roomName = ? "
            + "      AND Representation.dateHour = ? "
            + "      AND Bookings.dateHour = Representation.dateHour "
            + "      AND Bookings.roomName = Representation.roomName "
            + "      AND Bookings.rowNumber = Row_.rowNumber "
            + "      AND Bookings.seatNumber= Seat.seatNumber "
            + ") "
            + "AND (Seat.seatNumber, Seat.rowNumber) NOT IN "
            + "(   SELECT Seat.seatNumber, Seat.rowNumber "
            + "    FROM Seat, Row_, Representation,Sale "
            + "    WHERE Row_.roomName = Seat.roomName "
            + "      AND Seat.rowNumber = Row_.rowNumber "
            + "      AND Representation.roomName = Row_.roomName "
            + "      AND Representation.roomName = ? "
            + "      AND Representation.dateHour = ? "
            + "      AND Sale.dateHour = Representation.dateHour "
            + "      AND Sale.roomName = Representation.roomName "
            + "      AND Sale.rowNumber = Row_.rowNumber "
            + "      AND Sale.seatNumber= Seat.seatNumber "
            + ")";
    private final DAOFactory daoFactory;
    
    public DAORepresentationImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void create(Representation representation) throws DAOException {
        if (!representation.isIsWithdrawn()) {
            throw new DAOException("You try to add a Representation which is "
                    + "ever not Withdrawn");
        }
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(representation.getDate());
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT, false,
                    representation.getSpectacleNumber(),
                    sDate,
                    representation.getRoomName(),
                    true);
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at inserting a Representation.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public void cancel(Representation representation) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(representation.getDate());
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_CANCEL, false,
                    representation.getSpectacleNumber(),
                    sDate,
                    representation.getRoomName());
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at canceling a Representation.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public void actualize(Representation representation) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(representation.getDate());
        Connection connection = null;
        try {
            connection = daoFactory.getConnection();
            boolean res = isAlwaysWithdrawn(connection,
                    sDate,
                    representation.getRoomName(),
                    representation.getSpectacleNumber());
            representation.setIsWithdrawn(res);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(connection);
        }
    }

    public static boolean isAlwaysWithdrawn(Connection connection,
            String sDate, String roomName, int spectacleNumber) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean res = false;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT, false,
                    spectacleNumber,
                    sDate,
                    roomName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                res = resultSet.getBoolean("isWithdrawn");
            } else {
                System.out.println(preparedStatement.toString());
                throw new SQLException("This representation isn't in the database");
            }
        } catch (SQLException ex) {
            throw new SQLException("failure in isAlwaysWithdrawn", ex);
        } finally {
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
        }
        return res;
    }
    
    public static int spectacleNumber(Connection connection,
            String sDate, String roomName) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int res = -5;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_FIND_SPECTACLE_NUMBER, false,
                    sDate,
                    roomName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                res = resultSet.getInt("spectacleNumber");
            } else {
                throw new SQLException("This representation isn't in the database");
            }
        } catch (SQLException ex) {
            throw new SQLException("DAORepresentationImpl.spectacleNumber", ex);
        } finally {
            DAOUtility.silentClosure(resultSet);
            DAOUtility.silentClosure(preparedStatement);
        }
        return res;
    }
    
    @Override
    public void delete(Representation representation) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(representation.getDate());
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false,
                    representation.getSpectacleNumber(),
                    sDate,
                    representation.getRoomName());
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at deleting a Representation.");
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

  @Override
  public HashMap<Category, LinkedList<Seat>> findSeats(Representation representation) throws DAOException {
        HashMap<Category, LinkedList<Seat>> resultat = null;
        HashMap<String, Category> categories = null;
        Room room = new Room();
        room.setName(representation.getRoomName());
        ArrayList<Row> rows = new ArrayList<>();
        for (int i=0; i<10; i++) {
            Row row = new Row();
            row.setRowNumber(i+1);
            row.setRoom(room);
            rows.add(i, row);
        }
        room.setRows(rows);
        
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(representation.getDate());
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SEATS, false,
                    representation.getRoomName(),
                    sDate,
                    representation.getRoomName(),
                    sDate,
                    representation.getRoomName(),
                    sDate);
            resultSet = preparedStatement.executeQuery();
            resultat = new HashMap<>();
            categories = new HashMap<>();
            while (resultSet.next()) {
                int seatNumber = resultSet.getInt("seatNumber");
                int rowNumber = resultSet.getInt("rowNumber");
                String categoryName = resultSet.getString("categoryName");
                double tariff = resultSet.getDouble("tariff");
                Category category = new Category();
                category.setName(categoryName);
                category.setPrice(tariff);
                LinkedList<Seat> list = resultat.get(category);
                if (list==null) {
                    System.out.println("new Category: "+category.getName());
                    list = new LinkedList<>();
                    resultat.put(category, list);
                }
                Seat seat = new Seat();
                seat.setId(seatNumber);
                Row row = rows.get(rowNumber-1);
                assert(row.getRowNumber()==rowNumber);
                row.setCategory(category);
                seat.setRow(row);
                list.add(seat);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return resultat;
  }
}