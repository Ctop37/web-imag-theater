/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DAORoomImpl implements DAORoom {

    private static final String SQL_CREATE_ROOM=
            "INSERT INTO Room VALUES (?)";
    private static final String SQL_CREATE_ROW=
            "INSERT INTO Row_ VALUES (?,?,?)";
    private static final String SQL_CREATE_SEAT=
            "INSERT INTO Seat VALUES (?,?,?)";
    
    private static final String SQL_DELETE_SEATS=
            "DELETE FROM Seat";
    private static final String SQL_DELETE_ROWS=
            "DELETE FROM Row_";
    private static final String SQL_DELETE_ROOMS=
            "DELETE FROM Room";
    
    private static final String SQL_DELETE_PRECISE_ROOM1=
            "DELETE FROM Seat WHERE roomName = ?";
    private static final String SQL_DELETE_PRECISE_ROOM2=
            "DELETE FROM Row_ WHERE roomName = ?";
    private static final String SQL_DELETE_PRECISE_ROOM3=
            "DELETE FROM Room WHERE roomName = ?";
    
    private static final String SQL_SELECT=
            "SELECT Room.roomName FROM Room";
    
    private final DAOFactory daoFactory;

    public DAORoomImpl(DAOFactory daof) {
        this.daoFactory = daof;
    }

    @Override
    public void create(Room room) throws DAOException {
        Connection connection = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            String roomName = room.getName();
            ArrayList<Row> rows = room.getRows();
            Iterator<Row> iterator = rows.iterator();
            createRoom(roomName, connection);
            while (iterator.hasNext()) {
                Row row = iterator.next();
                createRow(roomName, row.getRowNumber(), row.getCategory(), connection);
                for (int i=1; i<21; i++) {
                    createSeat(roomName, row.getRowNumber(), i, connection);
                }
            }
            connection.commit();
        } catch (DAOException ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(DAORoomImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw new DAOException(ex);
        } catch (SQLException ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(DAORoomImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(connection);
        }
   }

    private void createRoom(String name, Connection connection) throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                connection, SQL_CREATE_ROOM, false, name);
                int statut;
                statut = preparedStatement.executeUpdate();
                if (statut == 0) {
                    throw new DAOException("Failure at inserting a Room");
                }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }
    }
    
    private void createRow(String roomName, int number, Category category, Connection connection)
            throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(connection,
                    SQL_CREATE_ROW, false,roomName, number, category.getName());
                int statut;
                statut = preparedStatement.executeUpdate();
                if (statut == 0) {
                    throw new DAOException("Failure at inserting a Row");
                }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }
    }
    
    private void createSeat(String roomName, int rowNumber, int seatNumber, Connection connection)
            throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(connection,
                    SQL_CREATE_SEAT, false,roomName, rowNumber, seatNumber);
                int statut;
                statut = preparedStatement.executeUpdate();
                if (statut == 0) {
                    throw new DAOException("Failure at inserting a seat");
                }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }
    }

    @Override
    public void deleteAll() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_SEATS, false);
            preparedStatement.executeUpdate();
            
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_ROWS, false);
            preparedStatement.executeUpdate();
            
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_ROOMS, false);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public Set<String> findAll() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Set<String> result = null;
        
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT, false);
            resultSet = preparedStatement.executeQuery();
            result = new HashSet<>();
            while (resultSet.next()) {
                result.add(resultSet.getString("roomName"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return result;
    }
    
    public void deleteRoom(String roomName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_PRECISE_ROOM1, false, roomName);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_PRECISE_ROOM2, false, roomName);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE_PRECISE_ROOM3, false, roomName);
            preparedStatement.executeUpdate();
            DAOUtility.silentClosure(preparedStatement);
            connection.commit();
        } catch (SQLException ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(DAORoomImpl.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
        
    }
}
