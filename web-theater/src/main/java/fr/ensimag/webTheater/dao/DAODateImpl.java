/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import static java.lang.Integer.valueOf;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;


public class DAODateImpl implements DAODate {

    private static final String SQL_INSERT=
            "INSERT INTO DateHour VALUES (?)";
        private static final String SQL_DELETE=
            "DELETE FROM DateHour WHERE dateHour=?";
    private final DAOFactory daoFactory;
    
    public DAODateImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    @Override
    public void create(Date date) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        if (date.getMonth() == Calendar.JULY || date.getMonth() == Calendar.AUGUST) {
            throw new DAOException("According to the order, there are no date in july and august.");
        }
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_INSERT, false, stringOfDate(date));
            int statut;
            statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at inserting a Date.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public void delete(Date date) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false, stringOfDate(date));
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new DAOException("Failure at deleting a Date.");
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement, connection);
        }
    }

    @Override
    public String stringOfDate(Date d) {
        int year = d.getYear()+1900;
        int month = d.getMonth()+1;
        int date = d.getDate();
        int hour = d.getHours();
        return year+"-"+month+"-"+date+" "+hour+":00:00";
    }

    @Override
    public Date dateOfString(String s) {
        Pattern p = Pattern.compile("\\W");
        String [] sDate = p.split(s);
        int year = valueOf(sDate[0]);
        int month = valueOf(sDate[1]);
        int date = valueOf(sDate[2]);
        int hour = valueOf(sDate[3]);
        Date d = new Date(year-1900, month-1, date, hour, 0);
        return d;
    }
}
