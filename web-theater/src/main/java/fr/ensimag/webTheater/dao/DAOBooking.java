/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Customer;
import java.util.Set;

/**
 *
 * @author thibaut
 */
public interface DAOBooking {

    /**
     * Being given a Booking bean, this method adds it in the database. More
     * exactly, this method realizes a unique transaction in : it verifies that
     * every one of these seats are free, it books every ones of the seats of
     * the bookings. In case where there is a failure (for example: someone have
     * ever books one seat), all the booking is properly canceled and an
     * exception is thrown.
     *
     * @param booking
     * @throws DAOException
     */
    void create(Booking booking) throws DAOException;

    /**
     * Being given a Booking bean, this method delete it in the database.
     *
     * @param booking
     * @throws DAOException
     */
    void delete(Booking booking) throws DAOException;

    /**
     * Return all the bookings owned by a customer.
     * each item of the returned Set which contain a Booking having a different
     * date from the other.
     * This method doesn't build a complete room (with its rows, its categories,
     * and its 200 Seats). The seat points on a row of the good number. However
     * this row doesn't point on a Category. On the same way, the Row point on
     * a Room however, the room doesn't have a Set of 10 Rows.
     *
     * @param customerMail
     * @return
     * @throws DAOException
     */
    Set<Booking> findBookingByCustomer(String customerMail) throws DAOException;
}
