/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import fr.ensimag.webTheater.beans.Seat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOBookingImpl implements DAOBooking {

    private static final String SQL_INSERT
            = "INSERT INTO Bookings VALUES (?,?,?,?,?)";

    private static final String SQL_SELECT_BOOKING
            = "SELECT * FROM Bookings b WHERE b.dateHour = ? "
            + "AND b.roomName = ? AND b.rowNumber = ? AND b.seatNumber = ? ";

    private static final String SQL_SELECT_SALE
            = "SELECT * FROM Sale s WHERE s.dateHour = ? "
            + "AND s.roomName = ? AND s.rowNumber = ? AND s.seatNumber = ? ";

    private static final String SQL_DELETE
            = "DELETE FROM Bookings WHERE userMail=? AND dateHour=? AND "
            + "roomName=? AND rowNumber=? AND seatNumber=?";

    private static final String SQL_SELECT_BOOKING_VIA_CUSTOMER
            = "SELECT b.dateHour, b.roomName, b.rowNumber, b.seatNumber "
            + "FROM Bookings b "
            + "WHERE b.userMail = ? "
            + "GROUP BY b.dateHour, b.roomName, b.rowNumber, b.seatNumber";

    private static final String SQL_COUNT
            = "SELECT COUNT(*) as 'number' "
            + "FROM Row_, Seat, Representation "
            + "WHERE Row_.roomName = Seat.roomName "
            + "  AND Seat.rowNumber = Row_.rowNumber "
            + "  AND Representation.roomName = Row_.roomName "
            + "  AND Representation.roomName = ? "
            + "  AND Representation.dateHour = ? "
            + "  AND Representation.isWithdrawn = true "
            + "  AND (Seat.seatNumber, Seat.rowNumber) NOT IN "
            + "( "
            + "    SELECT Seat.seatNumber, Seat.rowNumber "
            + "    FROM Seat, Row_, Representation, Bookings "
            + "    WHERE Row_.roomName = Seat.roomName "
            + "      AND Seat.rowNumber = Row_.rowNumber "
            + "      AND Representation.roomName = Row_.roomName "
            + "      AND Representation.roomName = ? "
            + "      AND Representation.dateHour = ? "
            + "      AND Representation.isWithdrawn = true "
            + "      AND Bookings.dateHour = Representation.dateHour "
            + "      AND Bookings.roomName = Representation.roomName "
            + "      AND Bookings.rowNumber = Row_.rowNumber "
            + "      AND Bookings.seatNumber= Seat.seatNumber "
            + ") "
            + "AND (Seat.seatNumber, Seat.rowNumber) NOT IN "
            + "(   SELECT Seat.seatNumber, Seat.rowNumber "
            + "    FROM Seat, Row_, Representation, Sale "
            + "    WHERE Row_.roomName = Seat.roomName "
            + "      AND Seat.rowNumber = Row_.rowNumber "
            + "      AND Representation.roomName = Row_.roomName "
            + "      AND Representation.roomName = ? "
            + "      AND Representation.dateHour = ? "
            + "      AND Representation.isWithdrawn = true "
            + "      AND Sale.dateHour = Representation.dateHour "
            + "      AND Sale.roomName = Representation.roomName "
            + "      AND Sale.rowNumber = Row_.rowNumber "
            + "      AND Sale.seatNumber= Seat.seatNumber "
            + ")";

    private final DAOFactory daoFactory;

    public DAOBookingImpl(DAOFactory daofactory) {
        this.daoFactory = daofactory;
    }

    @Override
    public void create(Booking booking) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(booking.getDate());
        Set<Seat> seats = booking.getSeats();
        Iterator<Seat> iterator = seats.iterator();
        String roomName = iterator.next().getRow().getRoom().getName();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_COUNT, false,
                    roomName, sDate,
                    roomName, sDate,
                    roomName, sDate);
            resultSet = preparedStatement.executeQuery();
            int numberRemainingPlaces = -5;
            if (resultSet.next()) {
                numberRemainingPlaces = resultSet.getInt("number");
            } else {
                throw new SQLException("unknown problem");
            }
            DAOUtility.silentClosure(preparedStatement);
            DAOUtility.silentClosure(resultSet);
            if (numberRemainingPlaces - seats.size() < 70) {
                connection.rollback();
                DAOUtility.silentClosure(resultSet, preparedStatement, connection);
                throw new DAOException("We can't do this booking: we have to "
                        + "keep 70 seats free for the sales of the date of the "
                        + "representation. There, we have "
                        + numberRemainingPlaces + " remaing seats and you want to"
                        + " book " + seats.size() + " seats. Added informations:"
                        + "The bookings you want to do concerns the date: "
                        + sDate + " and the room: " + roomName);
            }
            iterator = seats.iterator();
            while (iterator.hasNext()) {
                Seat seat = iterator.next();
                preparedStatement = DAOUtility.initializePreparedRequest(
                        connection, SQL_SELECT_BOOKING, false,
                        sDate,
                        seat.getRow().getRoom().getName(),
                        seat.getRow().getRowNumber(),
                        seat.getId());
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    connection.rollback();
                    DAOUtility.silentClosure(resultSet, preparedStatement, connection);
                    throw new DAOException("seat ever reserved");
                }
                preparedStatement = DAOUtility.initializePreparedRequest(
                        connection, SQL_SELECT_SALE, false,
                        sDate,
                        seat.getRow().getRoom().getName(),
                        seat.getRow().getRowNumber(),
                        seat.getId());
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    connection.rollback();
                    DAOUtility.silentClosure(resultSet, preparedStatement, connection);
                    throw new DAOException("seat ever sold");
                }
            }
            iterator = seats.iterator();
            while (iterator.hasNext()) {
                Seat seat = iterator.next();
                preparedStatement = DAOUtility.initializePreparedRequest(
                        connection, SQL_INSERT, false,
                        booking.getUserMail(),
                        sDate,
                        seat.getRow().getRoom().getName(),
                        seat.getRow().getRowNumber(),
                        seat.getId());
                int statut;
                statut = preparedStatement.executeUpdate();
                if (statut == 0) {
                    connection.rollback();
                    DAOUtility.silentClosure(resultSet, preparedStatement, connection);
                    throw new DAOException("Failure at inserting a Booking");
                }
            }
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(DAOBookingImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }

    }

    @Override
    public void delete(Booking booking) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        String sDate = dAODateImpl.stringOfDate(booking.getDate());
        Set<Seat> seats = booking.getSeats();
        Iterator<Seat> iterator = seats.iterator();
        Connection connection = null;
        try {
            connection = daoFactory.getConnection();
            connection.setAutoCommit(false);
            while (iterator.hasNext()) {
                Seat seat = iterator.next();
                deleteBooking(connection,
                        booking.getUserMail(),
                        sDate,
                        seat.getRow().getRoom().getName(),
                        seat.getRow().getRowNumber(),
                        seat.getId());
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(DAOBookingImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
            throw new DAOException(ex);
        } finally {
            DAOUtility.silentClosure(connection);
        }
    }

    public static void deleteBooking(Connection connection, String userMail,
            String sDate, String roomName, int rowNumber, int seatNumber)
            throws SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_DELETE, false,
                    userMail, sDate, roomName, rowNumber, seatNumber);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException("Failure at deleting a Booking.");
            }
        } catch (SQLException ex) {
            throw new SQLException("failure in deleteBooking", ex);
        } finally {
            DAOUtility.silentClosure(preparedStatement);
        }

    }

    @Override
    public Set<Booking> findBookingByCustomer(String customerMail) throws DAOException {
        DAODateImpl dAODateImpl = new DAODateImpl(daoFactory);
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Set<Booking> result = null;
        String lastDateHour = null;
        Booking booking = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = DAOUtility.initializePreparedRequest(
                    connection, SQL_SELECT_BOOKING_VIA_CUSTOMER, false,
                    customerMail);
            resultSet = preparedStatement.executeQuery();
            result = new HashSet<>();
            while (resultSet.next()) {
                String sDate = resultSet.getString("dateHour");
                String roomName = resultSet.getString("roomName");
                int rowNumber = resultSet.getInt("rowNumber");
                int seatNumber = resultSet.getInt("seatNumber");
                if (!sDate.equals(lastDateHour)) {
                    Date dateHour = dAODateImpl.dateOfString(sDate);
                    booking = new Booking();
                    booking.setDate(dateHour);
                    booking.setUserMail(customerMail);
                    booking.setSeats(new HashSet<Seat>());
                    result.add(booking);
                }
                Room room = new Room();
                room.setName(roomName);
                Row row = new Row();
                row.setRowNumber(rowNumber);
                row.setRoom(room);
                Seat seat = new Seat();
                seat.setId(seatNumber);
                seat.setRow(row);
                booking.getSeats().add(seat);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtility.silentClosure(resultSet, preparedStatement, connection);
        }
        return result;
    }
}
