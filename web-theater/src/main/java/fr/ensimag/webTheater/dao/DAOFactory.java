/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * inspired of OpenClassrooms
 * @author thibaut
 */
public class DAOFactory {
    
    private static final String url="jdbc:mysql://localhost:3306/projet_web_bd";
    private static final String driver   = "com.mysql.jdbc.Driver";
    private static final String username = "java";
    private static final String password = "pass";

    private DAOFactory() {}

    /*
     * This method charge the
     * JDBC driver and return an instance of the Factory.
     */
    public static DAOFactory getInstance() throws DAOConfigurationException {
        try {
            Class.forName(driver);
        } catch ( ClassNotFoundException e ) {
            throw new DAOConfigurationException(
                    "Driver not found in classpath.", e );
        }

        DAOFactory instance = new DAOFactory();
        return instance;
    }

    /* This method find the connection to the database.*/
    Connection getConnection() throws SQLException {
        return DriverManager.getConnection( url, username, password );
    }
    
    public DAOBooking getDAOBooking() {
      return new DAOBookingImpl(this);
    }
    
    public DAOCase getDAOCase() {
      return new DAOCaseImpl(this);
    }
    
    public DAOCategory getDAOCategory() {
      return new DAOCategoryImpl(this);
    }
    
    public DAODate getDAODate() {
      return new DAODateImpl(this);
    }
    
    public DAORepresentation getDAORepresentation() {
      return new DAORepresentationImpl(this);
    }
    
    public DAORoom getDAORoom() {
      return new DAORoomImpl(this);
    }
    
    public DAOSale getDAOSale() {
      return new DAOSaleImpl(this);
    }

    public DAOSpectacle getDAOSpectacle() {
      return new DAOSpectacleImpl(this);
    }
        
    public DAOUser getDAOUser() {
      return new DAOUserImpl(this);
    }
}