/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.form;

import fr.ensimag.webTheater.beans.User;
import fr.ensimag.webTheater.util.Crypt;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.dao.DAOUser;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author xavier
 */
public final class ConnectionForm {
  
  private static final String EMAIL_FIELD  = "email";
  private static final String PASS_FIELD   = "password";
  
  private DAOFactory daoFactory = null;
  private DAOUser daoUser = null;
  
  private String result;
  private Map<String, String> errors = new HashMap<String,String>();
  
  public ConnectionForm(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }
  
  public String getResult() {
    return result;
  }
  
  public Map<String, String> getErrors() {
    return errors;
  }
  
  
  public User connectUser ( HttpServletRequest request ) {
    String email = getFieldValue(request, EMAIL_FIELD);
    if (email != null){
      email = email.toLowerCase();
    }
    String password = getFieldValue(request, PASS_FIELD);
    
    User user = new User();
    daoFactory = DAOFactory.getInstance();
    daoUser = daoFactory.getDAOUser();
    
    try {
      user = validateUser(email,password);
    } catch (ExceptionConnection e) {
      errors.put(EMAIL_FIELD, e.getMessage());
    } catch (Exception e){
      errors.put(EMAIL_FIELD, "Oops, something went wrong..");
    }
    
    if ( errors.isEmpty() ) {
      result = "Vous êtes connecté";
    } else {
      result = "Échec de la connexion.";
    }
    
    return user;
  }
  
  private User validateUser(String email, String guessPassword) throws ExceptionConnection, Exception {
    User user = daoUser.find(email);
    if (user == null){
      // Incorrect user
      throw new ExceptionConnection("Identifiant ou mot de passe incorrect");
    }
    String hash = user.getPassword();
    String salt = hash.split("[$]")[0];
    String hashedPassword = hash.split("[$]")[1];
    if (!hashedPassword.equals(Crypt.sha512(salt + guessPassword))){
      // Incorrect password
      throw new ExceptionConnection("Identifiant ou mot de passe incorrect");
    }
    
    // Either return Administrator or Customer depending of the account's type
    if (daoUser.findAdministrator(email) != null){
      return daoUser.findAdministrator(email);
    } else {
      return daoUser.findCustomer(email);
    }
    
  }
  
  
  
  /*
  * Return the fieldName value unless the field is empty
  */
  private static String getFieldValue( HttpServletRequest request, String fieldName ) {
    String value = request.getParameter( fieldName );
    if ( value == null || value.trim().length() == 0 ) {
      return null;
    } else {
      return value.trim();
    }
  }
  
  
}
