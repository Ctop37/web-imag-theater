/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.form;

import fr.ensimag.webTheater.util.Crypt;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.User;
import fr.ensimag.webTheater.dao.DAOFactory;
import fr.ensimag.webTheater.dao.DAOUser;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author xavier
 */
public final class InscriptionForm {
  
  private static final String FIRST_NAME_FIELD = "first_name";
  private static final String LAST_NAME_FIELD = "last_name";
  private static final String EMAIL_FIELD  = "email";
  private static final String PASS_FIELD   = "password";
  private static final String CHECK_FIELD   = "password_check";
  
  private DAOFactory daoFactory = null;
  private DAOUser daoUser = null;
  
  private String result;
  private Map<String, String> errors = new HashMap<String,String>();

  public InscriptionForm(DAOFactory daoFactory){
      this.daoFactory = daoFactory;
  }
  
  public String getResult() {
    return result;
  }
  
  public Map<String, String> getErrors() {
    return errors;
  }
  
  
  public Customer registerUser ( HttpServletRequest request ) throws Exception {
    String email = getFieldValue (request, EMAIL_FIELD);
    email = email.toLowerCase();
    String password = getFieldValue (request, PASS_FIELD);
    String passwordCheck = getFieldValue (request, CHECK_FIELD);
    String firstName = getFieldValue(request, FIRST_NAME_FIELD);
    String lastName = getFieldValue(request, LAST_NAME_FIELD);
    
    Customer customer = new Customer();
    daoUser = daoFactory.getDAOUser();
    
    try {
      validateEmail(email);
    } catch ( Exception e ) {
      errors.put(EMAIL_FIELD, e.getMessage());
    }
    customer.setEmail( email );
    
    try {
      validateName(firstName);
    } catch ( Exception e ) {
      errors.put(FIRST_NAME_FIELD, e.getMessage());
    }
    customer.setFirstName(firstName);
    
    try {
      validateName(lastName);
    } catch ( Exception e ) {
      errors.put(LAST_NAME_FIELD, e.getMessage());
    }
    customer.setLastName(lastName);
    
    try {
      validatePassword( password, passwordCheck );
    } catch ( Exception e ) {
      errors.put(PASS_FIELD, e.getMessage());
    }
    
    // Generates salt and hashes (salt+password)
    password = hashPassword(password);
    customer.setPassword(password);
    
    if ( errors.isEmpty() ) {
      daoUser.createCustomer(customer);
      result = "Succès de l'inscription.";
    } else {
      result = "Échec de l'inscription.";
    }
    
    return customer;
  }
  
  private void validateEmail(String email) throws Exception {
    if ( email != null ) {
      if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
        throw new Exception( "Adresse email invalide !" );
      }
    } else {
      throw new Exception( "Veuillez saisir une adresse mail !" );
    }
    User user = daoUser.find(email);
    if (user != null){
      throw new Exception("Cette adresse mail est déjà utilisée !");
    }
  }
  
  private void validatePassword(String password, String passwordCheck) throws Exception {
    if ( password != null && passwordCheck != null ) {
      if (!password.equals(passwordCheck)) {
        throw new Exception( "Les mots de passes doivent être identiques !");
      } else if ( password.length() < 6 ) {
        throw new Exception( "Le mot de passe doit contenir au moins 6 caractères." );
      }
    } else {
      throw new Exception( "Merci de saisir et confirmer votre mot de passe." );
    }
  }
  
  private void validateName(String name) throws Exception {
    if ( name != null && name.length() < 3 ) {
      throw new Exception("Le nom saisi est incorrect");
    }
  }
  
  /*
  * Return the fieldName value unless the field is empty
  */
  private static String getFieldValue( HttpServletRequest request, String fieldName ) {
    String value = request.getParameter( fieldName );
    if ( value == null || value.trim().length() == 0 ) {
      return null;
    } else {
      return value.trim();
    }
  }

  private String hashPassword(String password) throws Exception {
    String salt = Crypt.genSalt();
    password = salt + "$" + Crypt.sha512(salt + password);
    return password;
  }
  
}

