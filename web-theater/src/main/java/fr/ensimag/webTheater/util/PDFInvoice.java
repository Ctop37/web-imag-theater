/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.util;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.beans.Seat;
import java.awt.Color;
import java.io.OutputStream;

/**
 *
 * @author xavier
 */
public class PDFInvoice {
  
  private static final int SPACE_WIDTH = 30;
  private static final int H_MARGIN = 30;
  private static final int V_MARGIN = 10;
  
  public static void generate(OutputStream out, Sale sale, Case bill, String spectacleName, String roomName, String date) throws DocumentException {
    
    Document document = new Document(PageSize.A4,H_MARGIN, H_MARGIN, V_MARGIN, V_MARGIN);
    PdfWriter.getInstance(document,out);
    document.open();
    
    makeTitle(document);
    makeCaseInfo(document, String.valueOf(bill.getTicketNumber()), bill.getUserMail());
    //makeCaseInfo(document, "123123123", "xavier@gmail.com");
    makeSpectacleTitle(document, spectacleName, roomName, date);
    makeSeatsTitle(document);
    
    double totalPrice = 0;
    for (Seat seat : sale.getSeats()){
      makeSeat(document, 
              String.valueOf(seat.getRow().getRowNumber()), 
              String.valueOf(seat.getSeatNumber()), 
              String.valueOf(seat.getPrice()));
      totalPrice += seat.getPrice();
    }
    
    //makeSeat(document, "2","8","130");
    document.add(Chunk.NEWLINE);
    
    makeSummary(document, String.valueOf(totalPrice));
    document.close();
  }
  
  private static void makeTitle(Document document) throws DocumentException{
    PdfPTable table =new PdfPTable(1);
    PdfPCell cell = new PdfPCell (new Paragraph ("Imag Theater"));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setPadding (10.0f);
    table.addCell(cell);
    table.setSpacingAfter(SPACE_WIDTH);
    document.add(table);
  }
  
  private static void makeCaseInfo(Document document, String ticketNumber, String email) throws DocumentException{
    document.add(new Paragraph("Numéro de ticket : " + ticketNumber));
    document.add(new Paragraph("Email client : " + email));
    document.add(Chunk.NEWLINE);
  }
  
  private static void makeSpectacleTitle(Document document, String spectacleName, String roomName, String date) throws DocumentException{
    PdfPTable table = new PdfPTable(1);
    
    PdfPCell cell = new PdfPCell(new Paragraph(spectacleName));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setBackgroundColor (new Color (128, 200, 128));
    cell.setPadding (10.0f);
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph("Salle : " + roomName));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setPadding (10.0f);
    table.addCell(cell);
    
    cell = new PdfPCell(new Paragraph("Horraire : " + date));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setPadding (10.0f);
    table.addCell(cell);
    
    table.setSpacingAfter(SPACE_WIDTH);
    document.add(table);
    
  }
  
  
  private static void makeSeatsTitle(Document document) throws DocumentException{
    PdfPTable table = new PdfPTable(3);
    
    PdfPCell cell = new PdfPCell (new Paragraph ("Rangée"));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setBackgroundColor (new Color (255, 200, 0));
    cell.setPadding (10.0f);
    table.addCell (cell);
    
    cell = new PdfPCell (new Paragraph ("Numéro de place"));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setBackgroundColor (new Color (255, 200, 0));
    cell.setPadding (10.0f);
    table.addCell (cell);
    
    cell = new PdfPCell (new Paragraph ("Prix"));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setBackgroundColor (new Color (255, 200, 0));
    cell.setPadding (10.0f);
    table.addCell (cell);
    
    document.add(table);
  }
  
  
  private static void makeSeat(Document document, String row, String number, String price) throws DocumentException{
    PdfPTable table = new PdfPTable(3);
    
    PdfPCell cell = new PdfPCell (new Paragraph (row));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setPadding (10.0f);
    table.addCell (cell);
    
    cell = new PdfPCell (new Paragraph (number));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setPadding (10.0f);
    table.addCell (cell);
    
    cell = new PdfPCell (new Paragraph (price + " €"));
    cell.setHorizontalAlignment (Element.ALIGN_CENTER);
    cell.setPadding (10.0f);
    table.addCell (cell);
    
    document.add(table);
  }
  
  private static void makeSummary(Document document, String totalPrice) throws DocumentException{
    document.add(new Paragraph("Prix total : " + totalPrice + " €"));
    document.add(new Paragraph("Merci de votre achat !"));
  }
  
}
