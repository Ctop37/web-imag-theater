/*
* The MIT License
*
* Copyright 2015 xavier.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package fr.ensimag.webTheater.util;
import java.security.MessageDigest;
import java.security.SecureRandom;


/**
 *
 * @author xavier
 */
public class Crypt {
    private static String byte2hex(byte[] bytes){
        // convert the byte-string to hex-string                                                                                                                                               
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }


    public static String sha512(String pass) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(pass.getBytes());
        byte[] bytes = md.digest();
        return byte2hex(bytes);
    }

    public static String genSalt() throws Exception {
        //Always use a SecureRandom generator                                                                                                                                                  
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        //Create array for salt                                                                                                                                                                
        byte[] salt = new byte[16];
        //Get a random salt                                                                                                                                                                    
        sr.nextBytes(salt);
        //return salt                                                                                                                                                                          
        return byte2hex(salt);
    }

}