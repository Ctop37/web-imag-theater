/* 
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

  function deleteBooking(idToDelete) {
		$.ajax({
      url: '/bookings/checkout?id='+idToDelete,
      type: 'GET',
      success : function(response) {
        location.reload();
      }
		});      
  }
  
  function checkout() {
		$.ajax({
      url: '/bookings/checkout',
      type: 'POST',
      success : function(response) {
        $('#myModalLabel').html("Paiement effectuée");
        $('#messageIcon').addClass('glyphicon-ok');
        $('#message').html("Votre paiement a bien étée effectué !");
        $('#messageBody').html("Vous pouvez continuer vos achats, et vous pouvez également accéder à vos factures tant que vous restez connectés, en allant dans votre panier.")
        $('#myModal').modal('toggle');
      },
      error: function(response) {
        $('#myModalLabel').html("Erreur lors du paiement");
        $('#messageIcon').addClass('glyphicon-remove');
        $('#message').html("Une erreur est survenue lors du paiement de vos places.");
        $('#messageBody').html("Un problème est survenu lors du paiement, certaines des reservations effectuées ont peut être expiré. Contactez l'administrateur en cas de problème.");        
        $('#myModal').modal('toggle');
      }
		});      
  }