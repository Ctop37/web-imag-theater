/* 
 * The MIT License
 *
 * Copyright 2015 jean-david.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

  function SeatsCategory(category, seatsNumber) {
    this.category = category;
    this.seatsNumber = seatsNumber;
  }

  function Booking(date, room, seatsCategory) {
    this.date = date;
    this.room = room;
    this.seats = seatsCategory;
  }

  function bookit(event) {
    event = event || window.event;
    var firedEvent = $(event.target);
    var totalPrice = parseFloat($('#total').html());
    if (firedEvent.val() >= 0) {
      totalPrice += ((parseFloat(firedEvent.val()) 
                  - parseFloat(firedEvent.attr('last-value'))))
                  * parseFloat(firedEvent.closest('td').next().html());
      $('#total').html(totalPrice.toFixed(2));
      firedEvent.attr('last-value', firedEvent.val());
    }
    if (totalPrice <= 0) {
      $('#checkout-div').addClass('hidden');
    } else {
      $('#checkout-div').removeClass('hidden');
    }
    firedEvent.attr('value', firedEvent.val());
  }
  
  function bookingsCheckout() {
    var bookingsData = new Array();
//    var i = 0;
    $('*[id*=representation]').each(function () {
      var seatsCategories = new Array();
      $(this).find('*[id*=seatsCategory]').each(function () {
        var s = new SeatsCategory(
                  $(this).find('#categoryName').html(),
                  $(this).find('#seatsNumber').val()
                );
        if(s.seatsNumber > 0) {
          seatsCategories.push(s);
        }
      });
      if(seatsCategories.length > 0) {
          var b = new Booking(
                $(this).find('#date').html(),
                $(this).find('#room').html(),
                seatsCategories
              );
          bookingsData.push(b);
      }
    });
    
		$.ajax({
      url: '/bookings',
      type: 'POST',
      data: JSON.stringify({id:parseInt($('#spetacleId').html()),
                            bookings:bookingsData}),
      success: function (response) {
        $('#myModalLabel').html("Réservation effectuée");
        $('#messageIcon').addClass('glyphicon-ok');
        $('#message').html("Votre réservation a bien étée prise en compte !");
        $('#messageBody').html("Cependant, elle sera perdue dans 30 minutes ou si vous vous déconnectez.")
        $('#myModal').modal('toggle');
      },
      error: function(response) {
        $('#myModalLabel').html("Erreur lors de la réservation");
        $('#messageIcon').addClass('glyphicon-remove');
        $('#message').html("Une erreur est survenue lors de la réservation de vos places.");
        $('#messageBody').html("Soit vous avez rentré des informations invalides, soit il n'y a plus de places disponibles");        
        $('#myModal').modal('toggle');
      }
		});    
//     $('#total').html(i);
  }


