/* 
 * The MIT License
 *
 * Copyright 2015 louis.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var select_salle_tab = ["Salle 01 : Poulet", "Salle 02 : Balcon", "Salle 03 : Saindoux", "Salle 04 : Coucou"];
var select_rang_tab = ["rang 01", "rang 02", "rang 03", "rang 04", "rang 05", "rang 06", "rang 07", "rang 08", "rang 09", "rang 10"];
var select_category_tab = ["Poulet", "Tomate", "oeufs"];
var select_prix_category_tab = [25, 20, 30];
var select_spectacle_tab = [];
var select_image_spectacle_tab = ["chat.jpg"];
var select_mail_spectacle_tab = ["coucou@hotmail.fr"];
var select_representation_tab = ["Representation 01", "Representation 02"];
var select_date_representation_tab = ["010203", "050607"];
var created_salles = 0;
var created_category = 0;
var created_spectacle = 0;
var created_representation = 0;


function fillSelectSalle() {
    var sel = document.getElementById("select_salle");
    for (var i = 0; i < select_salle_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_salle_tab[i];
        opt.value = select_salle_tab[i];
        sel.appendChild(opt);
    }
    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function newSalle() {
    var sel = document.getElementById("select_salle");
    var newSalleCreated = document.createElement("option");
    select_salle_tab.push("Nouvelle Salle" + created_salles++);
    newSalleCreated.innerHTML = select_salle_tab[select_salle_tab.length - 1];
    newSalleCreated.value = select_salle_tab[select_salle_tab.length - 1];
    sel.appendChild(newSalleCreated);
    //document.getElementById("nom_salle").value = "Nouvelle Salle";
    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function deleteSalle() {
    var deletedSalle = document.forms['form_salle'].elements['select_salle'];
    var sel = document.getElementById("select_salle");

    for (var i = 0; i < select_salle_tab.length; i++) {
        if (sel.options[i].value === deletedSalle.value) {
            sel.removeChild(sel.options[i]);
            select_salle_tab.splice(i, 1);
        }
    }
    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function monitorSalle() {
    document.getElementById('nom_salle').value = document.forms['form_salle'].elements['select_salle'].value;
    fillRangSalle();
    fillCategorySalle();
    showCategorySalle();
}

function applyChangesSalle() {
    var nomSalle = document.getElementById('nom_salle').value;
    var changedSalle = document.forms['form_salle'].elements['select_salle'];
    var sel = document.getElementById("select_salle");
    for (var i = 0; i < select_salle_tab.length; i++) {
        if (sel.options[i].value === changedSalle.value) {
            sel.options[i].value = nomSalle;
            sel.options[i].innerHTML = nomSalle;
            select_salle_tab[i] = nomSalle;
        }
    }
    showCategorySalle();
    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function fillRangSalle() {
    var sel = document.getElementById("select_rang_salle");
    for (var i = 0; i < select_rang_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_rang_tab[i];
        opt.value = select_rang_tab[i];
        sel.appendChild(opt);
    }
}

function fillCategorySalle() {
    var sel = document.getElementById("select_category_rang");
    for (var i = 0; i < select_category_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_category_tab[i];
        opt.value = select_category_tab[i];
        sel.appendChild(opt);
    }
}

function showCategorySalle() {
    var tarif = "Category actuelle : ";
    var sel = document.getElementById("select_category_rang");
    document.getElementById("show_category").innerHTML = tarif + sel.value;
}

function fillSelectCategory() {
    var sel = document.getElementById("select_category");
    for (var i = 0; i < select_category_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_category_tab[i];
        opt.value = select_category_tab[i];
        sel.appendChild(opt);
    }
    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function newCategory() {
    var sel = document.getElementById("select_category");
    var newCategoryCreated = document.createElement("option");
    select_category_tab.push("Nouvelle Category" + created_category++);
    select_prix_category_tab.push(0);
    newCategoryCreated.innerHTML = select_category_tab[select_category_tab.length - 1];
    newCategoryCreated.value = select_category_tab[select_category_tab.length - 1];
    sel.appendChild(newCategoryCreated);
}

function deleteCategory() {
    var deletedCategory = document.forms['form_category'].elements['select_category'];
    var sel = document.getElementById("select_category");

    for (var i = 0; i < select_category_tab.length; i++) {
        if (sel.options[i].value === deletedCategory.value) {
            sel.removeChild(sel.options[i]);
            select_category_tab.splice(i, 1);
            select_prix_category_tab.splice(i, 1);
        }
    }
}

function monitorCategory() {
    document.getElementById('nom_category').value = document.forms['form_category'].elements['select_category'].value;
    showCategoryTarif();
}

function applyChangesCategory() {
    var nomCategory = document.getElementById('nom_category').value;
    var prixCategory = document.getElementById('tarif_category').value;
    var changedCategory = document.forms['form_category'].elements['select_category'];
    var sel = document.getElementById("select_category");
    if (isNaN(parseFloat(prixCategory))) {
        alert("Le tarif doit être un nombre.");
    }
    else {
        prixCategory = parseFloat(prixCategory);
        for (var i = 0; i < select_category_tab.length; i++) {
            if (sel.options[i].value === changedCategory.value) {
                sel.options[i].value = nomCategory;
                sel.options[i].innerHTML = nomCategory;
                select_category_tab[i] = nomCategory;
                select_prix_category_tab[i] = prixCategory;
            }
        }
        showCategoryTarif();
    }

}

function showCategoryTarif() {
    var ind = document.forms['form_category'].elements['select_category'].selectedIndex;
    var strCat = "{listCategory.get(0).getPrice()}";
    strCat = [strCat.slice(0, 18), ind, strCat.slice(19)].join('');
    strCat = ["$", strCat].join('');
    document.getElementById('tarif_category').value = strCat;
    //document.getElementById('num_category').innerHTML = ind;
}

function fillSelectSpectacle() {
    var sel = document.getElementById("select_spectacle");
    for (var i = 0; i < 1; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = "${spectacles[0].getName()}";
        opt.value = "${spectacles[0].getName()}";
        sel.appendChild(opt);
    }

    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function newSpectacle() {
    var sel = document.getElementById("select_spectacle");
    var newSpectacleCreated = document.createElement("option");
    select_spectacle_tab.push("Nouveau Spectacle" + created_spectacle++);
    select_image_spectacle_tab.push("Entrez le lien d'une image");
    select_mail_spectacle_tab.push("Entrez une adresse mail");
    newSpectacleCreated.innerHTML = select_spectacle_tab[select_spectacle_tab.length - 1];
    newSpectacleCreated.value = select_spectacle_tab[select_spectacle_tab.length - 1];
    sel.appendChild(newSpectacleCreated);
}

function deleteSpectacle() {
    var deletedCategory = document.forms['form_spectacle'].elements['select_spectacle'];
    var sel = document.getElementById("select_spectacle");

    for (var i = 0; i < select_spectacle_tab.length; i++) {
        if (sel.options[i].value === deletedCategory.value) {
            sel.removeChild(sel.options[i]);
            select_spectacle_tab.splice(i, 1);
            select_image_spectacle_tab.splice(i, 1);
            select_mail_spectacle_tab.splice(i, 1);
        }
    }
}

function monitorSpectacle() {
    document.getElementById('nom_spectacle').value = document.forms['form_spectacle'].elements['select_spectacle'].value;
    showSpectacleImage();
    showSpectacleMail();
}

function applyChangesSpectacle() {
    var nomSpectacle = document.getElementById('nom_spectacle').value;
    var mailCategory = document.getElementById('mail_spectacle').value;
    var imageCategory = document.getElementById('image_spectacle').value;
    var changedSpectacle = document.forms['form_spectacle'].elements['select_spectacle'];
    var sel = document.getElementById("select_spectacle");
    for (var i = 0; i < select_spectacle_tab.length; i++) {
        if (sel.options[i].value === changedSpectacle.value) {
            sel.options[i].value = nomSpectacle;
            sel.options[i].innerHTML = nomSpectacle;
            select_spectacle_tab[i] = nomSpectacle;
            select_mail_spectacle_tab[i] = mailCategory;
            select_image_spectacle_tab[i] = imageCategory;
        }
    }
    showSpectacleImage();
    showSpectacleMail();
}

function showSpectacleImage() {
    var ind = document.forms['form_spectacle'].elements['select_spectacle'].selectedIndex;
    //document.getElementById('num_representation').innerHTML = ind;
    document.getElementById('image_spectacle').value = select_image_spectacle_tab[ind];
}

function showSpectacleMail() {
    var ind = document.forms['form_spectacle'].elements['select_spectacle'].selectedIndex;
    //document.getElementById('num_representation').innerHTML = ind;
    document.getElementById('mail_spectacle').value = select_mail_spectacle_tab[ind];
}

function fillSelectRepresentation() {
    var sel = document.getElementById("select_representation");
    for (var i = 0; i < select_representation_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_representation_tab[i];
        opt.value = select_representation_tab[i];
        sel.appendChild(opt);
    }
    //document.getElementById('num_salle').innerHTML = select_salle_tab.length;
}

function newRepresentation() {
    var sel = document.getElementById("select_representation");
    var newRepresentationCreated = document.createElement("option");
    select_representation_tab.push("Nouvelle Representation" + created_representation++);
    select_date_representation_tab.push("000000");
    newRepresentationCreated.innerHTML = select_representation_tab[select_representation_tab.length - 1];
    newRepresentationCreated.value = select_representation_tab[select_representation_tab.length - 1];
    sel.appendChild(newRepresentationCreated);
}

function deleteRepresentation() {
    var deletedRepresentation = document.forms['form_representation'].elements['select_representation'];
    var sel = document.getElementById("select_representation");

    for (var i = 0; i < select_representation_tab.length; i++) {
        if (sel.options[i].value === deletedRepresentation.value) {
            sel.removeChild(sel.options[i]);
            select_presentation_tab.splice(i, 1);
            select_date_representation_tab.splice(i, 1);
        }
    }
}

function monitorRepresentation() {
    document.getElementById('nom_category').value = document.forms['form_category'].elements['select_category'].value;
    fillSalleRepresentation();
    fillSpectacleRepresentation();
    showRepresentationDate();
}

function applyChangesRepresentation() {
    var nomRepresentation = document.getElementById('nom_representation').value;
    var dateRepresentation = document.getElementById('date_representation').value;
    var changedRepresentation = document.forms['form_representation'].elements['select_representation'];
    var sel = document.getElementById("select_representation");
    for (var i = 0; i < select_representation_tab.length; i++) {
        if (sel.options[i].value === changedRepresentation.value) {
            sel.options[i].value = nomRepresentation;
            sel.options[i].innerHTML = nomRepresentation;
            select_representation_tab[i] = nomRepresentation;
            select_date_representation_tab[i] = dateRepresentation;
        }
    }
    showRepresentationDate();
}

function showRepresentationDate() {
    var ind = document.forms['form_representation'].elements['select_representation'].selectedIndex;
    //document.getElementById('num_representation').innerHTML = ind;
    document.getElementById('date_representation').value = select_date_representation_tab[ind];
}

function fillSpectacleRepresentation() {
    var sel = document.getElementById("select_representation_spectacle");
    for (var i = 0; i < select_spectacle_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_spectacle_tab[i];
        opt.value = select_spectacle_tab[i];
        sel.appendChild(opt);
    }
}

function fillSalleRepresentation() {
    var sel = document.getElementById("select_representation_salle");
    for (var i = 0; i < select_salle_tab.length; i++) {
        var opt = document.createElement("option");
        opt.innerHTML = select_salle_tab[i];
        opt.value = select_salle_tab[i];
        sel.appendChild(opt);
    }
}