<%-- 
    Document   : checkout
    Created on : Apr 29, 2015, 8:39:07 AM
    Author     : jean-david
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <jsp:include page="/WEB-INF/jsp/layout/header.jsp"/>
  <body>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <h2><span class="glyphicon" id="messageIcon"></span> <span id="message"></span></h2>
            <p id="messageBody"></p>
          </div>
          <div class="modal-footer">
            <a class="btn btn-lg btn-primary" href="/index.html" role="button">Accueil</a>
            <a class="btn btn-lg btn-primary" onclick="location.reload();" role="button">Mes factures</a>
          </div>
        </div>
      </div>
    </div>    
    <jsp:include page="/WEB-INF/jsp/layout/navbar.jsp"/>
    <div class="container nested marketing">
      <div class="col-lg-offset-2 col-lg-8">
        <div class="text-center"><h1>Mon Panier</h1></div>
        <div class="fabric-wrapper col-lg-12 col-md-12">
          <h4 class="text-center"><c:out value="${errorMessage}" /></h4>
          <table class="table">
            <c:forEach items="${bookings}" var="booking" varStatus="forloop">
              <tr>
                <td><fmt:formatDate value="${booking.date}" pattern="dd/MM/yy à HH" />h</td>
                <td><c:out value="${booking.roomName}" /></td>
                <td><c:out value="${booking.spectacle.name}" /></td>
                <td><c:out value="${booking.totalPrice}"/></td>
                <td>€</td>
                <td><a onclick="deleteBooking(${forloop.index});"><span class="glyphicon glyphicon-remove"></span></a></td>
              </tr>
            </c:forEach>  
          </table>
        </div>
        <c:if test="${bookings ne null and !bookings.isEmpty()}">
          <div id="checkout-div" class="center-block col-lg-12 col-md-12">
            <h2>Total <span class="pull-right"><span id="total">${totalPrice}</span> €</span></h2>
            <button onclick="checkout()" class="btn btn-primary pull-right">Acheter</button>
          </div>
        </c:if>
        <div class="text-center"><h1>Mes factures</h1></div>
        <div class="fabric-wrapper col-lg-12 col-md-12">
          <h4 class="text-center">Les factures vous sont accessibles à partir d'un achat de votre part et jusqu'à votre déconnection</h4>
          <table class="table">
            <c:forEach items="${cases}" var="caseIterator" varStatus="forloopCases">
              <tr>
                <td><fmt:formatDate value="${caseIterator.date}" pattern="dd/MM/yy à HH" />h</td>
                <td><c:out value="${caseIterator.roomName}" /></td>
                <td><c:out value="${caseIterator.spectacleName}" /></td>
                <td><a href="/bookings/invoice?id=${forloopCases.index}"><span class="glyphicon glyphicon-download-alt"></span></a></td>
              </tr>
            </c:forEach>  
          </table> 
        </div>
      </div>
    </div>    
  </body>
  <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
</html>
