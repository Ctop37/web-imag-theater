<%-- 
    Document   : adminPage
    Created on : Apr 11, 2015, 12:40:47 PM
    Author     : louis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <jsp:include page="/WEB-INF/jsp/layout/header.jsp"/>
    <body  onload="//fillSelectSalle();
            //fillSelectCategory();
            //fillSelectSpectacle();
            //fillSelectRepresentation();">


        <!-- Special navbar for the admin panel -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <span class="navbar-brand">Imag Theater</span>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a href="index.html">Accueil</a></li>
                                <li><a href="#select_tables">Spectacles</a></li>
                                <li><a href="#salles">Salles</a></li>
                                <!--
                                <li><a href="#categories">Catégories</a></li>
                                <li><a href="#representations">Représentations</a></li>-->
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>


        <!-- Attention ca va etre degueu -->
        <br/><br/><br/><br/>
        <!-- J'avais prévenu... -->

        <!-- Each part of the admin Panel on one page -->
        <div class="container marketing">
            <!-- Admin panel monitoring "spectacles" -->
            <div id="select_tables">
                <h1 class="text-center">Gestion des spectacles</h1>
                <p>${testValue}</p>
                <div class="row">
                    <!-- START OF THE FORMS -->
                    <div class="col-md-6">
                        <form name="form_spectacle" method="post" action="adminPage">
                            <label> Spectacles </label>
                            <div class="form-group">
                                <select name="select_spectacle" id="select_spectacle" tabindex="30" size="5">
                                    <!-- WILL BE FILLED BY CATEGORY FROM DATABASE -->
                                    ${strSpectacle}

                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="new_spectacle" class="btn btn-primary right-block" value="0">Gestion</button>
                                <button type="submit" name="new_spectacle" class="btn btn-primary right-block" value="1">Nouvelle</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form name="form_representation" method="post" action="adminPage">
                            <label> Représentations </label>
                            <div class="form-group">
                                <select name="select_representation" id="select_representation" tabindex="30" size="5">
                                    <!-- WILL BE FILLED BY CATEGORY FROM DATABASE -->
                                    ${strRepresentation}
                                </select>
                            </div>
                            <p id="num_representation"></p>
                            <div class="form-group">
                                <button type="submit" name="new_representation" class="btn btn-primary right-block" value="0">Gestion</button>
                                <button type="submit" name="new_representation" class="btn btn-primary right-block" value="1">Nouvelle</button>
                            </div>
                        </form>
                    </div>
                    <!-- END OF THE FORMS -->
                </div><!-- /.row -->      <!-- START THE FEATURETTES -->  
            </div>
            <!-- Admin panel monitoring "représentations" -->
            <div id="manager_tables">
                <!--<h1 class="text-center">Gestion des représentations</h1>-->
                <div class="row">
                    <!-- START OF THE FORMS -->
                    <div class="col-md-6">
                        <form name="gestion_spectacle" method="post" action="adminSpectacle">
                            <label> Gestion des Spectacles</label>
                            <div class="form-group">
                                <label>Spectacle numéro: ${manager.spectacle.number}</label>
                            </div>
                            <div class="form-group">
                                <label>Nom</label>
                                <input type="string" class="form-control" name="nom_spectacle" id="nom_spectacle" placeholder="" value="<c:out value="${manager.spectacle.name}"/>" size="15" maxlength="40" />
                            </div>
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" class="form-control" name="image_spectacle" id="image_spectacle" placeholder="" value="<c:out value="${manager.spectacle.imagePath}"/>" size="15" maxlength="40" />
                            </div>
                            <div class="form-group">
                                <label>Mail</label>
                                <input type="string" class="form-control" name="mail_spectacle" id="mail_spectacle" placeholder="" value="<c:out value="${manager.spectacle.mailAdmin}"/>" size="15" maxlength="40" />
                            </div>
                            <div class="form-group">
                                <button type="submit" name="button_spectacle" class="btn btn-primary left-block" value=${manager.spectacle.number} >Appliquer</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form name="gestion_representation" method="post" action="adminRepresentation">
                            <label> Gestion des Représentations</label>
                            <div class="form-group">
                                <label>Spectacle numéro: </label>
                                <input type="string" class="form-control" name="num_spectacle_representation" id="date_representation" placeholder="" value="<c:out value="${manager.representation.spectacleNumber}"/>" size="15" maxlength="40" readonly/>
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input type="string" class="form-control" name="date_representation" id="date_representation" placeholder="" value="<fmt:formatDate value="${manager.representation.date}" pattern="dd/MM/yyyy:HH" />" size="15" maxlength="40" />
                            </div>
                            <div class="form-group">
                                <label>Salle</label>
                                <select name="select_representation_salle" id="select_representation_salle" value="" tabindex="30">
                                    <!-- WILL BE FILLED BY POSSIBLE ROOM -->
                                    ${strRoom}
                                </select>
                                <c:out value="${manager.representation.roomName}"/>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="representation_button" class="btn btn-primary left-block" value="0">Appliquer</button>
                                <button type="submit" name="representation_button" class="btn btn-primary right-block" value="1">Annuler</button>
                            </div>
                        </form>
                    </div>
                    <!-- END OF THE FORMS -->
                </div><!-- /.row -->      <!-- START THE FEATURETTES -->    
            </div>
            <hr class="featurette-divider">
            
            
            <!-- Admin panel monitoring "salles" -->
            <div id="salles" hidden>
                <h1 class="text-center">Gestion des salles</h1>
                <div class="row">
                    <!-- START OF THE FORMS -->
                    <div class="col-md-6">
                        <form name="form_salle" method="post" action="adminManageRoom">
                            <label> Salle </label>
                            <div class="form-group">
                                <select name="select_salle" id="select_salle" tabindex="30" size="7">
                                    <!-- WILL BE FILLED BY ROOMS FROM DATABASE -->
                                    ${strRoom}
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="button-room" class="btn btn-primary right-block" value="0">Nouvelle</button>
                                <button type="submit" name="button-room" class="btn btn-primary right-block" value="1">Gestion</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form name="gestion_salle" method="post" action="adminUpdateRoom">
                            <label> Gestion de Salle </label>
                            <div class="form-group">
                                <label>Nom</label>
                                <input type="string" class="form-control" name="nom_salle" id="nom_salle" placeholder="" value="<c:out value="${managerRoom.salle.name}"/>" size="15" maxlength="40" />
                            </div>
                            <div class="form-group">
                                <label>Rang</label>
                                <select name="select_rang_salle" id="select_rang_salle" tabindex="30">
                                    <!-- WILL BE FILLED BY EACH RANK OF THE ROOM -->
                                    ${strRang}
                                </select>
                                ${lol}
                            </div>
                            <div class="form-group">
                                <label>Catégorie</label>
                                <select name="select_category_rang" id="select_category_rang" tabindex="30">
                                    <!-- WILL BE FILLED BY POSSIBLE CATEGORY -->
                                    ${strCat}
                                </select>
                                ${lol}
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary center-block">Appliquer</button>
                            </div>
                        </form>
                    </div>
                    <!-- END OF THE FORMS -->
                </div><!-- /.row -->      <!-- START THE FEATURETTES -->      
                <hr class="featurette-divider">
            </div>

            <!-- Admin panel monitoring "catégories" -->
            <div id="categories" hidden="true">
                <h1 class="text-center">Gestion des catégories</h1>
                <div class="row">
                    <!-- START OF THE FORMS -->
                    <div class="col-md-6">
                        <form name="form_category" method="post" action="adminCategory">
                            <label> Catégorie </label>
                            <div class="form-group">
                                <select name="select_category" id="select_category" tabindex="30" size="5">
                                    <!-- WILL BE FILLED BY CATEGORY FROM DATABASE -->
                                    ${strCategory}
                                </select>
                            </div>
                            <p id="num_category"></p>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary right-block">Nouvelle</button>
                                <button type="button" class="btn btn-primary right-block">Supprimer</button>
                                <button type="button" class="btn btn-primary right-block">Gestion</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form name="gestion_category" method="post" action="adminCategory">
                            <label> Gestion des Catégories</label>
                            <div class="form-group">
                                <label>Nom</label>
                                <input type="string" class="form-control" name="nom_category" id="nom_category" placeholder="" value="" size="15" maxlength="40" />
                            </div>
                            <div class="form-group">
                                <label>Tarif</label>
                                <input type="string" class="form-control" name="tarif_category" id="tarif_category" placeholder="" value="" size="15" maxlength="40" />
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-primary center-block">Appliquer</button>
                            </div>
                        </form>
                    </div>
                    <!-- END OF THE FORMS -->
                </div><!-- /.row -->      <!-- START THE FEATURETTES -->
                <hr class="featurette-divider">
            </div>
        </div><!-- /.container -->

        <div id="flush_year">
            <div class="row">
                <!-- START OF THE FORMS -->
                <form name="reset_year" method="post" action="adminReset">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary center-block">Effacer la base de donnée de l'année passée</button>
                    </div>
                </form>
                <!-- END OF THE FORMS -->
            </div><!-- /.row -->      <!-- START THE FEATURETTES -->  

        </div>


        <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
    </body>
</html>
