<%-- 
    Document   : inscription
    Created on : 8 avr. 2015, 15:15:26
    Author     : xavier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
  <jsp:include page="/WEB-INF/jsp/layout/header.jsp"/>
  <body>
    <jsp:include page="/WEB-INF/jsp/layout/navbar.jsp"/>
    <div class="container nested">
      <div class="text-center">
        <h1>Inscription à Imag Theater</h1>
      </div>
      <form method="post" action="inscription">
        <div class="form-group">
          <label>Prénom</label>
          <input type="string" class="form-control" name="first_name" placeholder="" value="<c:out value="${user.firstName}"/>" size="20" maxlength="60" />
          <span class="text-error">${form.errors['first_name']}</span>                    
        </div>
        <div class="form-group">
          <label>Nom</label>
          <input type="string" class="form-control" name="last_name" placeholder="" value="<c:out value="${user.lastName}"/>" size="20" maxlength="60" />
          <span class="text-error">${form.errors['last_name']}</span>
        </div>
        <div class="form-group">
          <label>Courriel</label>
          <input type="string" class="form-control" name="email" placeholder="" value="<c:out value="${user.email}"/>" size="20" maxlength="60" />
          <span class="text-error">${form.errors['email']}</span> 
        </div>
        <div class="form-group">
          <label>Mot de passe</label>
          <input type="password" class="form-control" name="password" placeholder="" size="20" maxlength="60"/>
          <span class="text-error">${form.errors['password']}</span>
        </div>
        <div class="form-group">
          <label>Confirmation du mot de passe</label>
          <input type="password" class="form-control" name="password_check" placeholder="" size="20" maxlength="60"/>
          <span class="text-error">${form.errors['password_check']}</span>                    
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary center-block">S'inscrire</button>
        </div>
      </form>
      
      <div class="text-center">
        <p class="${empty form.errors? 'text-success' : 'text-error'}">${form.result}</p>
      </div>
    </div>
    <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
  </body>
</html>
