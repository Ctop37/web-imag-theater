<%-- 
    Document   : navbar
    Created on : Apr 5, 2015, 2:53:06 PM
    Author     : jean-david
--%>
<%@page import="fr.ensimag.webTheater.beans.Customer"%>
<%@page import="fr.ensimag.webTheater.beans.Administrator"%>
<!-- NAVBAR
================================================== -->

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
    prefix="fn" %>
  
  <div class="navbar-wrapper">
    <div class="container">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">Imag Theater</span>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
              <li><a href="/index.html">Accueil</a></li>
              <li><a href="/index.html#how">Fonctionnement</a></li>
              <li><a href="/index.html#spectacles">� l'affiche</a></li>

              
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <c:choose>
              <c:when test="${sessionScope.sessionUser!=null}">
                <%
                  if (request.getSession().getAttribute("sessionUser") instanceof  Administrator){
                %>
                <li><a href="/adminPage">Administration</a></li>
                <%
                  }
                %>
                <%
                  if (request.getSession().getAttribute("sessionUser") instanceof  Customer){
                %>
                <li><a href="/bookings/checkout"><span class="glyphicon glyphicon-shopping-cart"></span> ${fn:length(sessionScope.bookings)}</a></li>
                <%
                  }
                %>                 
                <li><a href="/Disconnection"><span class="glyphicon glyphicon-log-out"> D�connexion</span></a></li>
              </c:when>
              <c:otherwise>               
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Connection</a>
                  <ul class="dropdown-menu" role="menu">
                    <form method="post" action="connection">
                      <div class="form-group">
                        <input type="string" class="form-control" name="email" id="email" placeholder="Courriel" value="<c:out value="${user.email}"/>" />
                        <span class="text-warning">${form.errors['email']}</span>                    
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Mot de passe">
                        <span class="text-warning">${form.errors['password']}</span>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary center-block">Connexion</button>
                      </div>
                      <div class="text-center">
                        <a href="/inscription.jsp">Inscription</a>
                      </div>
                      <div class="text-center">
                        <p class="${empty form.errors? 'text-success' : 'text-warning'}">${form.result}</p>
                      </div>
                    </form>
                  </ul>
                </li>
              </c:otherwise>
            </c:choose>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</div>
