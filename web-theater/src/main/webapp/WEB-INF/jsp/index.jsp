<%-- 
    Document   : index
    Created on : Apr 5, 2015, 2:56:17 PM
    Author     : jean-david
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
  <jsp:include page="/WEB-INF/jsp/layout/header.jsp"/>
  <body>
	    
    
    <jsp:include page="/WEB-INF/jsp/layout/navbar.jsp"/>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <!-- Wrapper for Slides -->
      <div class="carousel-inner">
        <div class="item active">
          <!-- Set the first background image using inline CSS below. -->
          <div class="fill" style="background-image: url(public/media/spectacles/default.jpg);" /></div>
        <div class="carousel-caption">
          <h2>Exemple de spectacle 1</h2>
          <p>
            <a class="btn btn-lg btn-primary" href="#" role="button">Réserver</a>
          </p>
        </div>
      </div>
      <div class="item">
        <!-- Set the second background image using inline CSS below. -->
        <div class="fill" style="background-image: url(public/media/spectacles/img1.jpg);" /></div>
      <div class="carousel-caption">
        <h2>Exemple de spectacle 2</h2>
        <p><a class="btn btn-lg btn-primary" href="#" role="button">Réserver</a></p>
      </div>
    </div>
    <div class="item">
      <!-- Set the third background image using inline CSS below. -->
      <div class="fill" style="background-image: url(public/media/spectacles/img2.jpg);" /></div>
    <div class="carousel-caption">
      <h2>Exemple de spectacle 3</h2>
      <p><a class="btn btn-lg btn-primary" href="#" role="button">Réserver</a></p>
    </div>
  </div>
</div>
<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
</div><!-- /.carousel -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->
<div class="container marketing">
  <!-- Three columns of text below the carousel -->
  <div id="how">
    <h1 class="text-center">Fonctionnement</h1>
    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Des spectacles de qualité.<span class="text-muted"> Avec des acteurs exceptionnels.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
      </div>
      <div class="col-md-5">
        <img class="featurette-image img-responsive center-block" src="/public/static/img/quality.png" alt="Generic placeholder image">
      </div>
    </div>      <hr class="featurette-divider">      <div class="row featurette">
      <div class="col-md-7 col-md-push-5">
        <h2 class="featurette-heading">Un cadre exceptionnel.<span class="text-muted"> Voyez par vous même.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
      </div>
      <div class="col-md-5 col-md-pull-7">
        <img class="featurette-image img-responsive center-block" src="/public/media/spectacles/default.jpg" alt="Generic placeholder image">
      </div>
    </div>      <hr class="featurette-divider">      <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Des prix tout doux.<span class="text-muted"> Vraiment doux.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
      </div>
      <div class="col-md-5">
        <img class="featurette-image img-responsive center-block" src="/public/static/img/cash.png" alt="Generic placeholder image">
      </div>
    </div>      <hr class="featurette-divider">      <!-- /END THE FEATURETTES -->
  </div>
  <div id="spectacles">
    <h1 class="text-center">À l'affiche</h1>
    <div class="gallery">
      <ul class="row">
        <c:forEach items="${spectacles}" var="spectacle" >
        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-8">
          <a href="/bookings?id=${spectacle.number}">
            <img class="img-responsive" src="public/media/spectacles/default.jpg" name="${spectacle.name}">
            <h2>${spectacle.name}</h2>
          </a>
        </li>
        </c:forEach>
      </ul>
    </div>
  </div>
</div><!-- /.container -->
<jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
</body>
</html>
