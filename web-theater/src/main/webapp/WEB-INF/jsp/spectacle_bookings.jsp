<%-- 
    Document   : spectacle_bookings
    Created on : Apr 19, 2015, 5:21:55 PM
    Author     : jean-david
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <jsp:include page="/WEB-INF/jsp/layout/header.jsp"/>
  <body>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <h2><span class="glyphicon" id="messageIcon"></span> <span id="message"></span></h2>
            <p id="messageBody"></p>
          </div>
          <div class="modal-footer">
            <a class="btn btn-lg btn-primary" href="/index.html" role="button">Accueil</a>
            <a class="btn btn-lg btn-primary" href="/bookings/checkout" role="button">Mon panier</a>
          </div>
        </div>
      </div>
    </div>
    <div class="fill" style="background-image: url(public/media/spectacles/default.jpg);" />
      <jsp:include page="/WEB-INF/jsp/layout/navbar.jsp"/>
      <div class="container nested marketing">
        <div class="text-center">
          <h1><c:out value="${spectacle.name}" /> <span id="spetacleId" class="hidden"><c:out value="${spectacle.number}" /></span></h1>
        </div>
        <div class="col-lg-offset-2 col-lg-8">
          <div id="accordion" class="panel-group col-lg-12 col-md-12">
            <c:forEach items="${representations}" var="representation" varStatus="forloop">
              <div class="panel panel-default" id="representation">
                <div class="panel-heading"> 
                  <h3 class="panel-title">
                    <a href="#item${forloop.index}" data-parent="#accordion" data-toggle="collapse" class="indicator"><span id="accordionTitle">
                      <span id="date"><fmt:formatDate value="${representation.date}" pattern="dd/MM/yy à HH" />h</span>
                      <span id="room" class="pull-right"><c:out value="${representation.roomName}"/></span>
                    </a> 
                  </h3>
                </div>
                <div id="item${forloop.index}" class="panel-collapse collapse">
                  <div class="panel-body">   
                    <table class="table borderless">
                      <c:forEach items="${representation.seats}" var="seats" varStatus="seatsForLoop">
                        <tr id="seatsCategory">
                          <td><h4 id="categoryName"><c:out value="${seats.key.name}"/></h4></td> 
                          <td>
                            <form class="form-inline">                        
                              <div class="form-group">
                                <div class="input-group">
                                  <input onchange="bookit(event)" value="0" last-value="0" type="number" size="4" class="form-control" id="seatsNumber">
                                </div>
                              </div>
                            </form>
                          </td>
                          <td><c:out value="${seats.key.price}"/></td>
                          <td>€</td>
                        </tr>
                      </c:forEach>
                    </table>
                  </div>
                </div>
              </div>
            </c:forEach>
          </div>
          <div id="checkout-div" class="center-block hidden col-lg-12 col-md-12">
            <h2>Total <span class="pull-right"><span id="total">0</span> €</span></h2>
            <button onclick="bookingsCheckout()" class="btn btn-primary pull-right">Réserver</button>
          </div>
        </div>
      </div>
    </div>
  </body>
  <jsp:include page="/WEB-INF/jsp/layout/footer.jsp"/>
</html>
