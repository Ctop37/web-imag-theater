module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "../public/static/css/style.css": "src/less/style.less"
        }
      }
    },
    watch: {
      styles: {
        files: ['src/less/**/*.less'],
        tasks: ['less'],
        options: {
          nospawn: true
        }
      },
      scripts: {
        files: ['src/js/**/*.js'],
        tasks: ['concat', 'uglify'],
        options: {
          spawn: false
        }
      }
    },
    concat: {
      development: {
        src: 'src/js/**/*.js',
        dest: 'src/js/concat.js'
      }
    },
    uglify: {
      development: {
        src: 'src/js/concat.js',
        dest: '../public/static/js/script.js'
      }
    }
  });

  grunt.registerTask('default', ['less','concat','uglify', 'watch']);
};
