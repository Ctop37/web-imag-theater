/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Administrator;
import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import fr.ensimag.webTheater.beans.Spectacle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import junit.framework.TestCase;
import static junit.framework.TestCase.assertTrue;

/**
 *
 * @author thibaut
 */
public class DAOSpectacleImplTest extends TestCase {
    private DAOFactory daoFactory = null;
    private DAOSpectacleImpl instance = null;
    private DAOUserImpl dAOUserImpl = null;
    private DAORepresentationImpl dAORepresentationImpl = null;
    private DAOCategoryImpl dAOCategoryImpl = null;
    private DAORoomImpl dAORoomImpl = null;
    private DAODateImpl dAODateImpl = null;
    
    public DAOSpectacleImplTest(String testName) {
        super(testName);
    }
   
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAOSpectacleImpl(daoFactory);
        dAOUserImpl = new DAOUserImpl(daoFactory);
        dAORepresentationImpl = new DAORepresentationImpl(daoFactory);
        dAOCategoryImpl = new DAOCategoryImpl(daoFactory);
        dAORoomImpl = new DAORoomImpl(daoFactory);
        dAODateImpl = new DAODateImpl(daoFactory);

        Administrator administrator = new Administrator();
        administrator.setEmail("aille.ouille@Gmal.com");
        administrator.setFirstName("Aille");
        administrator.setLastName("Ouille");
        administrator.setPassword("passwordAchier");
        dAOUserImpl.createAdministrator(administrator);
        
        
        super.setUp();
    }
    
    public void tearDown() throws Exception {
        dAOUserImpl.deleteAdministrator("aille.ouille@Gmal.com");
        super.tearDown();

    }

    public void testCreateFindDelete1() {
        tNotFind(98765);
        tFindAll();
        tCreate(98765, "spectacle", "C:mon_fichier.png", "aille.ouille@Gmal.com");
        tFind(98765);
        tFindAll("98765 spectacle C:mon_fichier.png aille.ouille@Gmal.com");
        tNotFind(98766);
        tDelete(98765);
        tNotFind(98765);
        tFindAll();
    }

    public void testCreateFindDelete2() {
        tNotFind(98765);
        tFindAll();
        tCreate(98765, "spectacle98765", null, null);
        tFind(98765);
        tFindAll("98765 spectacle98765 null null");
        tNotFind(98766);
        tDelete(98765);
        tNotFind(98765);
        tFindAll();
    }

    private void tNotFind(int number) {
        System.out.println("notFind");
        Spectacle result = instance.find(number);
        assertNull(result);
        System.out.println("notFind Successful");        
    }
    
    public void tCreate(int number, String name, String path, String mail) {
        System.out.println("create");
        Spectacle spectacle = new Spectacle();
        spectacle.setNumber(number);
        spectacle.setName(name);
        spectacle.setImagePath(path);
        spectacle.setMailAdmin(mail);
        instance.create(spectacle);
        System.out.println("create Successful");        
    }
    
    public void tFind(int number) {
        System.out.println("find");
        Spectacle result = instance.find(number);
        assertNotNull(result);
        System.out.println("find Successful");        
    }
    
        public void tFindAll(String... names) {
        System.out.println("find all Spectacles");
        Set<Spectacle> res = instance.findAll();
        assertTrue(res.size() >= names.length);
        Iterator<Spectacle> iterator = res.iterator();
        Set<String> strings = new HashSet<>();
        while (iterator.hasNext()) {
            strings.add(iterator.next().toString());
        }
        for (int i=0; i<names.length; i++) {
            assertTrue(strings.contains(names[i]));
        }
        System.out.println("find all Spectacles Successful");
    }


    
    public void tDelete(int number) {
        System.out.println("delete");
        instance.delete(number);
        System.out.println("delete Successful");
    }
 
    public void testSetRepresentation() {
        tCreate(98765, "spectacle", null, null);
        buildRoomAndCategory();
        Spectacle spectacle = instance.find(98765);
        assertNotNull(spectacle);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),0);
        tCreateDateAndRepresentation(new Date(2015,2,8,21,0,0), 98765);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),1);
        tCreateDateAndRepresentation(new Date(2015,3,8,21,0,0), 98765);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),2);
        tCreateDateAndRepresentation(new Date(2015,2,9,21,0,0), 98765);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),3);
        tDeleteDateAndRepresentation(new Date(2015,2,8,21,0,0), 98765);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),2);
        tDeleteDateAndRepresentation(new Date(2015,3,8,21,0,0), 98765);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),1);
        tDeleteDateAndRepresentation(new Date(2015,2,9,21,0,0), 98765);
        instance.findRepresentations(spectacle);
        assertEquals(spectacle.getRepresentations().size(),0);
        destroyRoomAndCategories();
        tDelete(98765);
    }
    
    private void tCreateDateAndRepresentation(Date date, int spectacleNumber) {
        System.out.println("createDateAndRepresentation");
        dAODateImpl.create(date);
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(spectacleNumber);
        dAORepresentationImpl.create(representation);
        System.out.println("createDateAndRepresentation Sucessful");
    }
    
    private void tDeleteDateAndRepresentation(Date date, int spectacleNumber) {
        System.out.println("deleteDateAndRepresentation");
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(spectacleNumber);
        dAORepresentationImpl.delete(representation);
        dAODateImpl.delete(date);
        System.out.println("deleteDateAndRepresentation Successful");
    }
                
    public void buildRoomAndCategory() {
        Category category1 = null;
        Category category2 = null;
        Category category3 = null;
        category1 = new Category();
        category1.setName("Poulailler");
        category1.setPrice(52.17);
        category2 = new Category();
        category2.setName("Sous la scène");
        category2.setPrice(3.14);
        category3 = new Category();
        category3.setName("`A 200km");
        category3.setPrice(0.18);
        Room room = new Room();
        room.setName("salleSale");
        ArrayList<Row> rows = new ArrayList<>();
        Row row1 = new Row();
        row1.setRowNumber(1);
        row1.setCategory(category1);
        rows.add(row1);
        Row row2 = new Row();
        row2.setRowNumber(2);
        row2.setCategory(category1);
        rows.add(row2);
        Row row3 = new Row();
        row3.setRowNumber(3);
        row3.setCategory(category3);
        rows.add(row3);
        Row row4 = new Row();
        row4.setRowNumber(4);
        row4.setCategory(category1);
        rows.add(row4);
        Row row5 = new Row();
        row5.setRowNumber(5);
        row5.setCategory(category3);
        rows.add(row5);
        Row row6 = new Row();
        row6.setRowNumber(6);
        row6.setCategory(category2);
        rows.add(row6);
        Row row7 = new Row();
        row7.setRowNumber(7);
        row7.setCategory(category3);
        rows.add(row7);
        Row row8 = new Row();
        row8.setRowNumber(8);
        row8.setCategory(category2);
        rows.add(row8);
        Row row9 = new Row();
        row9.setRowNumber(9);
        row9.setCategory(category2);
        rows.add(row9);
        Row row10 = new Row();
        row10.setRowNumber(10);
        row10.setCategory(category3);
        rows.add(row10);
        room.setRows(rows);
        dAOCategoryImpl.create(category1);
        dAOCategoryImpl.create(category2);
        dAOCategoryImpl.create(category3);
        dAORoomImpl.create(room);
    }
    
    public void destroyRoomAndCategories() {
        dAORoomImpl.deleteRoom("salleSale");
        dAOCategoryImpl.delete("Poulailler");
        dAOCategoryImpl.delete("Sous la scène");
        dAOCategoryImpl.delete("`A 200km");
    }

}
