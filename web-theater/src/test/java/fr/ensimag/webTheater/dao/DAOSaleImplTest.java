/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.beans.Seat;
import fr.ensimag.webTheater.beans.Spectacle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import junit.framework.TestCase;
import static org.junit.Assert.*;

/**
 *
 * @author thibaut
 */
public class DAOSaleImplTest extends TestCase {

    private DAOFactory daoFactory = null;
    private DAOSaleImpl instance = null;
    private DAOUserImpl dAOUserImpl = null;
    private DAOCaseImpl dAOCaseImpl = null;
    private DAOCategoryImpl dAOCategoryImpl = null;
    private DAORoomImpl dAORoomImpl = null;
    private DAODateImpl dAODateImpl = null;
    private DAOBookingImpl dAOBookingImpl = null;
    private DAORepresentationImpl dAORepresentationImpl = null;
    private DAOSpectacleImpl dAOSpectacleImpl = null;

    public DAOSaleImplTest(String testName) {
        super(testName);
    }

    @Override
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAOSaleImpl(daoFactory);
        dAOUserImpl = new DAOUserImpl(daoFactory);
        dAOCaseImpl = new DAOCaseImpl(daoFactory);
        dAOCategoryImpl = new DAOCategoryImpl(daoFactory);
        dAORoomImpl = new DAORoomImpl(daoFactory);
        dAODateImpl = new DAODateImpl(daoFactory);
        dAOBookingImpl = new DAOBookingImpl(daoFactory);
        dAORepresentationImpl = new DAORepresentationImpl(daoFactory);
        dAOSpectacleImpl = new DAOSpectacleImpl(daoFactory);
        Customer customer = new Customer();
        customer.setEmail("frite.belge@frit.fr");
        customer.setFirstName("Frite");
        customer.setLastName("Belge");
        customer.setPassword("monSuperpasswordHasché");
        Case c = new Case();
        c.setNumber(42);
        c.setTicketNumber(748);
        c.setUserMail("frite.belge@frit.fr");
        dAOUserImpl.createCustomer(customer);
        dAOCaseImpl.create(c);
        buildRoomAndCategory();
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        dAODateImpl.create(date);
        
        Spectacle spectacle = new Spectacle();
        spectacle.setNumber(100);
        spectacle.setName("Spectacle du Tetrano");
        dAOSpectacleImpl.create(spectacle);
        
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(100);
        dAORepresentationImpl.create(representation);
        super.setUp();
    }

    @Override
    public void tearDown() throws Exception {
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);

        dAOCaseImpl.delete(42);
        dAOUserImpl.deleteCustomer("frite.belge@frit.fr");
        
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(100);
        dAORepresentationImpl.delete(representation);

        dAOSpectacleImpl.delete(100);
        destroyRoomAndCategories();
        dAODateImpl.delete(date);
        super.tearDown();
    }

    /**
     * Test of create method, of class DAOCaseImpl.
     */
    public void testCreate() {
        tCreate(2, 3, 5, 7, 9, 2);
        tDelete(2, 3, 5, 7, 9, 2);
        Booking b = tCreateBooking(12, 5, 13, 5, 14, 5);
        tCreateSaleFromBooking(b);
        tDelete(12, 5, 13, 5, 14, 5);
        
        Booking b2 = tCreateBooking(12, 5, 13, 5, 14, 5);
        tCreateSaleFromBooking(b2);
        Booking b3 = tCreateBooking(18, 5, 19, 5, 20, 5);
        tCreateSaleFromBooking(b3);
        tDelete(12, 5, 13, 5, 14, 5);
        tDelete(18, 5, 19, 5, 20, 5);
        
    }

    public void tCreate(int... seatRowNumber) {
        System.out.println("create");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Sale sale = new Sale();
        sale.setDate(date);
        sale.setIdCase(42);
        Room room = new Room();
        room.setName("salleSale");
        HashSet<Seat> seats = new HashSet<>();
        for (int i = 0; i < (seatRowNumber.length) / 2; i++) {
            Seat seat = new Seat();
            Row row = new Row();
            row.setRoom(room);
            seat.setRow(row);
            seat.setId(seatRowNumber[2 * i]);
            row.setRowNumber(seatRowNumber[2 * i + 1]);
            seats.add(seat);
        }
        sale.setSeats(seats);
        instance.create(sale);
        System.out.println("create Successful");
    }

    public void tDelete(int... seatRowNumber) {
        System.out.println("delete");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Room room = new Room();
        room.setName("salleSale");
        for (int i = 0; i < (seatRowNumber.length) / 2; i++) {
            Seat seat = new Seat();
            Row row = new Row();
            row.setRoom(room);
            seat.setRow(row);
            seat.setId(seatRowNumber[2 * i]);
            row.setRowNumber(seatRowNumber[2 * i + 1]);
            instance.delete(42, date, seat);
        }
        System.out.println("delete Successful");
    }

    public Booking tCreateBooking(int... seatRowNumber) {
        System.out.println("create Booking");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        HashSet<Seat> seats = new HashSet<>();
        Room room = new Room();
        room.setName("salleSale");
        for (int i = 0; i < (seatRowNumber.length) / 2; i++) {
            Seat seat = new Seat();
            Row row = new Row();
            row.setRoom(room);
            seat.setRow(row);
            seat.setId(seatRowNumber[2 * i]);
            row.setRowNumber(seatRowNumber[2 * i + 1]);
            seats.add(seat);
        }
        Booking booking = new Booking();
        booking.setDate(date);
        booking.setUserMail("frite.belge@frit.fr");
        booking.setSeats(seats);
        dAOBookingImpl.create(booking);
        System.out.println("create Booking Successful");
        return booking;
    }

    public void tCreateSaleFromBooking(Booking booking) {
        System.out.println("create Sale from Booking");
        Case c = new Case();
        c.setNumber(42);
        c.setTicketNumber(748);
        c.setUserMail(booking.getUserMail());
        instance.createFromBooking(booking, c);
        System.out.println("create Sale from Booking Successful");
    }

    public void buildRoomAndCategory() {
        Category category1 = null;
        Category category2 = null;
        Category category3 = null;
        category1 = new Category();
        category1.setName("Poulailler");
        category1.setPrice(52.17);
        category2 = new Category();
        category2.setName("Sous la scène");
        category2.setPrice(3.14);
        category3 = new Category();
        category3.setName("`A 200km");
        category3.setPrice(0.18);
        Room room = new Room();
        room.setName("salleSale");
        ArrayList<Row> rows = new ArrayList<>();
        Row row1 = new Row();
        row1.setRowNumber(1);
        row1.setCategory(category1);
        rows.add(row1);
        Row row2 = new Row();
        row2.setRowNumber(2);
        row2.setCategory(category1);
        rows.add(row2);
        Row row3 = new Row();
        row3.setRowNumber(3);
        row3.setCategory(category3);
        rows.add(row3);
        Row row4 = new Row();
        row4.setRowNumber(4);
        row4.setCategory(category1);
        rows.add(row4);
        Row row5 = new Row();
        row5.setRowNumber(5);
        row5.setCategory(category3);
        rows.add(row5);
        Row row6 = new Row();
        row6.setRowNumber(6);
        row6.setCategory(category2);
        rows.add(row6);
        Row row7 = new Row();
        row7.setRowNumber(7);
        row7.setCategory(category3);
        rows.add(row7);
        Row row8 = new Row();
        row8.setRowNumber(8);
        row8.setCategory(category2);
        rows.add(row8);
        Row row9 = new Row();
        row9.setRowNumber(9);
        row9.setCategory(category2);
        rows.add(row9);
        Row row10 = new Row();
        row10.setRowNumber(10);
        row10.setCategory(category3);
        rows.add(row10);
        room.setRows(rows);
        dAOCategoryImpl.create(category1);
        dAOCategoryImpl.create(category2);
        dAOCategoryImpl.create(category3);
        dAORoomImpl.create(room);
    }

    public void destroyRoomAndCategories() {
        dAORoomImpl.deleteRoom("salleSale");
        dAOCategoryImpl.delete("Poulailler");
        dAOCategoryImpl.delete("Sous la scène");
        dAOCategoryImpl.delete("`A 200km");
    }
}
