/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import java.util.Date;
import junit.framework.TestCase;

/**
 *
 * @author thibaut
 */
public class DAOArchiveImplTest extends TestCase {
    private DAOFactory daoFactory = null;
    private DAOArchiveImpl instance = null;
    

    public DAOArchiveImplTest(String testName) {
        super(testName);
    }
    
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAOArchiveImpl(daoFactory);
        super.setUp();
    }
    
    public void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of archive method, of class DAOArchiveImpl.
     * In fact, it is something difficult to test. To test it, we have to look
     * at the return String and parse it. It is so something complicated and I
     * prefered to make the test by adding Tuple in the database, using this test
     * and then verifying that the Tuple are well deleted and that the String
     * corresponds to what we want.
     * We don't really have a non-regression test. The only one reason for
     * leting this test there, is that if we change something in the code and
     * the method don't fonction, perhaps this test with throw an Exception and
     * we will find the error.
     */
    public void testArchiveWithoutAddingThingsInDatabase() {
        System.out.println("archive without adding things in database");
        Date dateDebut = new Date(2013-1900, 9-1, 1, 0, 0);
        Date dateFin = new Date(2014-1900,6-1,30,23,0);
        System.out.println("dateDebut: " + dateDebut);
        System.out.println("dateFin: " + dateFin);
        System.out.println(instance.archive(dateDebut, dateFin));
        System.out.println("archive without adding things in database Successful");
    }
    
}
