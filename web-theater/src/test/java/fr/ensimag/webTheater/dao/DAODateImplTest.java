/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import java.util.Date;
import junit.framework.TestCase;
/**
 *
 * @author thibaut
 */
public class DAODateImplTest extends TestCase{
    private DAOFactory daoFactory = null;
    private DAODateImpl instance = null;
    
    public DAODateImplTest(String testName) {
        super(testName);
    }

    @Override
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAODateImpl(daoFactory);
        super.setUp();
    }
    
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCreateAndDelete() {
        tCreate();
        tDelete();
    }
    
    public void tCreate() {
        System.out.println("create");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        instance.create(date);
        System.out.println("create Successful");
    }
    
    public void tDelete() {
        System.out.println("delete");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        instance.delete(date);
        System.out.println("delete Successful");        
    }
    
    public void testDateAndString() {
        System.out.println("conversion date String");
        Date date = new Date();
        date.setHours(18);
        date.setMonth(11);
        date.setYear(2020-1900);
        date.setDate(20);
        String s = instance.stringOfDate(date);
        assertTrue(s.equals("2020-12-20 18:00:00"));
        Date date2 = instance.dateOfString(s);
        assertEquals(date.getYear(),2020-1900);
        assertEquals(date.getMonth(),11);
        assertEquals(date.getDate(),20);
        assertEquals(date.getHours(),18);
        System.out.println("conversion date String Successful");
    }

   public void testDateAndString2() {
        System.out.println("conversion date String");
        Date date = new Date();
        date.setHours(0);
        date.setMonth(0);
        date.setYear(2015-1900);
        date.setDate(31);
        String s = instance.stringOfDate(date);
        assertTrue(s.equals("2015-1-31 0:00:00"));
        Date date2 = instance.dateOfString(s);
        assertEquals(date.getYear(),2015-1900);
        assertEquals(date.getMonth(),0);
        assertEquals(date.getDate(),31);
        assertEquals(date.getHours(),0);
        System.out.println("conversion date String Successful");
    }
}