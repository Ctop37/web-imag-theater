/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import java.util.ArrayList;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author thibaut
 */
public class DAORoomImplTest extends TestCase {
    private DAOFactory daoFactory = null;
    private DAORoomImpl instance = null;
    private DAOCategoryImpl daoCategory = null;
    private Category category1 = null;
    private Category category2 = null;
    private Category category3 = null;

    public DAORoomImplTest(String testName) {
        super(testName);
    }
    
        @Override
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAORoomImpl(daoFactory);
        daoCategory = new DAOCategoryImpl(daoFactory);
        category1 = new Category();
        category1.setName("Poulailler");
        category1.setPrice(52.17);
        category2 = new Category();
        category2.setName("Sous la scène");
        category2.setPrice(3.14);
        category3 = new Category();
        category3.setName("`A 200km");
        category3.setPrice(0.18);
        daoCategory.create(category1);
        daoCategory.create(category2);
        daoCategory.create(category3);
        super.setUp();
    }
    
    @Override
    public void tearDown() throws Exception {
        daoCategory.delete("Poulailler");
        daoCategory.delete("Sous la scène");
        daoCategory.delete("`A 200km");
        super.tearDown();
    }

    public void testcomplet() {
        tFind();
        tCreate("salleSale");
        tFind("salleSale");
        tCreate("sallePropre");
        tFind("salleSale","sallePropre");
        tCreate("salle nommée Tetrano (why not?)");
        tFind("salleSale","sallePropre","salle nommée Tetrano (why not?)");
        tDelete("salleSale");
        tDelete("sallePropre");
        tDelete("salle nommée Tetrano (why not?)");
        tFind();
    }
    
    public void tFind(String... names) {
        System.out.println("find room names");
        Set<String> res = instance.findAll();
        assertTrue(res.size() >= names.length);
        for (int i =0; i<names.length; i++) {
            assertTrue(res.contains(names[i]));
        }
        System.out.println("find room names Successful");
    }
    
    
    public void tCreate(String roomName) {
        System.out.println("create");
        Room room = new Room();
        room.setName(roomName);
        ArrayList<Row> rows = new ArrayList<>();
        Row row1 = new Row();
        row1.setRowNumber(1);
        row1.setCategory(category1);
        rows.add(row1);
        Row row2 = new Row();
        row2.setRowNumber(2);
        row2.setCategory(category1);
        rows.add(row2);
        Row row3 = new Row();
        row3.setRowNumber(3);
        row3.setCategory(category3);
        rows.add(row3);
        Row row4 = new Row();
        row4.setRowNumber(4);
        row4.setCategory(category1);
        rows.add(row4);
        Row row5 = new Row();
        row5.setRowNumber(5);
        row5.setCategory(category3);
        rows.add(row5);
        Row row6 = new Row();
        row6.setRowNumber(6);
        row6.setCategory(category2);
        rows.add(row6);
        Row row7 = new Row();
        row7.setRowNumber(7);
        row7.setCategory(category3);
        rows.add(row7);
        Row row8 = new Row();
        row8.setRowNumber(8);
        row8.setCategory(category2);
        rows.add(row8);
        Row row9 = new Row();
        row9.setRowNumber(9);
        row9.setCategory(category2);
        rows.add(row9);
        Row row10 = new Row();
        row10.setRowNumber(10);
        row10.setCategory(category3);
        rows.add(row10);
        room.setRows(rows);
        instance.create(room);
        System.out.println("create Successful");
    }
    
    public void tDelete(String roomName) {
        System.out.println("delete");
        instance.deleteRoom(roomName);
        System.out.println("delete Successful");
    }
}