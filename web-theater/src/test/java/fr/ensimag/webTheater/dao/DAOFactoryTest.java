/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 * @author thibaut
 */
public class DAOFactoryTest extends TestCase {
    
    public DAOFactoryTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getInstance method, of class DAOFactory.
     */
    public void test() {
        System.out.println("getInstance");
        DAOFactory result = null;
        try {
            result = DAOFactory.getInstance();
            
        } catch (DAOException e) {
            System.out.println("Problem DAO EXCEPTION");
            fail();
        } catch (DAOConfigurationException e) {
            System.out.println("Problem DAO Configuration EXCEPTION");
            fail();
        }
        assertNotNull(result);
        System.out.println("Loading of JDBC driver succeeded.");
        Connection connection = null;
        try {
            connection = result.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFactoryTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        assertNotNull(connection);
        System.out.println("Connection to the database succeeded.");
    }    
}
