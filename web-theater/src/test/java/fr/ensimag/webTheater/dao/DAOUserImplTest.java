/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Administrator;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.User;
import junit.framework.TestCase;

/**
 *
 * @author thibaut
 */
public class DAOUserImplTest extends TestCase {
    
    private DAOFactory daoFactory = null;
    private DAOUserImpl instance = null;
    
    public DAOUserImplTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAOUserImpl(daoFactory);

        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

 //////////////////////////////////////////////////////////////////
 //   TEST SPECIFICS FOR USERS (NO CUSTOMERS OR ADMINISTRATORS:  //
 //////////////////////////////////////////////////////////////////
    
    public void testUser() {
        tFind1();
        tCreate1();
        tFind2();
        tCreate2();
        tDelete();
        tFind3();   
    }
    /**
     * We search a administrator having frite.belge@frit.fr as email.
     * This administrator isn't yet in the databas, so we assert that a DAOException
 is well thrown.
     */
    public void tFind1() {
        System.out.println("find1");
        String mail = "frite.belge@frit.fr";
        User result = instance.find(mail);
        assertNull(result);
        System.out.println("find1 Successful");
    }

    /**
     * We create a administrator having frite.belge@frit.fr as email.
     */
    public void tCreate1() {
        System.out.println("create1");
        User user = new User();
        user.setEmail("frite.belge@frit.fr");
        user.setFirstName("Frite");
        user.setLastName("Belge");
        user.setPassword("monSuperpasswordHasché");
        instance.create(user);
        System.out.println("create1 Successful");
    }

    /**
     * We assert that we can find this User in the database.
     */
    public void tFind2() {
        System.out.println("find2");
        String mail = "frite.belge@frit.fr";
        User result = instance.find(mail);
        assertNotNull(result);
        System.out.println("find2 Successful");
    }

    /**
     * We try to add another Usre having the same mail and we verify that
     * a DAOException is well thrown.
     */
    public void tCreate2() {
        System.out.println("create2");
        User user = new User();
        user.setEmail("frite.belge@frit.fr");
        user.setFirstName("FriteLeRetour");
        user.setLastName("BelgeOneMoreTime");
        user.setPassword("monSuperpasswordHaschéDifférentDuPremier");
        try {
            instance.create(user);
            fail("Exception not thrown");
        } catch (DAOException e) {
            System.out.println("The mistake due two the creation of another"
                    + " User having the same mail has well been thrown =D "
                    + "\nHere is it: " + e);
        }
        System.out.println("create2 Successful");
    }

    /**
     * We delete the User.
     */
    public void tDelete() {
        System.out.println("delete");
        String mail = "frite.belge@frit.fr";
        instance.delete(mail);
        System.out.println("delete Successful");
    }
    
    /**
     * We assert that the User isn't in the database (since we deleted it)
     */
    public void tFind3() {
        System.out.println("find3");
        String mail = "frite.belge@frit.fr";
        User result = instance.find(mail);
        assertNull(result);
        System.out.println("find3 Successful");
    }
    
 //////////////////////////////////////////////////////////////////
 //////     TEST WITH BOTH CUSTOMERS AND ADMINISTRATORS:     //////
 //////////////////////////////////////////////////////////////////

    public void testComplet() {
        tFindCustomer1();
        tCreateCustomer1();
        tFindCustomer2();
        tFindAdministrator1();
        tCreateAdministrator1();
        tFindAdministrator2();
        tFindCustomer3();
        tCreateCustomer2();
        tFindCustomer4();
        tFindAdministrator3();
        tCreateAdministrator2();
        tFindAdministrator4();
        tDeleteAdministrator1();
        tFindAdministrator5();
        tFindCustomer5();
        tCreateAdministrator3();
        tDeleteCustomer1();
        tFindCustomer6();
        tDeleteCustomer2();
        tFindAdministrator6();
        tCreateCustomer3();
        tDeleteAdministrator2();
        tFindAdministrator7();
    }
    public void tFindCustomer1() {
        System.out.println("testFindCustomer1");
        String mail = "aille.ouille@Gmal.com";
        Customer result = instance.findCustomer(mail);
        assertNull(result);
        System.out.println("testFindCustomer1 Successful");
    }
    
    public void tCreateCustomer1() {
        System.out.println("testCreateCustomer1");
        Customer customer = new Customer();
        customer.setEmail("aille.ouille@Gmal.com");
        customer.setFirstName("Aille");
        customer.setLastName("Ouille");
        customer.setPassword("passwordAchier");
        instance.createCustomer(customer);
        System.out.println("testCreateCustomer1 Successful");
    }
    

    public void tFindCustomer2() {
        System.out.println("testFindCustomer2");
        String mail = "aille.ouille@Gmal.com";
        Customer result = instance.findCustomer(mail);
        assertNotNull(result);
        System.out.println("testFindCustomer2 Successful");
    }
    
    public void tFindAdministrator1() {
        System.out.println("testFindAdministrator1");
        String mail = "hiphiphip.oura@yahoo.fr";
        Administrator result = instance.findAdministrator(mail);
        assertNull(result);
        System.out.println("testFindAdministrator1 Successful");
    }
    
    public void tCreateAdministrator1() {
        System.out.println("testCreateAdministrator1");
        Administrator administrator = new Administrator();
        administrator.setEmail("hiphiphip.oura@yahoo.fr");
        administrator.setFirstName("Trop");
        administrator.setLastName("Joyeux");
        administrator.setPassword("passwordContent");
        instance.createAdministrator(administrator);
        System.out.println("testCreateAdministrator1 Successful");
    }
    
    public void tFindAdministrator2() {
        System.out.println("testFindAdministrator2");
        String mail = "hiphiphip.oura@yahoo.fr";
        Administrator result = instance.findAdministrator(mail);
        assertNotNull(result);
        System.out.println("testFindAdministrator2 Successful");
    }

    
    public void tFindCustomer3() {
        System.out.println("testFindCustomer3");
        String mail = "hiphiphip.oura@yahoo.fr";
        Customer result = instance.findCustomer(mail);
        assertNull(result);
        System.out.println("testFindCustomer3 Successful");
    }
    
    public void tCreateCustomer2() {
        System.out.println("testCreateCustomer2");
        Customer customer = new Customer();
        customer.setEmail("hiphiphip.oura@yahoo.fr");
        customer.setFirstName("Trop");
        customer.setLastName("Joyeux");
        customer.setPassword("passwordContent");
        instance.createCustomer(customer);
        System.out.println("testCreateCustomer2 Successful");
    }
    
    public void tFindCustomer4() {
        System.out.println("testFindCustomer4");
        String mail = "hiphiphip.oura@yahoo.fr";
        Customer result = instance.findCustomer(mail);
        assertNotNull(result);
        System.out.println("testFindCustomer4 Successful");
    }

    public void tFindAdministrator3() {
        System.out.println("testFindAdministrator3");
        String mail = "aille.ouille@Gmal.com";
        Administrator result = instance.findAdministrator(mail);
        assertNull(result);
        System.out.println("testFindAdministrator3 Successful");
    }
    
    public void tCreateAdministrator2() {
        System.out.println("testCreateAdministrator2");
        Administrator administrator = new Administrator();
        administrator.setEmail("aille.ouille@Gmal.com");
        administrator.setFirstName("Aille");
        administrator.setLastName("Ouille");
        administrator.setPassword("passwordAchier");
        instance.createAdministrator(administrator);
        System.out.println("testCreateAdministrator2 Successful");
    }
    
    public void tFindAdministrator4() {
        System.out.println("testFindAdministrator4");
        String mail = "aille.ouille@Gmal.com";
        Administrator result = instance.findAdministrator(mail);
        assertNotNull(result);
        System.out.println("testFindAdministrator4 Successful");
    }
    
    public void tDeleteAdministrator1() {
        System.out.println("deleteAdministrator1");
        String mail = "aille.ouille@Gmal.com";
        instance.deleteAdministrator(mail);
        System.out.println("deleteAdministrator1 Successful");
    }

    public void tFindAdministrator5() {
        System.out.println("testFindAdministrator5");
        String mail = "aille.ouille@Gmal.com";
        Administrator result = instance.findAdministrator(mail);
        assertNull(result);
        System.out.println("testFindAdministrator5 Successful");
    }

    public void tFindCustomer5() {
        System.out.println("testFindCustomer5");
        String mail = "aille.ouille@Gmal.com";
        Customer result = instance.findCustomer(mail);
        assertNotNull(result);
        System.out.println("testFindCustomer5 Successful");
    }

    public void tCreateAdministrator3() {
        System.out.println("testCreateAdministrator3");
        Administrator administrator = new Administrator();
        administrator.setEmail("aille.ouille@Gmal.com");
        administrator.setFirstName("Aille");
        administrator.setLastName("Ouille7");
        administrator.setPassword("passwordAchier");
        try {
            instance.createAdministrator(administrator);
            fail();
        } catch (DAOException e) {}
        System.out.println("testCreateAdministrator3 Successful");
    }

    public void tDeleteCustomer1() {
        System.out.println("deleteCustomer1");
        String mail = "aille.ouille@Gmal.com";
        instance.deleteCustomer(mail);
        System.out.println("deleteCustomer1 Successful");
    }


    public void tFindCustomer6() {
        System.out.println("testFindCustomer6");
        String mail = "aille.ouille@Gmal.com";
        Customer result = instance.findCustomer(mail);
        assertNull(result);
        System.out.println("testFindCustomer6 Successful");
    }
    
    public void tDeleteCustomer2() {
        System.out.println("deleteCustomer2");
        String mail = "hiphiphip.oura@yahoo.fr";
        instance.deleteCustomer(mail);
        System.out.println("delcreateeteCustomer2 Successful");
    }

    public void tFindAdministrator6() {
        System.out.println("testFindAdministrator6");
        String mail = "hiphiphip.oura@yahoo.fr";
        Administrator result = instance.findAdministrator(mail);
        assertNotNull(result);
        System.out.println("testFindAdministrator6 Successful");
    }
    
    public void tCreateCustomer3() {
        System.out.println("testCreateCustomer3");
        Customer customer = new Customer();
        customer.setEmail("hiphiphip.oura@yahoo.fr");
        customer.setFirstName("Trop");
        customer.setLastName("Joyeux");
        customer.setPassword("passwordContentX");
        try {
            instance.createCustomer(customer);
            fail();
        } catch (DAOException e) {}
        System.out.println("testCreateCustomer3 Successful");
    }

    public void tDeleteAdministrator2() {
        System.out.println("deleteAdministrator2");
        String mail = "hiphiphip.oura@yahoo.fr";
        instance.deleteAdministrator(mail);
        System.out.println("deleteAdministrator2 Successful");
    }

    public void tFindAdministrator7() {
        System.out.println("testFindAdministrator7");
        String mail = "hiphiphip.oura@yahoo.fr";
        Administrator result = instance.findAdministrator(mail);
        assertNull(result);
        System.out.println("testFindAdministrator7 Successful");
    }
}
