/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import fr.ensimag.webTheater.beans.Sale;
import fr.ensimag.webTheater.beans.Seat;
import fr.ensimag.webTheater.beans.Spectacle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author thibaut
 */
public class DAOBookingImplTest extends TestCase {

    private DAOFactory daoFactory = null;
    private DAOBookingImpl instance = null;
    private DAOUserImpl dAOUserImpl = null;
    private DAOCategoryImpl dAOCategoryImpl = null;
    private DAORoomImpl dAORoomImpl = null;
    private DAODateImpl dAODateImpl = null;
    private DAORepresentationImpl dAORepresentationImpl = null;
    private DAOSpectacleImpl dAOSpectacleImpl = null;

    public DAOBookingImplTest(String testName) {
        super(testName);
    }

    @Override
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAOBookingImpl(daoFactory);
        dAOUserImpl = new DAOUserImpl(daoFactory);
        dAOCategoryImpl = new DAOCategoryImpl(daoFactory);
        dAORoomImpl = new DAORoomImpl(daoFactory);
        dAODateImpl = new DAODateImpl(daoFactory);
        dAORepresentationImpl = new DAORepresentationImpl(daoFactory);
        dAOSpectacleImpl = new DAOSpectacleImpl(daoFactory);

        Customer customer1 = new Customer();
        customer1.setEmail("youhou@yahoo.fr");
        customer1.setFirstName("happy");
        customer1.setLastName("hourra");
        customer1.setPassword("monSuperpasswordHasché1");
        dAOUserImpl.createCustomer(customer1);

        Customer customer2 = new Customer();
        customer2.setEmail("frite.belge@frit.fr");
        customer2.setFirstName("Frite");
        customer2.setLastName("Belge");
        customer2.setPassword("monSuperpasswordHasché2");
        dAOUserImpl.createCustomer(customer2);

        Customer customer3 = new Customer();
        customer3.setEmail("ailleouille@Gmal.com");
        customer3.setFirstName("aille");
        customer3.setLastName("ouille");
        customer3.setPassword("monSuperpasswordHasché3");
        dAOUserImpl.createCustomer(customer3);

        buildRoomAndCategory();
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        dAODateImpl.create(date);
        
        Spectacle spectacle = new Spectacle();
        spectacle.setNumber(100);
        spectacle.setName("Spectacle du Tetrano");
        dAOSpectacleImpl.create(spectacle);
        
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(100);
        dAORepresentationImpl.create(representation);
        
        super.setUp();
    }

    @Override
    public void tearDown() throws Exception {
        dAOUserImpl.deleteCustomer("youhou@yahoo.fr");
        dAOUserImpl.deleteCustomer("frite.belge@frit.fr");
        dAOUserImpl.deleteCustomer("ailleouille@Gmal.com");

        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);

        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(100);
        dAORepresentationImpl.delete(representation);

        dAOSpectacleImpl.delete(100);
        destroyRoomAndCategories();
        
        dAODateImpl.delete(date);
        super.tearDown();
    }

    /**
     * Test of create method, of class DAOCaseImpl.
     */
    public void testCreate() {
        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 0);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tCreateBooking("frite.belge@frit.fr", 1, 1, 1, 2, 9, 1);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 3);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tCreateBooking("youhou@yahoo.fr", 8, 1, 7, 2, 9, 3);

        tFindBookingByCustomer("youhou@yahoo.fr", 3);
        tFindBookingByCustomer("frite.belge@frit.fr", 3);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tCreateWithMistake("ailleouille@Gmal.com", 1, 1);
        tCreateWithMistake("youhou@yahoo.fr", 7, 2);
        
        tFindBookingByCustomer("youhou@yahoo.fr", 3);
        tFindBookingByCustomer("frite.belge@frit.fr", 3);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tDeleteBooking("youhou@yahoo.fr", 8, 1, 7, 2, 9, 3);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 3);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tCreateBooking("ailleouille@Gmal.com", 8, 1, 7, 2, 19, 1);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 3);
        tFindBookingByCustomer("ailleouille@Gmal.com", 3);

        tDeleteBooking("frite.belge@frit.fr", 1, 1, 1, 2, 9, 1);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 0);
        tFindBookingByCustomer("ailleouille@Gmal.com", 3);

        tDeleteBooking("ailleouille@Gmal.com", 8, 1, 7, 2, 19, 1);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 0);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tCreateBooking("frite.belge@frit.fr", 13, 2, 14, 2, 15, 2);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 3);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

        tDeleteBooking("frite.belge@frit.fr", 13, 2, 14, 2, 15, 2);

        tFindBookingByCustomer("youhou@yahoo.fr", 0);
        tFindBookingByCustomer("frite.belge@frit.fr", 0);
        tFindBookingByCustomer("ailleouille@Gmal.com", 0);

    }

    public void tCreateBooking(String userMail, int... seatRowNumber) {
        System.out.println("create Booking");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        HashSet<Seat> seats = new HashSet<>();
        Room room = new Room();
        room.setName("salleSale");
        for (int i = 0; i < (seatRowNumber.length) / 2; i++) {
            Seat seat = new Seat();
            Row row = new Row();
            row.setRoom(room);
            seat.setRow(row);
            seat.setId(seatRowNumber[2 * i]);
            row.setRowNumber(seatRowNumber[2 * i + 1]);
            seats.add(seat);
        }
        Booking booking = new Booking();
        booking.setDate(date);
        booking.setUserMail(userMail);
        booking.setSeats(seats);
        instance.create(booking);
        System.out.println("create Booking Successful");
    }

    public void tCreateWithMistake(String userMail, int seatNumber, int rowNumber) {
        System.out.println("create Booking with Mistake");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        HashSet<Seat> seats = new HashSet<>();
        Room room = new Room();
        room.setName("salleSale");
        Seat seat = new Seat();
        Row row = new Row();
        row.setRoom(room);
        seat.setRow(row);
        seat.setId(seatNumber);
        row.setRowNumber(rowNumber);
        seats.add(seat);
        Booking booking = new Booking();
        booking.setDate(date);
        booking.setUserMail(userMail);
        booking.setSeats(seats);
        try {
            instance.create(booking);
            fail("No exception thrown.");
        } catch (DAOException e) {
            System.out.println("Everything is alright: the exception has been "
                    + "thrown.");
        }
        System.out.println("create Booking with Mistake Successful");
    }

    public void tFindBookingByCustomer(String userMail, int nombreAttendu) {
        System.out.println("find booking");
        Set<Booking> res = instance.findBookingByCustomer(userMail);
        assertNotNull(res);
        assertEquals(nombreAttendu, res.size());
        System.out.println("find booking Successful");
    }

    public void tDeleteBooking(String userMail, int... seatRowNumber) {
        System.out.println("delete Booking");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        HashSet<Seat> seats = new HashSet<>();
        Room room = new Room();
        room.setName("salleSale");
        for (int i = 0; i < (seatRowNumber.length) / 2; i++) {
            Seat seat = new Seat();
            Row row = new Row();
            row.setRoom(room);
            seat.setRow(row);
            seat.setId(seatRowNumber[2 * i]);
            row.setRowNumber(seatRowNumber[2 * i + 1]);
            seats.add(seat);
        }
        Booking booking = new Booking();
        booking.setDate(date);
        booking.setUserMail(userMail);
        booking.setSeats(seats);
        instance.delete(booking);
        System.out.println("delete Booking Successful");
    }

    public void buildRoomAndCategory() {
        Category category1 = null;
        Category category2 = null;
        Category category3 = null;
        category1 = new Category();
        category1.setName("Poulailler");
        category1.setPrice(52.17);
        category2 = new Category();
        category2.setName("Sous la scène");
        category2.setPrice(3.14);
        category3 = new Category();
        category3.setName("`A 200km");
        category3.setPrice(0.18);
        Room room = new Room();
        room.setName("salleSale");
        ArrayList<Row> rows = new ArrayList<>();
        Row row1 = new Row();
        row1.setRowNumber(1);
        row1.setCategory(category1);
        rows.add(row1);
        Row row2 = new Row();
        row2.setRowNumber(2);
        row2.setCategory(category1);
        rows.add(row2);
        Row row3 = new Row();
        row3.setRowNumber(3);
        row3.setCategory(category3);
        rows.add(row3);
        Row row4 = new Row();
        row4.setRowNumber(4);
        row4.setCategory(category1);
        rows.add(row4);
        Row row5 = new Row();
        row5.setRowNumber(5);
        row5.setCategory(category3);
        rows.add(row5);
        Row row6 = new Row();
        row6.setRowNumber(6);
        row6.setCategory(category2);
        rows.add(row6);
        Row row7 = new Row();
        row7.setRowNumber(7);
        row7.setCategory(category3);
        rows.add(row7);
        Row row8 = new Row();
        row8.setRowNumber(8);
        row8.setCategory(category2);
        rows.add(row8);
        Row row9 = new Row();
        row9.setRowNumber(9);
        row9.setCategory(category2);
        rows.add(row9);
        Row row10 = new Row();
        row10.setRowNumber(10);
        row10.setCategory(category3);
        rows.add(row10);
        room.setRows(rows);
        dAOCategoryImpl.create(category1);
        dAOCategoryImpl.create(category2);
        dAOCategoryImpl.create(category3);
        dAORoomImpl.create(room);
    }

    public void destroyRoomAndCategories() {
        dAORoomImpl.deleteRoom("salleSale");
        dAOCategoryImpl.delete("Poulailler");
        dAOCategoryImpl.delete("Sous la scène");
        dAOCategoryImpl.delete("`A 200km");
    }
}
