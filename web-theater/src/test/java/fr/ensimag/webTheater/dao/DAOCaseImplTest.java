/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Case;
import fr.ensimag.webTheater.beans.Customer;
import junit.framework.TestCase;
/**
 *
 * @author thibaut
 */
public class DAOCaseImplTest extends TestCase{
    private DAOFactory daoFactory = null;
    private DAOCaseImpl instance = null;
    private DAOUserImpl dAOUserImpl = null;

    
    public DAOCaseImplTest(String testName) {
        super(testName);
    }

    @Override
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAOCaseImpl(daoFactory);
        dAOUserImpl = new DAOUserImpl(daoFactory);
        Customer customer = new Customer();
        customer.setEmail("frite.belge@frit.fr");
        customer.setFirstName("Frite");
        customer.setLastName("Belge");
        customer.setPassword("monSuperpasswordHasché");
        dAOUserImpl.createCustomer(customer);
        super.setUp();
    }
    
    @Override
    public void tearDown() throws Exception {
        dAOUserImpl.deleteCustomer("frite.belge@frit.fr");
        super.tearDown();
    }

    /**
     * Test of create method, of class DAOCaseImpl.
     */
    public void testCreate() {
        tFindNull();
        tCreate();
        tFindNotNull();
        tDelete();
        tFindNull();
    }
    
    public void tFindNull() {
        System.out.println("findNull");
        Case c = instance.find(42);
        assertNull(c);
        System.out.println("findNull Successful");
    }

    public void tFindNotNull() {
        System.out.println("findNotNull");
        Case c = instance.find(42);
        assertNotNull(c);
        System.out.println("findNotNull Successful");
    }
    
    public void tCreate() {
        System.out.println("create");
        Case c = new Case();
        c.setNumber(42);
        c.setTicketNumber(8916);
        c.setUserMail("frite.belge@frit.fr");
        instance.create(c);
        System.out.println("create Successful");
    }
    
    public void tDelete() {
        System.out.println("delete");
        instance.delete(42);
        System.out.println("delete Successful");
    }
}
