/*
 * The MIT License
 *
 * Copyright 2015 thibaut.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fr.ensimag.webTheater.dao;

import fr.ensimag.webTheater.beans.Booking;
import fr.ensimag.webTheater.beans.Category;
import fr.ensimag.webTheater.beans.Customer;
import fr.ensimag.webTheater.beans.Representation;
import fr.ensimag.webTheater.beans.Room;
import fr.ensimag.webTheater.beans.Row;
import fr.ensimag.webTheater.beans.Seat;
import fr.ensimag.webTheater.beans.Spectacle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author thibaut
 */
public class DAORepresentationImplTest extends TestCase{
    
    private DAOFactory daoFactory = null;
    private DAORepresentationImpl instance = null;
    private DAOCategoryImpl dAOCategoryImpl = null;
    private DAORoomImpl dAORoomImpl = null;
    private DAODateImpl dAODateImpl = null;
    private DAOSpectacleImpl dAOSpectacleImpl = null;
    private DAOBookingImpl dAOBookingImpl = null;
    private DAOUserImpl dAOUserImpl = null;

    public DAORepresentationImplTest(String testName) {
        super(testName);
    }
    
    

    @Override
    public void setUp() throws Exception {
        daoFactory = DAOFactory.getInstance();
        instance = new DAORepresentationImpl(daoFactory);
        dAOCategoryImpl = new DAOCategoryImpl(daoFactory);
        dAORoomImpl = new DAORoomImpl(daoFactory);
        dAODateImpl = new DAODateImpl(daoFactory);
        dAOSpectacleImpl = new DAOSpectacleImpl(daoFactory);
        dAOBookingImpl = new DAOBookingImpl(daoFactory);
        dAOUserImpl= new DAOUserImpl(daoFactory);
        buildRoomAndCategory();
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        dAODateImpl.create(date);
        Spectacle spectacle = new Spectacle();
        spectacle.setName("Ribery en concert");
        spectacle.setNumber(78);
        spectacle.setMailAdmin(null);
        dAOSpectacleImpl.create(spectacle);
        super.setUp();
    }
    
    @Override
    public void tearDown() throws Exception {
        destroyRoomAndCategories();
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        dAODateImpl.delete(date);
        dAOSpectacleImpl.delete(78);
        super.tearDown();
    }

    public void testCreateAndDelete() {
        tCreate(); // We create
        tActualize(true); // We verify that the retpresentation is withdrawn
        tFindSeats(60,60,80); // We look for seats
        tCreateBooking("blabla@truc.com",5,2);
        tFindSeats(60,60,79); // We look for seats
        tCreateBooking("blabla2@truc.com",5,3);
        tFindSeats(60,60,78); // We look for seats
        tCreateBooking("blabla3@truc.com",1,1);
        tFindSeats(59,60,78); // We look for seats
        tCreateBooking("blabla4@truc.com",9,2);
        tFindSeats(59,59,78); // We look for seats
        tDeleteBooking("blabla@truc.com",5,2);
        tFindSeats(59,59,79); // We look for seats
        tDeleteBooking("blabla2@truc.com",5,3);
        tFindSeats(59,59,80); // We look for seats
        tDeleteBooking("blabla3@truc.com",1,1);
        tFindSeats(60,59,80); // We look for seats
        tDeleteBooking("blabla4@truc.com",9,2);
        tFindSeats(60,60,80); // We look for seats
        tCancel();  // We cancel the representation
        tActualize(false); // We verify that the representation is not withdrawn
        tDelete(); // We delete.
    }
    
    public void tCreate() {
        System.out.println("create");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(78);
        instance.create(representation);
        System.out.println("create Successful");
    }
    
    
    public void tActualize(boolean stateIsWithdrawnThen) {
        System.out.println("actualize");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(78);
        instance.actualize(representation);
        assertEquals(representation.isIsWithdrawn(), stateIsWithdrawnThen);
        System.out.println("actualize Successful");        
    }
    
    public void tFindSeats(int nCategory1, int nCategory2, int nCategory3) {
        System.out.println("search seats");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(78);
        HashMap<Category, LinkedList<Seat>> result;
        result = instance.findSeats(representation);
        assertNotNull(result);
        assertEquals(result.size(),3);

        Category category1 = new Category();
        category1.setName("Poulailler");
        Category category2 = new Category();
        category2.setName("Sous la scène");
        Category category3 = new Category();
        category3.setName("`A 200km");
        assertEquals(nCategory1, result.get(category1).size());
        assertEquals(nCategory2, result.get(category2).size());
        assertEquals(nCategory3, result.get(category3).size());

        System.out.println("search seats Successful");
    }
    
    public void tCreateBooking(String userMail, int rowN, int seatN) {
        System.out.println("create Booking");
        Booking booking = new Booking();
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Customer customer = new Customer();
        customer.setEmail(userMail);
        customer.setFirstName("monPrenom");
        customer.setFirstName("monNomDeFamille");
        customer.setPassword("toto");
        dAOUserImpl.createCustomer(customer);
        booking.setDate(date);
        booking.setUserMail(userMail);
        Set<Seat> seats = new HashSet<Seat>();
        Room room = new Room();
        room.setName("salleSale");
        Row row = new Row();
        row.setRowNumber(rowN);
        row.setRoom(room);
        Seat seat = new Seat();
        seat.setId(seatN);
        seat.setRow(row);
        seats.add(seat);
        booking.setSeats(seats);
        dAOBookingImpl.create(booking);
        System.out.println("create Booking Successful");
    }
    
    public void tDeleteBooking(String userMail, int rowN, int seatN) {
        System.out.println("delete Booking");
        Booking booking = new Booking();
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Customer customer = new Customer();
        customer.setEmail(userMail);
        customer.setFirstName("monPrenom");
        customer.setFirstName("monNomDeFamille");
        customer.setPassword("toto");
        booking.setDate(date);
        booking.setUserMail(userMail);
        Set<Seat> seats = new HashSet<Seat>();
        Room room = new Room();
        room.setName("salleSale");
        Row row = new Row();
        row.setRowNumber(rowN);
        row.setRoom(room);
        Seat seat = new Seat();
        seat.setId(seatN);
        seat.setRow(row);
        seats.add(seat);
        booking.setSeats(seats);
        dAOBookingImpl.delete(booking);
        dAOUserImpl.deleteCustomer(userMail);
        System.out.println("delete Booking Successful");
    }
    
    public void tCancel() {
        System.out.println("cancel");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(78);
        instance.cancel(representation);
        System.out.println("cancel Successful");
    }
    
    public void tDelete() {
        System.out.println("delete");
        Date date = new Date();
        date.setHours(10);
        date.setMonth(3);
        date.setYear(2000);
        date.setDate(10);
        Representation representation = new Representation();
        representation.setDate(date);
        representation.setRoomName("salleSale");
        representation.setSpectacleNumber(78);
        instance.delete(representation);
        System.out.println("delete Successful");        
    }

    public void buildRoomAndCategory() {
        Category category1 = null;
        Category category2 = null;
        Category category3 = null;
        category1 = new Category();
        category1.setName("Poulailler");
        category1.setPrice(52.17);
        category2 = new Category();
        category2.setName("Sous la scène");
        category2.setPrice(3.14);
        category3 = new Category();
        category3.setName("`A 200km");
        category3.setPrice(0.18);
        Room room = new Room();
        room.setName("salleSale");
        ArrayList<Row> rows = new ArrayList<>();
        Row row1 = new Row();
        row1.setRowNumber(1);
        row1.setCategory(category1);
        rows.add(row1);
        Row row2 = new Row();
        row2.setRowNumber(2);
        row2.setCategory(category1);
        rows.add(row2);
        Row row3 = new Row();
        row3.setRowNumber(3);
        row3.setCategory(category3);
        rows.add(row3);
        Row row4 = new Row();
        row4.setRowNumber(4);
        row4.setCategory(category1);
        rows.add(row4);
        Row row5 = new Row();
        row5.setRowNumber(5);
        row5.setCategory(category3);
        rows.add(row5);
        Row row6 = new Row();
        row6.setRowNumber(6);
        row6.setCategory(category2);
        rows.add(row6);
        Row row7 = new Row();
        row7.setRowNumber(7);
        row7.setCategory(category3);
        rows.add(row7);
        Row row8 = new Row();
        row8.setRowNumber(8);
        row8.setCategory(category2);
        rows.add(row8);
        Row row9 = new Row();
        row9.setRowNumber(9);
        row9.setCategory(category2);
        rows.add(row9);
        Row row10 = new Row();
        row10.setRowNumber(10);
        row10.setCategory(category3);
        rows.add(row10);
        room.setRows(rows);
        dAOCategoryImpl.create(category1);
        dAOCategoryImpl.create(category2);
        dAOCategoryImpl.create(category3);
        dAORoomImpl.create(room);
    }
    
    public void destroyRoomAndCategories() {
        dAORoomImpl.deleteRoom("salleSale");
        dAOCategoryImpl.delete("Poulailler");
        dAOCategoryImpl.delete("Sous la scène");
        dAOCategoryImpl.delete("`A 200km");
    }

}